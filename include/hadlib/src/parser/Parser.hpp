#ifndef PARSER_PARSER_HPP
#define PARSER_PARSER_HPP

#include <memory>
#include <boost/filesystem.hpp>
#include "IParser.hpp"

namespace NParser
{

/**
 * Creates a parser instance.
 */
std::unique_ptr<IParser> create();

}

#endif  // PARSER_PARSER_HPP
