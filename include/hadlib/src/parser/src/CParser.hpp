#ifndef PARSER_CPARSER_HPP
#define PARSER_CPARSER_HPP

#include <parser/IParser.hpp>
#include <boost/noncopyable.hpp>

namespace NParser
{

class CParser final : public IParser, public boost::noncopyable
{
public:
   /** 
    * Constructor
    */
   CParser() noexcept;

   virtual void parseLaneTile(const std::vector<char>& blob, nds::lane::main::LaneTile& laneTile) const;

   virtual void parseLocTemplateDetails(const std::vector<char>& blob, nds::locobj::attrdefs::LocTemplateDetails& lobTemplateDetails) const;

   virtual void parseRoutingTile(const std::vector<char>& blob, nds::routing::main::RoutingTile& routingTile) const;

   virtual void parseRoutingGeoTile(const std::vector<char>& blob, nds::routing::main::RoutingGeoTile& routingGeoTile) const;
};

}

#endif  // PARSER_CPARSER_HPP
