#ifndef PARSER_ERRORS_HPP
#define PARSER_ERRORS_HPP

#include <stdexcept>

namespace NParser
{

   /** Means that a given blob may not fit the datascript format. */
   class ParserException : public std::runtime_error
   {
   public:
      explicit ParserException(const char* message)
         : std::runtime_error(std::string("ParserException: ") + message)
      {
      }

      explicit ParserException(const std::string& message)
         : std::runtime_error(std::string("ParserException: ") + message)
      {
      }
   };

}

#endif  // PARSER_ERRORS_HPP
