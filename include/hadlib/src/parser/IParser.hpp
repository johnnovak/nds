#ifndef PARSER_IPARSER_HPP
#define PARSER_IPARSER_HPP

#include <memory>
#include <nds/lane/main/LaneTile.h>
#include <nds/locobj/attrdefs/LocTemplateDetails.h>
#include <nds/routing/main/RoutingTile.h>
#include <nds/routing/main/RoutingGeoTile.h>
#include "Errors.hpp"

namespace NParser
{

/** Represents NDS Parser */
class IParser
{
public:

   /**
   * Parses the lane tile from the given blob
   * Throws: ParserException if the blob cannot be parsed.
   */
   virtual void parseLaneTile(const std::vector<char>& blob, nds::lane::main::LaneTile& laneTile) const = 0;

   /**
   * Parses the loc template details from the given blob
   * Throws: ParserException if the blob cannot be parsed.
   */
   virtual void parseLocTemplateDetails(const std::vector<char>& blob, nds::locobj::attrdefs::LocTemplateDetails& lobTemplateDetails) const = 0;

   /**
   * Parses the routing tile from the given blob
   * Throws: ParserException if the blob cannot be parsed.
   */
   virtual void parseRoutingTile(const std::vector<char>& blob, nds::routing::main::RoutingTile& routingTile) const = 0;

   /**
   * Parses the routing geo tile from the given blob
   * Throws: ParserException if the blob cannot be parsed.
   */
   virtual void parseRoutingGeoTile(const std::vector<char>& blob, nds::routing::main::RoutingGeoTile& routingGeoTile) const = 0;

   virtual ~IParser() {}
};

}

#endif  // PARSER_IPARSER_HPP
