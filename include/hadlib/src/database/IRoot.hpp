#ifndef DATABASE_IROOT_HPP
#define DATABASE_IROOT_HPP

#include <vector>
#include "IProduct.hpp"

namespace NDatabase
{

/** Represents NDS Root database */
class IRoot
{
public:
   /** Returns number of products. */
   virtual std::size_t getNumProducts() const noexcept = 0;

   /** 
    * Returns product by index. 
    * Throws: std::out_of_range 
    */
   virtual const IProduct& getProduct(const std::size_t index) const = 0;

   virtual ~IRoot() {}
};

}

#endif  // DATABASE_IROOT_HPP
