#ifndef DATABASE_CPRODUCT_HPP
#define DATABASE_CPRODUCT_HPP

#include <database/IProduct.hpp>
#include <database/ISharedDataBuildingBlock.hpp>
#include <database/IRoutingBuildingBlock.hpp>
#include <database/ILaneBuildingBlock.hpp>
#include <database/ILocObjectBuildingBlock.hpp>
#include <sal/CSqlConnection.hpp>

namespace NDatabase
{

class CProduct final : public IProduct, public boost::noncopyable
{
public:
   /**
   * Constructor
   * Throws: UnknownFormat if connection doesn't lead to a valid NDS database product.
   */
   CProduct(const int id, const int ndsSupplierId, NSal::CSqlConnection const& connection);

   CProduct(CProduct&& other) noexcept;

   virtual int getId() const noexcept;
   virtual int getSupplierId() const noexcept;
   virtual const std::vector<SBuildingBlockInfo>& getBuildingBlocksInfo() const noexcept;

   /**
   * Throws: NSal::CannotOpenException if there are missing files in the database.
   */
   virtual std::unique_ptr<ISharedDataBuildingBlock> createSharedDataBuildingBlock(const int urId, const int bbId) const;

   /**
   * Throws: NSal::CannotOpenException if there are missing files in the database.
   */
   virtual std::unique_ptr<IRoutingBuildingBlock> createRoutingBuildingBlock(const int urId, const int bbId) const;

   /**
   * Throws: NSal::CannotOpenException if there are missing files in the database.
   */
   virtual std::unique_ptr<ILaneBuildingBlock> createLaneBuildingBlock(const int urId, const int bbId) const;

   /**
   * Throws: NSal::CannotOpenException if there are missing files in the database.
   */
   virtual std::unique_ptr<ILocObjectBuildingBlock> createLocObjectBuildingBlock(const int urId, const int bbId) const;

private:
   const int mId;
   const int mNdsSupplierId;
   std::vector<SBuildingBlockInfo> mBuildingBlockInfos;
   std::vector<boost::filesystem::path> mBbUris;
};

}

#endif  // DATABASE_CPRODUCT_HPP
