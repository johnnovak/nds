#ifndef DATABASE_CROUTINGBUILDINGBLOCK_HPP
#define DATABASE_CROUTINGBUILDINGBLOCK_HPP

#include <database/IProduct.hpp>
#include <database/IRoutingBuildingBlock.hpp>
#include <sal/CSqlConnection.hpp>

namespace NDatabase
{

class CRoutingBuildingBlock final : public IRoutingBuildingBlock, public boost::noncopyable
{
public:
   CRoutingBuildingBlock(const SBuildingBlockInfo& info, NSal::CSqlConnection&& connection) noexcept;

   virtual const SBuildingBlockInfo& getInfo() const noexcept;

   virtual void loadRoutingTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SRoutingTile&)> receiver) const;

   virtual void loadRoutingGeoTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SRoutingGeoTile&)> receiver) const;

private:
   const SBuildingBlockInfo& mInfo;
   NSal::CSqlConnection mConnection;
};

}

#endif  // DATABASE_CROUTINGBUILDINGBLOCK_HPP
