#ifndef DATABASE_CLOCOBJECTBUILDINGBLOCK_HPP
#define DATABASE_CLOCOBJECTBUILDINGBLOCK_HPP

#include <database/IProduct.hpp>
#include <database/ILocObjectBuildingBlock.hpp>
#include <sal/CSqlConnection.hpp>

namespace NDatabase
{

class CLocObjectBuildingBlock final : public ILocObjectBuildingBlock, public boost::noncopyable
{
public:
   CLocObjectBuildingBlock(const SBuildingBlockInfo& info, NSal::CSqlConnection&& connection) noexcept;

   virtual const SBuildingBlockInfo& getInfo() const noexcept;

   virtual void loadTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SLocObjectTile&)> receiver) const;

   virtual void loadLocTemplates(const std::vector<TLocTemplateId>& ids, std::function<bool(const SLocTemplate&)> receiver) const;

private:
   const SBuildingBlockInfo& mInfo;
   NSal::CSqlConnection mConnection;
};

}

#endif  // DATABASE_CLOCOBJECTBUILDINGBLOCK_HPP
