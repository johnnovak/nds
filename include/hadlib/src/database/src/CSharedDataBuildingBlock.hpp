#ifndef DATABASE_CSHAREDDATABUILDINGBLOCK_HPP
#define DATABASE_CSHAREDDATABUILDINGBLOCK_HPP

#include <database/IProduct.hpp>
#include <database/ISharedDataBuildingBlock.hpp>
#include <sal/CSqlConnection.hpp>

namespace NDatabase
{

class CSharedDataBuildingBlock final : public ISharedDataBuildingBlock, public boost::noncopyable
{
public:
   CSharedDataBuildingBlock(const SBuildingBlockInfo& info, NSal::CSqlConnection&& connection) noexcept;

   virtual const SBuildingBlockInfo& getInfo() const noexcept;

   virtual std::vector<SLevelMetadata> getLevelMetadata() const;

private:
   const SBuildingBlockInfo& mInfo;
   NSal::CSqlConnection mConnection;
};

}

#endif  // DATABASE_CSHAREDDATABUILDINGBLOCK_HPP
