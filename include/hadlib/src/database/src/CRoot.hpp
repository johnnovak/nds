#ifndef DATABASE_CROOT_HPP
#define DATABASE_CROOT_HPP

#include <database/IRoot.hpp>
#include <sal/CSqlConnection.hpp>

#include "CProduct.hpp"

namespace NDatabase
{

class CRoot final : public IRoot
{
public:
   /** 
    * Constructor
    * Throws: UnknownFormat if connection doesn't lead to a valid NDS database root.
    * Throws: CannotOpen if database files are missing.
    */
   explicit CRoot(NSal::CSqlConnection const& connection);

   virtual std::size_t getNumProducts() const noexcept;
   virtual const IProduct& getProduct(const std::size_t index) const;

private:
   std::vector<CProduct> mProducts;
};

}

#endif  // DATABASE_CROOT_HPP
