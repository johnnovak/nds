#ifndef DATABASE_CLANEBUILDINGBLOCK_HPP
#define DATABASE_CLANEBUILDINGBLOCK_HPP

#include <database/IProduct.hpp>
#include <database/ILaneBuildingBlock.hpp>
#include <sal/CSqlConnection.hpp>

namespace NDatabase
{

class CLaneBuildingBlock final : public ILaneBuildingBlock, public boost::noncopyable
{
public:
   CLaneBuildingBlock(const SBuildingBlockInfo& info, NSal::CSqlConnection&& connection) noexcept;

   virtual const SBuildingBlockInfo& getInfo() const noexcept;

   virtual void loadTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SLaneTile&)> receiver) const;

private:
   const SBuildingBlockInfo& mInfo;
   NSal::CSqlConnection mConnection;
};

}

#endif  // DATABASE_CLANEBUILDINGBLOCK_HPP
