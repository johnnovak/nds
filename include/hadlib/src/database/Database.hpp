#ifndef DATABASE_DATABASE_HPP
#define DATABASE_DATABASE_HPP

#include <memory>
#include <boost/filesystem.hpp>
#include "IRoot.hpp"

namespace NDatabase
{

/** 
 * Opens database and returns Root element of it. 
 * Throws: NSal::CannotOpenException
 * Throws: UnknownFormat - if product information is missed.
 */
std::unique_ptr<IRoot> open(const boost::filesystem::path& path);

}

#endif  // DATABASE_DATABASE_HPP
