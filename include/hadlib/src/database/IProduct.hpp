#ifndef DATABASE_IPRODUCT_HPP
#define DATABASE_IPRODUCT_HPP

#include <string>
#include <vector>
#include <memory>

namespace NDatabase
{

enum class BuildingBlockType
{
   SHARED_DATA = 0,
   ROUTING = 2,
   LANE = 18,
   LOCALIZATION_OBJECTS = 19
};

struct SBuildingBlockInfo
{
   int urId;
   int bbId;
   std::string name;
   BuildingBlockType bbType;
};

class ISharedDataBuildingBlock;
class IRoutingBuildingBlock;
class ILaneBuildingBlock;
class ILocObjectBuildingBlock;

/** Represents NDS Product database. */
class IProduct
{
public:
   /** Returns id. */
   virtual int getId() const noexcept = 0;

   /** Returns nds supplier id. */
   virtual int getSupplierId() const noexcept = 0;

   /** Returns building blocks information. */
   virtual const std::vector<SBuildingBlockInfo>& getBuildingBlocksInfo() const noexcept = 0;

   /**
   * Creates instance of specific building block
   * Throws: NSal::CannotOpenException if there are missing files in the database, required to create this building block.
   * Note: if there is no building block with given parameters - empty pointer is returned.
   */
   virtual std::unique_ptr<ISharedDataBuildingBlock> createSharedDataBuildingBlock(const int urId, const int bbId) const = 0;

   /**
   * Creates instance of specific building block
   * Throws: NSal::CannotOpenException if there are missing files in the database, required to create this building block.
   * Note: if there is no building block with given parameters - empty pointer is returned.
   */
   virtual std::unique_ptr<IRoutingBuildingBlock> createRoutingBuildingBlock(const int urId, const int bbId) const = 0;

   /**
   * Creates instance of specific building block
   * Throws: NSal::CannotOpenException if there are missing files in the database, required to create this building block.
   * Note: if there is no building block with given parameters - empty pointer is returned.
   */
   virtual std::unique_ptr<ILaneBuildingBlock> createLaneBuildingBlock(const int urId, const int bbId) const = 0;

   /**
   * Creates instance of specific building block
   * Throws: NSal::CannotOpenException if there are missing files in the database, required to create this building block.
   * Note: if there is no building block with given parameters - empty pointer is returned.
   */
   virtual std::unique_ptr<ILocObjectBuildingBlock> createLocObjectBuildingBlock(const int urId, const int bbId) const = 0;

   virtual ~IProduct() {}
};

}

#endif  // DATABASE_IPRODUCT_HPP
