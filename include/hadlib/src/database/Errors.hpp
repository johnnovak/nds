#ifndef DATABASE_ERRORS_HPP
#define DATABASE_ERRORS_HPP

#include <stdexcept>

namespace NDatabase
{

   /** Means that format of database cannot be interpreted correctly. */
   class UnknownFormatException : public std::runtime_error
   {
   public:
      explicit UnknownFormatException(const char* message)
         : std::runtime_error(std::string("UnknownFormatException: ") + message)
      {
      }

      explicit UnknownFormatException(const std::string& message)
         : std::runtime_error(std::string("UnknownFormatException: ") + message)
      {
      }
   };

}

#endif  // DATABASE_ERRORS_HPP
