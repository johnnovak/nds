#ifndef DATABASE_IROUTINGBUILDINGBLOCK_HPP
#define DATABASE_IROUTINGBUILDINGBLOCK_HPP

#include "IProduct.hpp"
#include <functional>

namespace NDatabase
{

typedef std::int32_t TPackedTileId;

struct SRoutingTile
{
   TPackedTileId id;
   std::vector<char> blob;
   int versionId;
};

struct SRoutingGeoTile
{
   TPackedTileId id;
   std::vector<char> blob;
   int versionId;
};

/** Represents database Routing Building block. */
class IRoutingBuildingBlock
{
public:
   /** Returns building block info. */
   virtual const SBuildingBlockInfo& getInfo() const noexcept = 0;

   /** Loads routing tiles. */
   virtual void loadRoutingTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SRoutingTile&)> receiver) const = 0;

   /** Loads routing geo tiles. */
   virtual void loadRoutingGeoTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SRoutingGeoTile&)> receiver) const = 0;

   virtual ~IRoutingBuildingBlock() {}
};

}

#endif  // DATABASE_IROUTINGBUILDINGBLOCK_HPP
