#ifndef DATABASE_ILOCOBJECTBUILDINGBLOCK_HPP
#define DATABASE_ILOCOBJECTBUILDINGBLOCK_HPP

#include "IProduct.hpp"
#include <functional>

namespace NDatabase
{

typedef std::int32_t TPackedTileId;

struct SLocObjectTile
{
   TPackedTileId id;
   std::vector<char> blob;
   std::vector<char> locObjectLayer;
   int versionId;
};

typedef std::int32_t TLocTemplateId;

enum class LocObjectType
{
   SIGN = 0,
   POLE = 1,
   MARKING = 2
};

struct SLocTemplate
{
   TLocTemplateId id;
   int versionId;
   LocObjectType type;
   std::vector<char> details;
};

/** Represents database Localization Object Building block. */
class ILocObjectBuildingBlock
{
public:
   /** Returns building block info. */
   virtual const SBuildingBlockInfo& getInfo() const noexcept = 0;

   /** Loads tiles. */
   virtual void loadTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SLocObjectTile&)> receiver) const = 0;

   /** Loads loc templates */
   virtual void loadLocTemplates(const std::vector<TLocTemplateId>& ids, std::function<bool(const SLocTemplate&)> receiver) const = 0;

   virtual ~ILocObjectBuildingBlock() {}
};

}

#endif  // DATABASE_ILOCOBJECTBUILDINGBLOCK_HPP
