#ifndef DATABASE_ISHAREDDATABUILDINGBLOCK_HPP
#define DATABASE_ISHAREDDATABUILDINGBLOCK_HPP

#include "IProduct.hpp"
#include <functional>

namespace NDatabase
{

struct SLevelMetadata
{
   int levelNumber;
   int bbId;
   int coordShift;
};

/** Represents database SharedData Building block. */
class ISharedDataBuildingBlock
{
public:
   /** Returns building block info. */
   virtual const SBuildingBlockInfo& getInfo() const noexcept = 0;

   /** Returns level metadata. */
   virtual std::vector<SLevelMetadata> getLevelMetadata() const = 0;

   virtual ~ISharedDataBuildingBlock() {}
};

}

#endif  // DATABASE_ISHAREDDATABUILDINGBLOCK_HPP
