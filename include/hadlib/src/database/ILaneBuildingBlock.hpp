#ifndef DATABASE_ILANEBUILDINGBLOCK_HPP
#define DATABASE_ILANEBUILDINGBLOCK_HPP

#include "IProduct.hpp"
#include <functional>

namespace NDatabase
{

typedef std::int32_t TPackedTileId;

struct SLaneTile
{
   TPackedTileId id;
   std::vector<char> blob;
   int versionId;
};

/** Represents database Lane Building block. */
class ILaneBuildingBlock
{
public:
   /** Returns building block info. */
   virtual const SBuildingBlockInfo& getInfo() const noexcept = 0;

   /** Loads tiles. */
   virtual void loadTiles(const std::vector<TPackedTileId>& ids, std::function<bool(const SLaneTile&)> receiver) const = 0;

   virtual ~ILaneBuildingBlock() {}
};

}

#endif  // DATABASE_ILANEBUILDINGBLOCK_HPP
