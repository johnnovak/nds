#ifndef SAL_CSQLRECORDSET_HPP
#define SAL_CSQLRECORDSET_HPP

#include <nds_sqlite3.h>
#include "Errors.hpp"
#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>
#include <map>
#include <vector>
#include <memory.h>

namespace NSal
{

enum class SqlColumnType
{
   Int,
   Blob,
   String
};

template<SqlColumnType SQL_COLUMN_TYPE>
struct SqlColumnTypeTraits
{};

template<>
struct SqlColumnTypeTraits<SqlColumnType::Int>
{
   typedef int Type;
   static const int SQLITE_TYPE = SQLITE_INTEGER;
   static Type extract(sqlite3_stmt * stmt, const int columnNum)
   {
      return sqlite3_column_int(stmt, columnNum);
   }
};

template<>
struct SqlColumnTypeTraits<SqlColumnType::String>
{
   typedef std::string Type;
   static const int SQLITE_TYPE = SQLITE_TEXT;
   static Type extract(sqlite3_stmt * stmt, const int columnNum)
   {
      return reinterpret_cast<const char*>(sqlite3_column_text(stmt, columnNum));
   }
};

template<>
struct SqlColumnTypeTraits<SqlColumnType::Blob>
{
   typedef std::vector<char> Type;
   static const int SQLITE_TYPE = SQLITE_BLOB;
   static Type extract(sqlite3_stmt * stmt, const int columnNum)
   {
      std::vector<char> result;

      const void *blob = sqlite3_column_blob(stmt, columnNum);
      if (blob != 0)
      {
         int size = sqlite3_column_bytes(stmt, columnNum);
         result.resize(size);
         memcpy(&result.front(), blob, size);
      }

      return result;
   }
};

class CSqlRecordSet : public boost::noncopyable
{
public:
   /** 
    * Constructor.
    * Requirement: openedDbStatement must not be null, must be opened sqlite statement.
    */
   CSqlRecordSet(sqlite3_stmt * openedDbStatement);

   /** Move constructor. */
   CSqlRecordSet(CSqlRecordSet&& other) noexcept;

   /** 
    * Reposition record set pointer to the next element if possible.
    * Returns true if reposition was successfull, false if end of record set was reached.
    * Throw: DatabaseErrorException
    */
   bool next();

   /** 
    * Returns cell of the column named *columnName* of given type. 
    * Throw: LogicErrorException - if column not found or type mismatch.
    */
   template<SqlColumnType SQL_COLUMN_TYPE>
   typename SqlColumnTypeTraits<SQL_COLUMN_TYPE>::Type getCellData(const std::string& columnName) const;

   /** Destructor. */
   ~CSqlRecordSet();

private:
   sqlite3_stmt * mStmt;
   std::map<std::string, int> mColumnNameToIndexMap;
};

template<SqlColumnType SQL_COLUMN_TYPE>
typename SqlColumnTypeTraits<SQL_COLUMN_TYPE>::Type CSqlRecordSet::getCellData(const std::string & columnName) const
{
   auto iter = mColumnNameToIndexMap.find(columnName);
   if (iter == mColumnNameToIndexMap.end())
   {
      throw LogicErrorException("Column " + columnName + " not found in query " + sqlite3_sql(mStmt));
   }

   if (sqlite3_column_type(mStmt, iter->second) != SqlColumnTypeTraits<SQL_COLUMN_TYPE>::SQLITE_TYPE)
   {
      throw LogicErrorException(sqlite3_errmsg(sqlite3_db_handle(mStmt)));
   }
   
   return SqlColumnTypeTraits<SQL_COLUMN_TYPE>::extract(mStmt, iter->second);
}

}

#endif  // SAL_CSQLRECORDSET_HPP
