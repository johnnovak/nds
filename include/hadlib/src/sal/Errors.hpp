#ifndef SAL_ERRORS_HPP
#define SAL_ERRORS_HPP

#include <stdexcept>

namespace NSal
{

   /** Means that database cannot be opened. */
   class CannotOpenException : public std::runtime_error
   {
   public:
      explicit CannotOpenException(const char* message)
         : std::runtime_error(std::string("CannotOpenException: ") + message)
      {
      }

      explicit CannotOpenException(const std::string& message)
         : std::runtime_error(std::string("CannotOpenException: ") + message)
      {
      }
   };

   /** Means that logic is violated. */
   class LogicErrorException : public std::logic_error
   {
   public:
      explicit LogicErrorException(const char* message)
         : std::logic_error(std::string("LogicErrorException: ") + message)
      {
      }

      explicit LogicErrorException(const std::string& message)
         : std::logic_error(std::string("LogicErrorException: ") + message)
      {
      }
   };

   /** Means that something wrong with nds database. */
   class DatabaseErrorException : public std::runtime_error
   {
   public:
      explicit DatabaseErrorException(const char* message)
         : std::runtime_error(std::string("DatabaseErrorException: ") + message)
      {
      }

      explicit DatabaseErrorException(const std::string& message)
         : std::runtime_error(std::string("DatabaseErrorException: ") + message)
      {
      }
   };

}

#endif  // SAL_ERRORS_HPP
