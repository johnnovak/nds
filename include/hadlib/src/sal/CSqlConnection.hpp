#ifndef SAL_CSQLCONNECTION_HPP
#define SAL_CSQLCONNECTION_HPP

#include <nds_sqlite3.h>
#include <boost/filesystem/path.hpp>
#include "CSqlRecordSet.hpp"

namespace NSal
{

/**
 * Connection to the sqlite database
 */
class CSqlConnection : public boost::noncopyable
{
public:
   /** 
    * Constructor 
    * Throws: CannotOpenException
    */
   CSqlConnection(const boost::filesystem::path& filePath);

   CSqlConnection(CSqlConnection&& other) noexcept;

   /** 
    * Creates record set using provided query and position it to first element. 
    * Throws: LogicErrorException - if query is malformed.
    */
   CSqlRecordSet execQuery(const std::string& queryText) const;

   /** Returns path of the sqlite file. */
   const boost::filesystem::path& getPath() const noexcept;

   /** Dtor */
   ~CSqlConnection();

private:
   sqlite3* mDb = nullptr;
   const boost::filesystem::path mPath;
};

} // NSal

#endif  // SAL_CSQLCONNECTION_HPP
