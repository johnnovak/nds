#ifndef NDS_HB_EXTENSIONS_H
#define NDS_HB_EXTENSIONS_H

#include "nds_sqlite3.h"

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * The function registers Harman NDS extensions.
 *
 * \param p_db The pointer to database for which to register the extensions.
 *
 * \return Returns SQLITE_OK in case of success, otherwise returns SQLite error code.
 */
int nds_extensions_init(sqlite3* p_db);

SQLITE_API int sqlite3_getIOStatistics(unsigned long long* totalReadAmount, unsigned int* totalNumReads);
SQLITE_API int sqlite3_resetIOStatistics();
SQLITE_API int sqlite3_getIOStatistics_v2(unsigned long long* totalReadAmount, unsigned int* totalNumReads);
#ifdef __cplusplus
}
#endif

#endif /* NDS_HB_EXTENSIONS_H */
