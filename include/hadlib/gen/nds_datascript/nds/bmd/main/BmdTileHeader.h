/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_BMD_MAIN_BMD_TILE_HEADER_H
#define NDS_BMD_MAIN_BMD_TILE_HEADER_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/bmd/main/BmdContentMask.h"
#include "nds/common/RelativeBlobOffset.h"

namespace nds
{
namespace bmd
{
namespace main
{

class BmdTileHeader
{
public:
    BmdTileHeader();
    BmdTileHeader(datascript::BitStreamReader& _in);

    nds::bmd::main::BmdContentMask& getContentMask();
    const nds::bmd::main::BmdContentMask& getContentMask() const;
    void setContentMask(const nds::bmd::main::BmdContentMask& contentMask);

    nds::common::RelativeBlobOffset getAreasOffset() const;
    void setAreasOffset(nds::common::RelativeBlobOffset areasOffset);
    bool hasAreasOffset() const;

    nds::common::RelativeBlobOffset getAreaIdRangeListOffset() const;
    void setAreaIdRangeListOffset(nds::common::RelativeBlobOffset areaIdRangeListOffset);
    bool hasAreaIdRangeListOffset() const;

    nds::common::RelativeBlobOffset getLinesOffset() const;
    void setLinesOffset(nds::common::RelativeBlobOffset linesOffset);
    bool hasLinesOffset() const;

    nds::common::RelativeBlobOffset getLineIdRangeListOffset() const;
    void setLineIdRangeListOffset(nds::common::RelativeBlobOffset lineIdRangeListOffset);
    bool hasLineIdRangeListOffset() const;

    nds::common::RelativeBlobOffset getPointsOffset() const;
    void setPointsOffset(nds::common::RelativeBlobOffset pointsOffset);
    bool hasPointsOffset() const;

    nds::common::RelativeBlobOffset getPointIdRangeListOffset() const;
    void setPointIdRangeListOffset(nds::common::RelativeBlobOffset pointIdRangeListOffset);
    bool hasPointIdRangeListOffset() const;

    nds::common::RelativeBlobOffset getFlexibleAttributesOffset() const;
    void setFlexibleAttributesOffset(nds::common::RelativeBlobOffset flexibleAttributesOffset);
    bool hasFlexibleAttributesOffset() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const BmdTileHeader& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    datascript::InPlaceOptionalHolder<nds::bmd::main::BmdContentMask> m_contentMask;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_areasOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_areaIdRangeListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_linesOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_lineIdRangeListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_pointsOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_pointIdRangeListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_flexibleAttributesOffset;
};

} // namespace main
} // namespace bmd
} // namespace nds

#endif // NDS_BMD_MAIN_BMD_TILE_HEADER_H
