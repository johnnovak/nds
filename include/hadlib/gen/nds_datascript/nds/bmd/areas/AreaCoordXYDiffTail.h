/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_BMD_AREAS_AREA_COORD_XYDIFF_TAIL_H
#define NDS_BMD_AREAS_AREA_COORD_XYDIFF_TAIL_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace bmd
{
namespace areas
{

class AreaCoordXYDiffTail
{
public:
    AreaCoordXYDiffTail();
    AreaCoordXYDiffTail(datascript::BitStreamReader& _in,
        uint8_t numBits);

    AreaCoordXYDiffTail(const AreaCoordXYDiffTail& _other);
    AreaCoordXYDiffTail& operator=(const AreaCoordXYDiffTail& _other);

    void initialize(
        uint8_t numBits);

    uint8_t getNumBits() const;

    int16_t getDx() const;
    void setDx(int16_t dx);

    int16_t getDy() const;
    void setDy(int16_t dy);

    uint8_t getIsPseudo() const;
    void setIsPseudo(uint8_t isPseudo);

    uint8_t getPolygonFlag() const;
    void setPolygonFlag(uint8_t polygonFlag);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const AreaCoordXYDiffTail& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint8_t m_numBits;
    bool m_isInitialized;
    int16_t m_dx;
    int16_t m_dy;
    uint8_t m_isPseudo;
    uint8_t m_polygonFlag;
};

} // namespace areas
} // namespace bmd
} // namespace nds

#endif // NDS_BMD_AREAS_AREA_COORD_XYDIFF_TAIL_H
