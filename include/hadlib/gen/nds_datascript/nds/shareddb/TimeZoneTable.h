/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SHAREDDB_TIME_ZONE_TABLE_H
#define NDS_SHAREDDB_TIME_ZONE_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/shareddb/TimeZoneTableRow.h"
#include "nds/common/DateTime.h"
#include "nds/common/TimeZoneId.h"
#include "nds/shareddb/QuarterHourTimeOffset.h"
#include "nds/shareddb/TimeZoneNameId.h"

namespace nds
{
namespace shareddb
{

class TimeZoneTable
{
public:
    TimeZoneTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TimeZoneTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TimeZoneTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TimeZoneTableRow>& rows) const;
    void write(const std::vector<TimeZoneTableRow>& rows);
    void update(const TimeZoneTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TimeZoneTableRow>& rows);
    static void writeRow(const TimeZoneTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace shareddb
} // namespace nds

#endif // NDS_SHAREDDB_TIME_ZONE_TABLE_H
