/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_NG_MAIN_NG_INTERMEDIATE_FEATURE_LIST_TABLE_H
#define NDS_NG_MAIN_NG_INTERMEDIATE_FEATURE_LIST_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ng/main/NgIntermediateFeatureListTableRow.h"
#include "nds/ng/main/NgAssociatedFeatureRefId.h"
#include "nds/ng/main/NgIntermediateFeatureListId.h"

namespace nds
{
namespace ng
{
namespace main
{

class NgIntermediateFeatureListTable
{
public:
    NgIntermediateFeatureListTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~NgIntermediateFeatureListTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<NgIntermediateFeatureListTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<NgIntermediateFeatureListTableRow>& rows) const;
    void write(const std::vector<NgIntermediateFeatureListTableRow>& rows);
    void update(const NgIntermediateFeatureListTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<NgIntermediateFeatureListTableRow>& rows);
    static void writeRow(const NgIntermediateFeatureListTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace main
} // namespace ng
} // namespace nds

#endif // NDS_NG_MAIN_NG_INTERMEDIATE_FEATURE_LIST_TABLE_H
