/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_NG_MAIN_NG_FEATURE_SOURCE_TYPE_H
#define NDS_NG_MAIN_NG_FEATURE_SOURCE_TYPE_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace ng
{
namespace main
{

class NgFeatureSourceType
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_NgFeatureSourceType
    {
        AREA = UINT8_C(0),
        LINE = UINT8_C(1),
        POINT = UINT8_C(2),
        POI = UINT8_C(3)
    };

    NgFeatureSourceType();
    NgFeatureSourceType(e_NgFeatureSourceType value);
    explicit NgFeatureSourceType(datascript::BitStreamReader& _in);

    operator e_NgFeatureSourceType() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const NgFeatureSourceType& other) const;
    bool operator==(e_NgFeatureSourceType other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static NgFeatureSourceType toEnum(uint8_t rawValue);

private:
    e_NgFeatureSourceType m_value;
};

} // namespace main
} // namespace ng
} // namespace nds

#endif // NDS_NG_MAIN_NG_FEATURE_SOURCE_TYPE_H
