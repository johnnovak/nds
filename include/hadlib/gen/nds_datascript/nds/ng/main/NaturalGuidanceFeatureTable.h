/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_NG_MAIN_NATURAL_GUIDANCE_FEATURE_TABLE_H
#define NDS_NG_MAIN_NATURAL_GUIDANCE_FEATURE_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ng/main/NaturalGuidanceFeatureTableRow.h"
#include "nds/common/MortonCode.h"
#include "nds/icondata/main/IconSetId.h"
#include "nds/ng/main/NgFeatureClass.h"
#include "nds/ng/main/NgFeatureId.h"
#include "nds/ng/main/NgFeatureSourceType.h"
#include "nds/ng/ngname/NameStringCollectionId.h"

namespace nds
{
namespace ng
{
namespace main
{

class NaturalGuidanceFeatureTable
{
public:
    NaturalGuidanceFeatureTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~NaturalGuidanceFeatureTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<NaturalGuidanceFeatureTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<NaturalGuidanceFeatureTableRow>& rows) const;
    void write(const std::vector<NaturalGuidanceFeatureTableRow>& rows);
    void update(const NaturalGuidanceFeatureTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<NaturalGuidanceFeatureTableRow>& rows);
    static void writeRow(const NaturalGuidanceFeatureTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace main
} // namespace ng
} // namespace nds

#endif // NDS_NG_MAIN_NATURAL_GUIDANCE_FEATURE_TABLE_H
