/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_NG_MAIN_NG_FEATURE_CLASS_DEFINITION_TABLE_ROW_H
#define NDS_NG_MAIN_NG_FEATURE_CLASS_DEFINITION_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/icondata/main/IconSetId.h"
#include "nds/ng/main/NgFeatureClass.h"
#include "nds/ng/main/NgStandardFeatureClass.h"
#include "nds/ng/ngname/NameStringCollectionId.h"

namespace nds
{
namespace ng
{
namespace main
{

class NgFeatureClassDefinitionTableRow
{
public:
    NgFeatureClassDefinitionTableRow();
    ~NgFeatureClassDefinitionTableRow();

    nds::ng::main::NgFeatureClass getFeatureClass() const;
    void setFeatureClass(nds::ng::main::NgFeatureClass featureClass);
    bool isNullFeatureClass() const;
    void setNullFeatureClass();

    nds::ng::main::NgStandardFeatureClass getStandardFeatureClass() const;
    void setStandardFeatureClass(nds::ng::main::NgStandardFeatureClass standardFeatureClass);
    bool isNullStandardFeatureClass() const;
    void setNullStandardFeatureClass();

    nds::ng::ngname::NameStringCollectionId getNameStringCollectionId() const;
    void setNameStringCollectionId(nds::ng::ngname::NameStringCollectionId nameStringCollectionId);
    bool isNullNameStringCollectionId() const;
    void setNullNameStringCollectionId();

    nds::icondata::main::IconSetId getIconSetId() const;
    void setIconSetId(nds::icondata::main::IconSetId iconSetId);
    bool isNullIconSetId() const;
    void setNullIconSetId();

private:
    datascript::InPlaceOptionalHolder<nds::ng::main::NgFeatureClass> m_featureClass;
    datascript::InPlaceOptionalHolder<nds::ng::main::NgStandardFeatureClass> m_standardFeatureClass;
    datascript::InPlaceOptionalHolder<nds::ng::ngname::NameStringCollectionId> m_nameStringCollectionId;
    datascript::InPlaceOptionalHolder<nds::icondata::main::IconSetId> m_iconSetId;
};

} // namespace main
} // namespace ng
} // namespace nds

#endif // NDS_NG_MAIN_NG_FEATURE_CLASS_DEFINITION_TABLE_ROW_H
