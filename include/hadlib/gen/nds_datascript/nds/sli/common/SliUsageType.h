/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SLI_COMMON_SLI_USAGE_TYPE_H
#define NDS_SLI_COMMON_SLI_USAGE_TYPE_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace sli
{
namespace common
{

class SliUsageType
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_SliUsageType
    {
        DEFAULT_OFFICIAL = UINT8_C(0),
        ROTATED_DEFAULT_OFFICIAL = UINT8_C(1),
        MODIFIED_DEFAULT_OFFICIAL = UINT8_C(2),
        MODIFIED_ROTATED_DEFAULT_OFFICIAL = UINT8_C(3),
        OFFICIAL = UINT8_C(10),
        ROTATED_OFFICIAL = UINT8_C(11),
        MODIFIED_OFFICIAL = UINT8_C(12),
        MODIFIED_ROTATED_OFFICIAL = UINT8_C(13),
        ABBREVIATION = UINT8_C(20),
        ROTATED_ABBREVIATION = UINT8_C(21),
        MODIFIED_ABBREVIATION = UINT8_C(22),
        MODIFIED_ROTATED_ABBREVIATION = UINT8_C(23),
        ALTERNATE_NAME = UINT8_C(30),
        ROTATED_ALTERNATE = UINT8_C(31),
        MODIFIED_ALTERNATE = UINT8_C(32),
        MODIFIED_ROTATED_ALTERNATE = UINT8_C(33),
        CLUSTER = UINT8_C(40),
        ROTATED_CLUSTER = UINT8_C(41),
        MODIFIED_CLUSTER = UINT8_C(42),
        MODIFIED_ROTATED_CLUSTER = UINT8_C(43),
        EXONYM = UINT8_C(50),
        MODIFIED_EXONYM = UINT8_C(51),
        ROTATED_EXONYM = UINT8_C(52),
        MODIFIED_ROTATED_EXONYM = UINT8_C(53),
        BASENAME = UINT8_C(60),
        ROTATED_BASENAME = UINT8_C(61),
        MODIFIED_BASENAME = UINT8_C(62),
        MODIFIED_ROTATED_BASENAME = UINT8_C(63),
        PHONE_NUMBER = UINT8_C(70)
    };

    SliUsageType();
    SliUsageType(e_SliUsageType value);
    explicit SliUsageType(datascript::BitStreamReader& _in);

    operator e_SliUsageType() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const SliUsageType& other) const;
    bool operator==(e_SliUsageType other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static SliUsageType toEnum(uint8_t rawValue);

private:
    e_SliUsageType m_value;
};

} // namespace common
} // namespace sli
} // namespace nds

#endif // NDS_SLI_COMMON_SLI_USAGE_TYPE_H
