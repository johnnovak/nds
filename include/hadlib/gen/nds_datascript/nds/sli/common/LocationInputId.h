/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SLI_COMMON_LOCATION_INPUT_ID_H
#define NDS_SLI_COMMON_LOCATION_INPUT_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace sli
{
namespace common
{

typedef int64_t LocationInputId;

} // namespace common
} // namespace sli
} // namespace nds

#endif // NDS_SLI_COMMON_LOCATION_INPUT_ID_H
