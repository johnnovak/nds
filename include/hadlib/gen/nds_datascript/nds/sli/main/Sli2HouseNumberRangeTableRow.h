/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SLI_MAIN_SLI2_HOUSE_NUMBER_RANGE_TABLE_ROW_H
#define NDS_SLI_MAIN_SLI2_HOUSE_NUMBER_RANGE_TABLE_ROW_H

#include <datascript/Types.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/UpdateRegionId.h"

namespace nds
{
namespace sli
{
namespace main
{

class Sli2HouseNumberRangeTableRow
{
public:
    Sli2HouseNumberRangeTableRow();
    ~Sli2HouseNumberRangeTableRow();

    int64_t getCriterion2HouseNumberRangeId() const;
    void setCriterion2HouseNumberRangeId(int64_t criterion2HouseNumberRangeId);
    bool isNullCriterion2HouseNumberRangeId() const;
    void setNullCriterion2HouseNumberRangeId();

    nds::common::UpdateRegionId getUpdateRegionId() const;
    void setUpdateRegionId(nds::common::UpdateRegionId updateRegionId);
    bool isNullUpdateRegionId() const;
    void setNullUpdateRegionId();

private:
    datascript::InPlaceOptionalHolder<int64_t> m_criterion2HouseNumberRangeId;
    datascript::InPlaceOptionalHolder<nds::common::UpdateRegionId> m_updateRegionId;
};

} // namespace main
} // namespace sli
} // namespace nds

#endif // NDS_SLI_MAIN_SLI2_HOUSE_NUMBER_RANGE_TABLE_ROW_H
