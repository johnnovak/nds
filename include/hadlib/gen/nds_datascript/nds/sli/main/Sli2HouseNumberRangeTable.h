/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SLI_MAIN_SLI2_HOUSE_NUMBER_RANGE_TABLE_H
#define NDS_SLI_MAIN_SLI2_HOUSE_NUMBER_RANGE_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/sli/main/Sli2HouseNumberRangeTableRow.h"
#include "nds/common/UpdateRegionId.h"

namespace nds
{
namespace sli
{
namespace main
{

class Sli2HouseNumberRangeTable
{
public:
    Sli2HouseNumberRangeTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~Sli2HouseNumberRangeTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<Sli2HouseNumberRangeTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<Sli2HouseNumberRangeTableRow>& rows) const;
    void write(const std::vector<Sli2HouseNumberRangeTableRow>& rows);
    void update(const Sli2HouseNumberRangeTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<Sli2HouseNumberRangeTableRow>& rows);
    static void writeRow(const Sli2HouseNumberRangeTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace main
} // namespace sli
} // namespace nds

#endif // NDS_SLI_MAIN_SLI2_HOUSE_NUMBER_RANGE_TABLE_H
