/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SLI_MAIN_NAMED_OBJECT2_SLI_TABLE_ROW_H
#define NDS_SLI_MAIN_NAMED_OBJECT2_SLI_TABLE_ROW_H

#include <datascript/Types.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/UpdateRegionId.h"

namespace nds
{
namespace sli
{
namespace main
{

class NamedObject2SliTableRow
{
public:
    NamedObject2SliTableRow();
    ~NamedObject2SliTableRow();

    int64_t getNamedObjectId2SliId() const;
    void setNamedObjectId2SliId(int64_t namedObjectId2SliId);
    bool isNullNamedObjectId2SliId() const;
    void setNullNamedObjectId2SliId();

    nds::common::UpdateRegionId getUpdateRegionId() const;
    void setUpdateRegionId(nds::common::UpdateRegionId updateRegionId);
    bool isNullUpdateRegionId() const;
    void setNullUpdateRegionId();

private:
    datascript::InPlaceOptionalHolder<int64_t> m_namedObjectId2SliId;
    datascript::InPlaceOptionalHolder<nds::common::UpdateRegionId> m_updateRegionId;
};

} // namespace main
} // namespace sli
} // namespace nds

#endif // NDS_SLI_MAIN_NAMED_OBJECT2_SLI_TABLE_ROW_H
