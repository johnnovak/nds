/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_DTM_HEIGHTMAP_DTM_HEIGHT_MAP_TILE_PATTERN_TABLE_ROW_H
#define NDS_DTM_HEIGHTMAP_DTM_HEIGHT_MAP_TILE_PATTERN_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/TilePatternId.h"
#include "nds/common/VersionId.h"
#include "nds/dtm/heightmap/SimpleArrayHeightMapData.h"

namespace nds
{
namespace dtm
{
namespace heightmap
{

class DtmHeightMapTilePatternTableRow
{
public:
    DtmHeightMapTilePatternTableRow();
    ~DtmHeightMapTilePatternTableRow();

    nds::common::TilePatternId getId() const;
    void setId(nds::common::TilePatternId id);
    bool isNullId() const;
    void setNullId();

    nds::common::VersionId getVersionId() const;
    void setVersionId(nds::common::VersionId versionId);
    bool isNullVersionId() const;
    void setNullVersionId();

    const nds::dtm::heightmap::SimpleArrayHeightMapData& getNdsData() const;
    void setNdsData(const nds::dtm::heightmap::SimpleArrayHeightMapData& ndsData);
    bool isNullNdsData() const;
    void setNullNdsData();

private:
    datascript::InPlaceOptionalHolder<nds::common::TilePatternId> m_id;
    datascript::InPlaceOptionalHolder<nds::common::VersionId> m_versionId;
    datascript::InPlaceOptionalHolder<nds::dtm::heightmap::SimpleArrayHeightMapData> m_ndsData;
};

} // namespace heightmap
} // namespace dtm
} // namespace nds

#endif // NDS_DTM_HEIGHTMAP_DTM_HEIGHT_MAP_TILE_PATTERN_TABLE_ROW_H
