/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_DTM_SURFACE_RENDER_SURFACE_PART_H
#define NDS_DTM_SURFACE_RENDER_SURFACE_PART_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/BitFieldArray.h>
#include <datascript/OptionalHolder.h>
#include <datascript/Types.h>

#include "nds/objects3d/material/Material.h"

namespace nds
{
namespace dtm
{
namespace surface
{

class RenderSurfacePart
{
public:
    RenderSurfacePart();
    RenderSurfacePart(datascript::BitStreamReader& _in);

    bool getHasMaterial() const;
    void setHasMaterial(bool hasMaterial);

    nds::objects3d::material::Material& getMaterial();
    const nds::objects3d::material::Material& getMaterial() const;
    void setMaterial(const nds::objects3d::material::Material& material);
    bool hasMaterial() const;

    uint8_t getNumIndexBits() const;
    void setNumIndexBits(uint8_t numIndexBits);

    uint32_t getNumIndices() const;
    void setNumIndices(uint32_t numIndices);

    datascript::UInt32Array& getIndicesList();
    const datascript::UInt32Array& getIndicesList() const;
    void setIndicesList(const datascript::UInt32Array& indicesList);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const RenderSurfacePart& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_hasMaterial;
    datascript::OptimizedOptionalHolder<nds::objects3d::material::Material> m_material;
    uint8_t m_numIndexBits;
    uint32_t m_numIndices;
    datascript::UInt32Array m_indicesList;
};

} // namespace surface
} // namespace dtm
} // namespace nds

#endif // NDS_DTM_SURFACE_RENDER_SURFACE_PART_H
