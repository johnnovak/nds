/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_PRODUCTDB_VERSION_TABLE_H
#define NDS_PRODUCTDB_VERSION_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/productdb/VersionTableRow.h"
#include "nds/common/DateTime.h"
#include "nds/common/VersionId.h"

namespace nds
{
namespace productdb
{

class VersionTable
{
public:
    VersionTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~VersionTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<VersionTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<VersionTableRow>& rows) const;
    void write(const std::vector<VersionTableRow>& rows);
    void update(const VersionTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<VersionTableRow>& rows);
    static void writeRow(const VersionTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace productdb
} // namespace nds

#endif // NDS_PRODUCTDB_VERSION_TABLE_H
