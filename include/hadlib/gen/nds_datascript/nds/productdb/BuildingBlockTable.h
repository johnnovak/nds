/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_PRODUCTDB_BUILDING_BLOCK_TABLE_H
#define NDS_PRODUCTDB_BUILDING_BLOCK_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/productdb/BuildingBlockTableRow.h"
#include "nds/common/BuildingBlockDetailedType.h"
#include "nds/common/BuildingBlockId.h"
#include "nds/common/BuildingBlockType.h"
#include "nds/productdb/GlobalBuildingBlockMetadata.h"

namespace nds
{
namespace productdb
{

class BuildingBlockTable
{
public:
    BuildingBlockTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~BuildingBlockTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<BuildingBlockTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<BuildingBlockTableRow>& rows) const;
    void write(const std::vector<BuildingBlockTableRow>& rows);
    void update(const BuildingBlockTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<BuildingBlockTableRow>& rows);
    static void writeRow(const BuildingBlockTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace productdb
} // namespace nds

#endif // NDS_PRODUCTDB_BUILDING_BLOCK_TABLE_H
