/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_PRODUCTDB_COLOR_TABLE_ROW_H
#define NDS_PRODUCTDB_COLOR_TABLE_ROW_H

#include <datascript/Types.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/ColorId.h"

namespace nds
{
namespace productdb
{

class ColorTableRow
{
public:
    ColorTableRow();
    ~ColorTableRow();

    nds::common::ColorId getColorId() const;
    void setColorId(nds::common::ColorId colorId);
    bool isNullColorId() const;
    void setNullColorId();

    uint8_t getRed() const;
    void setRed(uint8_t red);
    bool isNullRed() const;
    void setNullRed();

    uint8_t getGreen() const;
    void setGreen(uint8_t green);
    bool isNullGreen() const;
    void setNullGreen();

    uint8_t getBlue() const;
    void setBlue(uint8_t blue);
    bool isNullBlue() const;
    void setNullBlue();

    uint8_t getAlpha() const;
    void setAlpha(uint8_t alpha);
    bool isNullAlpha() const;
    void setNullAlpha();

private:
    datascript::InPlaceOptionalHolder<nds::common::ColorId> m_colorId;
    datascript::InPlaceOptionalHolder<uint8_t> m_red;
    datascript::InPlaceOptionalHolder<uint8_t> m_green;
    datascript::InPlaceOptionalHolder<uint8_t> m_blue;
    datascript::InPlaceOptionalHolder<uint8_t> m_alpha;
};

} // namespace productdb
} // namespace nds

#endif // NDS_PRODUCTDB_COLOR_TABLE_ROW_H
