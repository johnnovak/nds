/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_PRODUCTDB_GLOBAL_BUILDING_BLOCK_METADATA_H
#define NDS_PRODUCTDB_GLOBAL_BUILDING_BLOCK_METADATA_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/AnyHolder.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/BuildingBlockType.h"
#include "nds/productdb/BmdGlobalMetadata.h"
#include "nds/productdb/PoiGlobalMetadata.h"

namespace nds
{
namespace productdb
{

class GlobalBuildingBlockMetadata
{
public:
    GlobalBuildingBlockMetadata();
    GlobalBuildingBlockMetadata(datascript::BitStreamReader& _in,
        nds::common::BuildingBlockType buildingBlockType);

    GlobalBuildingBlockMetadata(const GlobalBuildingBlockMetadata& _other);
    GlobalBuildingBlockMetadata& operator=(const GlobalBuildingBlockMetadata& _other);

    void initialize(
        nds::common::BuildingBlockType buildingBlockType);

    nds::common::BuildingBlockType getBuildingBlockType() const;

    nds::productdb::PoiGlobalMetadata& getPoiGlobalMetadata();
    const nds::productdb::PoiGlobalMetadata& getPoiGlobalMetadata() const;
    void setPoiGlobalMetadata(const nds::productdb::PoiGlobalMetadata& poiGlobalMetadata);

    nds::productdb::BmdGlobalMetadata& getBmdGlobalMetadata();
    const nds::productdb::BmdGlobalMetadata& getBmdGlobalMetadata() const;
    void setBmdGlobalMetadata(const nds::productdb::BmdGlobalMetadata& bmdGlobalMetadata);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const GlobalBuildingBlockMetadata& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::BuildingBlockType m_buildingBlockType;
    bool m_isInitialized;
    datascript::AnyHolder m_objectChoice;
};

} // namespace productdb
} // namespace nds

#endif // NDS_PRODUCTDB_GLOBAL_BUILDING_BLOCK_METADATA_H
