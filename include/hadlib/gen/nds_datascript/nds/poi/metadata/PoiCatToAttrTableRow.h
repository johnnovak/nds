/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_METADATA_POI_CAT_TO_ATTR_TABLE_ROW_H
#define NDS_POI_METADATA_POI_CAT_TO_ATTR_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/poi/main/CategoryId.h"
#include "nds/poi/main/PoiCatCollectionId.h"
#include "nds/poi/optattr/AttributeTypeId.h"

namespace nds
{
namespace poi
{
namespace metadata
{

class PoiCatToAttrTableRow
{
public:
    PoiCatToAttrTableRow();
    ~PoiCatToAttrTableRow();

    nds::poi::main::PoiCatCollectionId getCatCollectionId() const;
    void setCatCollectionId(nds::poi::main::PoiCatCollectionId catCollectionId);
    bool isNullCatCollectionId() const;
    void setNullCatCollectionId();

    nds::poi::main::CategoryId getCatId() const;
    void setCatId(nds::poi::main::CategoryId catId);
    bool isNullCatId() const;
    void setNullCatId();

    nds::poi::optattr::AttributeTypeId getAttrId() const;
    void setAttrId(nds::poi::optattr::AttributeTypeId attrId);
    bool isNullAttrId() const;
    void setNullAttrId();

private:
    datascript::InPlaceOptionalHolder<nds::poi::main::PoiCatCollectionId> m_catCollectionId;
    datascript::InPlaceOptionalHolder<nds::poi::main::CategoryId> m_catId;
    datascript::InPlaceOptionalHolder<nds::poi::optattr::AttributeTypeId> m_attrId;
};

} // namespace metadata
} // namespace poi
} // namespace nds

#endif // NDS_POI_METADATA_POI_CAT_TO_ATTR_TABLE_ROW_H
