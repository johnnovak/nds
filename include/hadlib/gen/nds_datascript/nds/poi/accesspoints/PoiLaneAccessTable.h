/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_ACCESSPOINTS_POI_LANE_ACCESS_TABLE_H
#define NDS_POI_ACCESSPOINTS_POI_LANE_ACCESS_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/poi/accesspoints/PoiLaneAccessTableRow.h"
#include "nds/common/PackedTileId.h"
#include "nds/common/flexattr/attrmaps/FeatureType.h"
#include "nds/lane/attrdefs/LaneGroupId.h"
#include "nds/lane/attrdefs/LaneRange.h"
#include "nds/poi/main/PoiId.h"

namespace nds
{
namespace poi
{
namespace accesspoints
{

class PoiLaneAccessTable
{
public:
    PoiLaneAccessTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~PoiLaneAccessTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<PoiLaneAccessTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<PoiLaneAccessTableRow>& rows) const;
    void write(const std::vector<PoiLaneAccessTableRow>& rows);
    void update(const PoiLaneAccessTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<PoiLaneAccessTableRow>& rows);
    static void writeRow(const PoiLaneAccessTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace accesspoints
} // namespace poi
} // namespace nds

#endif // NDS_POI_ACCESSPOINTS_POI_LANE_ACCESS_TABLE_H
