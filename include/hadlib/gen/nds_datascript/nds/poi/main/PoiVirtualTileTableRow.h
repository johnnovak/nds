/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_MAIN_POI_VIRTUAL_TILE_TABLE_ROW_H
#define NDS_POI_MAIN_POI_VIRTUAL_TILE_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/PackedTileId.h"
#include "nds/common/VersionId.h"
#include "nds/poi/main/PoiId.h"

namespace nds
{
namespace poi
{
namespace main
{

class PoiVirtualTileTableRow
{
public:
    PoiVirtualTileTableRow();
    ~PoiVirtualTileTableRow();

    nds::common::PackedTileId getTileId() const;
    void setTileId(nds::common::PackedTileId tileId);
    bool isNullTileId() const;
    void setNullTileId();

    nds::poi::main::PoiId getMinId() const;
    void setMinId(nds::poi::main::PoiId minId);
    bool isNullMinId() const;
    void setNullMinId();

    nds::poi::main::PoiId getMaxId() const;
    void setMaxId(nds::poi::main::PoiId maxId);
    bool isNullMaxId() const;
    void setNullMaxId();

    nds::common::VersionId getVersionId() const;
    void setVersionId(nds::common::VersionId versionId);
    bool isNullVersionId() const;
    void setNullVersionId();

    bool getIsDirty() const;
    void setIsDirty(bool isDirty);
    bool isNullIsDirty() const;
    void setNullIsDirty();

private:
    datascript::InPlaceOptionalHolder<nds::common::PackedTileId> m_tileId;
    datascript::InPlaceOptionalHolder<nds::poi::main::PoiId> m_minId;
    datascript::InPlaceOptionalHolder<nds::poi::main::PoiId> m_maxId;
    datascript::InPlaceOptionalHolder<nds::common::VersionId> m_versionId;
    datascript::InPlaceOptionalHolder<bool> m_isDirty;
};

} // namespace main
} // namespace poi
} // namespace nds

#endif // NDS_POI_MAIN_POI_VIRTUAL_TILE_TABLE_ROW_H
