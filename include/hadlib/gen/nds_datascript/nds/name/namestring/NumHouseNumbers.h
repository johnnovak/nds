/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_NAME_NAMESTRING_NUM_HOUSE_NUMBERS_H
#define NDS_NAME_NAMESTRING_NUM_HOUSE_NUMBERS_H

#include <datascript/Types.h>

namespace nds
{
namespace name
{
namespace namestring
{

typedef uint16_t NumHouseNumbers;

} // namespace namestring
} // namespace name
} // namespace nds

#endif // NDS_NAME_NAMESTRING_NUM_HOUSE_NUMBERS_H
