/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_NAME_NAMESTRING_ADDRESS_POINT_COLLECTION_H
#define NDS_NAME_NAMESTRING_ADDRESS_POINT_COLLECTION_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/ObjectArray.h>

#include "nds/name/namestring/AddressPoint.h"
#include "nds/name/namestring/NumAddressPoints.h"

namespace nds
{
namespace name
{
namespace namestring
{

class AddressPointCollection
{
public:
    AddressPointCollection();
    AddressPointCollection(datascript::BitStreamReader& _in);

    void initializeChildren();

    nds::name::namestring::NumAddressPoints getNumAddressPoints() const;
    void setNumAddressPoints(nds::name::namestring::NumAddressPoints numAddressPoints);

    datascript::ObjectArray<nds::name::namestring::AddressPoint>& getAddressPoint();
    const datascript::ObjectArray<nds::name::namestring::AddressPoint>& getAddressPoint() const;
    void setAddressPoint(const datascript::ObjectArray<nds::name::namestring::AddressPoint>& addressPoint);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const AddressPointCollection& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::name::namestring::NumAddressPoints m_numAddressPoints;
    datascript::ObjectArray<nds::name::namestring::AddressPoint> m_addressPoint;
};

} // namespace namestring
} // namespace name
} // namespace nds

#endif // NDS_NAME_NAMESTRING_ADDRESS_POINT_COLLECTION_H
