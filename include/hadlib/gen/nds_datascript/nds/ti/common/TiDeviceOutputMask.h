/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_COMMON_TI_DEVICE_OUTPUT_MASK_H
#define NDS_TI_COMMON_TI_DEVICE_OUTPUT_MASK_H

#include <datascript/Types.h>

namespace nds
{
namespace ti
{
namespace common
{

typedef uint8_t TiDeviceOutputMask;

} // namespace common
} // namespace ti
} // namespace nds

#endif // NDS_TI_COMMON_TI_DEVICE_OUTPUT_MASK_H
