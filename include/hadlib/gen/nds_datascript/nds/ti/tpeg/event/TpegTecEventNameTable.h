/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TPEG_EVENT_TPEG_TEC_EVENT_NAME_TABLE_H
#define NDS_TI_TPEG_EVENT_TPEG_TEC_EVENT_NAME_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ti/tpeg/event/TpegTecEventNameTableRow.h"
#include "nds/common/LanguageCode.h"
#include "nds/icondata/main/IconSetId.h"
#include "nds/ti/common/TiEventTextQuantifier.h"
#include "nds/ti/common/TiNameId.h"
#include "nds/ti/tpeg/common/TpegEventCode.h"
#include "nds/ti/tpeg/common/TpegEventText.h"
#include "nds/ti/tpeg/common/TpegTecTableName.h"

namespace nds
{
namespace ti
{
namespace tpeg
{
namespace event
{

class TpegTecEventNameTable
{
public:
    TpegTecEventNameTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TpegTecEventNameTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TpegTecEventNameTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TpegTecEventNameTableRow>& rows) const;
    void write(const std::vector<TpegTecEventNameTableRow>& rows);
    void update(const TpegTecEventNameTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TpegTecEventNameTableRow>& rows);
    static void writeRow(const TpegTecEventNameTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace event
} // namespace tpeg
} // namespace ti
} // namespace nds

#endif // NDS_TI_TPEG_EVENT_TPEG_TEC_EVENT_NAME_TABLE_H
