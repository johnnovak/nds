/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_OVERVIEWMAP_TRAFFIC_SEGMENTS_MAP_H
#define NDS_TI_OVERVIEWMAP_TRAFFIC_SEGMENTS_MAP_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/ti/overviewmap/TiOverviewMapImageFormat.h"
#include "nds/ti/overviewmap/Tmc2PngMap.h"
#include "nds/ti/overviewmap/Tmc2SvgMap.h"

namespace nds
{
namespace ti
{
namespace overviewmap
{

class TrafficSegmentsMap
{
public:
    TrafficSegmentsMap();
    TrafficSegmentsMap(datascript::BitStreamReader& _in,
        nds::ti::overviewmap::TiOverviewMapImageFormat imageFormat);

    TrafficSegmentsMap(const TrafficSegmentsMap& _other);
    TrafficSegmentsMap& operator=(const TrafficSegmentsMap& _other);

    void initialize(
        nds::ti::overviewmap::TiOverviewMapImageFormat imageFormat);

    nds::ti::overviewmap::TiOverviewMapImageFormat getImageFormat() const;

    nds::ti::overviewmap::Tmc2SvgMap& getTmc2SvgMap();
    const nds::ti::overviewmap::Tmc2SvgMap& getTmc2SvgMap() const;
    void setTmc2SvgMap(const nds::ti::overviewmap::Tmc2SvgMap& tmc2SvgMap);
    bool hasTmc2SvgMap() const;

    nds::ti::overviewmap::Tmc2PngMap& getTmc2PngMap();
    const nds::ti::overviewmap::Tmc2PngMap& getTmc2PngMap() const;
    void setTmc2PngMap(const nds::ti::overviewmap::Tmc2PngMap& tmc2PngMap);
    bool hasTmc2PngMap() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const TrafficSegmentsMap& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::ti::overviewmap::TiOverviewMapImageFormat m_imageFormat;
    bool m_isInitialized;
    datascript::OptimizedOptionalHolder<nds::ti::overviewmap::Tmc2SvgMap> m_tmc2SvgMap;
    datascript::OptimizedOptionalHolder<nds::ti::overviewmap::Tmc2PngMap> m_tmc2PngMap;
};

} // namespace overviewmap
} // namespace ti
} // namespace nds

#endif // NDS_TI_OVERVIEWMAP_TRAFFIC_SEGMENTS_MAP_H
