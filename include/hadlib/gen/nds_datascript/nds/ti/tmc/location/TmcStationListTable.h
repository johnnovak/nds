/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TMC_LOCATION_TMC_STATION_LIST_TABLE_H
#define NDS_TI_TMC_LOCATION_TMC_STATION_LIST_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ti/tmc/location/TmcStationListTableRow.h"
#include "nds/ti/common/TIServiceDataType.h"
#include "nds/ti/common/TiChannel.h"
#include "nds/ti/common/TiNameId.h"
#include "nds/ti/tmc/common/TmcLocationTableId.h"
#include "nds/ti/tmc/common/TmcStationId.h"

namespace nds
{
namespace ti
{
namespace tmc
{
namespace location
{

class TmcStationListTable
{
public:
    TmcStationListTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TmcStationListTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TmcStationListTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TmcStationListTableRow>& rows) const;
    void write(const std::vector<TmcStationListTableRow>& rows);
    void update(const TmcStationListTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TmcStationListTableRow>& rows);
    static void writeRow(const TmcStationListTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace location
} // namespace tmc
} // namespace ti
} // namespace nds

#endif // NDS_TI_TMC_LOCATION_TMC_STATION_LIST_TABLE_H
