/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TMC_LOCATION_TMC_CROSS_REFERENCE_TABLE_ROW_H
#define NDS_TI_TMC_LOCATION_TMC_CROSS_REFERENCE_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/ti/tmc/common/TmcLocationId.h"

namespace nds
{
namespace ti
{
namespace tmc
{
namespace location
{

class TmcCrossReferenceTableRow
{
public:
    TmcCrossReferenceTableRow();
    ~TmcCrossReferenceTableRow();

    nds::ti::tmc::common::TmcLocationId getLocIdRoadOne() const;
    void setLocIdRoadOne(nds::ti::tmc::common::TmcLocationId locIdRoadOne);
    bool isNullLocIdRoadOne() const;
    void setNullLocIdRoadOne();

    nds::ti::tmc::common::TmcLocationId getLocIdRoadTwo() const;
    void setLocIdRoadTwo(nds::ti::tmc::common::TmcLocationId locIdRoadTwo);
    bool isNullLocIdRoadTwo() const;
    void setNullLocIdRoadTwo();

private:
    datascript::InPlaceOptionalHolder<nds::ti::tmc::common::TmcLocationId> m_locIdRoadOne;
    datascript::InPlaceOptionalHolder<nds::ti::tmc::common::TmcLocationId> m_locIdRoadTwo;
};

} // namespace location
} // namespace tmc
} // namespace ti
} // namespace nds

#endif // NDS_TI_TMC_LOCATION_TMC_CROSS_REFERENCE_TABLE_ROW_H
