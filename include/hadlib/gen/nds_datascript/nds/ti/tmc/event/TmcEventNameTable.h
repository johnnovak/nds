/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TMC_EVENT_TMC_EVENT_NAME_TABLE_H
#define NDS_TI_TMC_EVENT_TMC_EVENT_NAME_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ti/tmc/event/TmcEventNameTableRow.h"
#include "nds/common/LanguageCode.h"
#include "nds/ti/common/TiEventTextQuantifier.h"
#include "nds/ti/common/TiNameId.h"
#include "nds/ti/tmc/common/TmcEventCode.h"

namespace nds
{
namespace ti
{
namespace tmc
{
namespace event
{

class TmcEventNameTable
{
public:
    TmcEventNameTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TmcEventNameTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TmcEventNameTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TmcEventNameTableRow>& rows) const;
    void write(const std::vector<TmcEventNameTableRow>& rows);
    void update(const TmcEventNameTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TmcEventNameTableRow>& rows);
    static void writeRow(const TmcEventNameTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace event
} // namespace tmc
} // namespace ti
} // namespace nds

#endif // NDS_TI_TMC_EVENT_TMC_EVENT_NAME_TABLE_H
