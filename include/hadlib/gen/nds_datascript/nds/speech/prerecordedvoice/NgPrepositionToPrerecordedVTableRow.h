/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PRERECORDEDVOICE_NG_PREPOSITION_TO_PRERECORDED_VTABLE_ROW_H
#define NDS_SPEECH_PRERECORDEDVOICE_NG_PREPOSITION_TO_PRERECORDED_VTABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/ng/main/PrepositionId.h"
#include "nds/speech/common/PrerecordedVoiceId.h"
#include "nds/speech/common/SpeechType.h"

namespace nds
{
namespace speech
{
namespace prerecordedvoice
{

class NgPrepositionToPrerecordedVTableRow
{
public:
    NgPrepositionToPrerecordedVTableRow();
    ~NgPrepositionToPrerecordedVTableRow();

    nds::ng::main::PrepositionId getPrepositionId() const;
    void setPrepositionId(nds::ng::main::PrepositionId prepositionId);
    bool isNullPrepositionId() const;
    void setNullPrepositionId();

    nds::speech::common::SpeechType getPrerecordedVoiceType() const;
    void setPrerecordedVoiceType(nds::speech::common::SpeechType prerecordedVoiceType);
    bool isNullPrerecordedVoiceType() const;
    void setNullPrerecordedVoiceType();

    nds::speech::common::PrerecordedVoiceId getPrerecordedVoiceId() const;
    void setPrerecordedVoiceId(nds::speech::common::PrerecordedVoiceId prerecordedVoiceId);
    bool isNullPrerecordedVoiceId() const;
    void setNullPrerecordedVoiceId();

private:
    datascript::InPlaceOptionalHolder<nds::ng::main::PrepositionId> m_prepositionId;
    datascript::InPlaceOptionalHolder<nds::speech::common::SpeechType> m_prerecordedVoiceType;
    datascript::InPlaceOptionalHolder<nds::speech::common::PrerecordedVoiceId> m_prerecordedVoiceId;
};

} // namespace prerecordedvoice
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PRERECORDEDVOICE_NG_PREPOSITION_TO_PRERECORDED_VTABLE_ROW_H
