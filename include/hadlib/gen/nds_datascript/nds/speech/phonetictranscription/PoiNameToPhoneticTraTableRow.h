/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PHONETICTRANSCRIPTION_POI_NAME_TO_PHONETIC_TRA_TABLE_ROW_H
#define NDS_SPEECH_PHONETICTRANSCRIPTION_POI_NAME_TO_PHONETIC_TRA_TABLE_ROW_H

#include <datascript/Types.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/LanguageCode.h"
#include "nds/name/namestring/NameStringId.h"
#include "nds/speech/common/SpeechType.h"
#include "nds/speech/phonetictranscription/PhoneticTranscriptionId.h"

namespace nds
{
namespace speech
{
namespace phonetictranscription
{

class PoiNameToPhoneticTraTableRow
{
public:
    PoiNameToPhoneticTraTableRow();
    ~PoiNameToPhoneticTraTableRow();

    nds::name::namestring::NameStringId getNameStringCollectionId() const;
    void setNameStringCollectionId(nds::name::namestring::NameStringId nameStringCollectionId);
    bool isNullNameStringCollectionId() const;
    void setNullNameStringCollectionId();

    uint32_t getSeqNr() const;
    void setSeqNr(uint32_t seqNr);
    bool isNullSeqNr() const;
    void setNullSeqNr();

    nds::common::LanguageCode getLanguageCode() const;
    void setLanguageCode(nds::common::LanguageCode languageCode);
    bool isNullLanguageCode() const;
    void setNullLanguageCode();

    nds::speech::common::SpeechType getPhoneticTraType() const;
    void setPhoneticTraType(nds::speech::common::SpeechType phoneticTraType);
    bool isNullPhoneticTraType() const;
    void setNullPhoneticTraType();

    uint8_t getOrderOfPreference() const;
    void setOrderOfPreference(uint8_t orderOfPreference);
    bool isNullOrderOfPreference() const;
    void setNullOrderOfPreference();

    nds::speech::phonetictranscription::PhoneticTranscriptionId getPhoneticTranscriptionId() const;
    void setPhoneticTranscriptionId(nds::speech::phonetictranscription::PhoneticTranscriptionId phoneticTranscriptionId);
    bool isNullPhoneticTranscriptionId() const;
    void setNullPhoneticTranscriptionId();

private:
    datascript::InPlaceOptionalHolder<nds::name::namestring::NameStringId> m_nameStringCollectionId;
    datascript::InPlaceOptionalHolder<uint32_t> m_seqNr;
    datascript::InPlaceOptionalHolder<nds::common::LanguageCode> m_languageCode;
    datascript::InPlaceOptionalHolder<nds::speech::common::SpeechType> m_phoneticTraType;
    datascript::InPlaceOptionalHolder<uint8_t> m_orderOfPreference;
    datascript::InPlaceOptionalHolder<nds::speech::phonetictranscription::PhoneticTranscriptionId> m_phoneticTranscriptionId;
};

} // namespace phonetictranscription
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PHONETICTRANSCRIPTION_POI_NAME_TO_PHONETIC_TRA_TABLE_ROW_H
