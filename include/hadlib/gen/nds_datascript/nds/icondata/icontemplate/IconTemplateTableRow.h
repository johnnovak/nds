/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ICONDATA_ICONTEMPLATE_ICON_TEMPLATE_TABLE_ROW_H
#define NDS_ICONDATA_ICONTEMPLATE_ICON_TEMPLATE_TABLE_ROW_H

#include <datascript/Types.h>
#include <string>
#include <datascript/OptionalHolder.h>

#include "nds/icondata/icontemplate/IconTemplateSetId.h"
#include "nds/icondata/main/IconId.h"
#include "nds/icondata/main/ImageAnchorPos.h"
#include "nds/icondata/main/UsageTypeMask.h"

namespace nds
{
namespace icondata
{
namespace icontemplate
{

class IconTemplateTableRow
{
public:
    IconTemplateTableRow();
    ~IconTemplateTableRow();

    nds::icondata::icontemplate::IconTemplateSetId getIconTemplateSetId() const;
    void setIconTemplateSetId(nds::icondata::icontemplate::IconTemplateSetId iconTemplateSetId);
    bool isNullIconTemplateSetId() const;
    void setNullIconTemplateSetId();

    nds::icondata::main::UsageTypeMask getUsageTypeMask() const;
    void setUsageTypeMask(nds::icondata::main::UsageTypeMask usageTypeMask);
    bool isNullUsageTypeMask() const;
    void setNullUsageTypeMask();

    const std::string& getTemplateAttributeKey() const;
    void setTemplateAttributeKey(const std::string& templateAttributeKey);
    bool isNullTemplateAttributeKey() const;
    void setNullTemplateAttributeKey();

    const std::string& getTemplateAttributeValue() const;
    void setTemplateAttributeValue(const std::string& templateAttributeValue);
    bool isNullTemplateAttributeValue() const;
    void setNullTemplateAttributeValue();

    int8_t getDrawingLayer() const;
    void setDrawingLayer(int8_t drawingLayer);
    bool isNullDrawingLayer() const;
    void setNullDrawingLayer();

    const nds::icondata::main::ImageAnchorPos& getTemplateConnectionPoint() const;
    void setTemplateConnectionPoint(const nds::icondata::main::ImageAnchorPos& templateConnectionPoint);
    bool isNullTemplateConnectionPoint() const;
    void setNullTemplateConnectionPoint();

    nds::icondata::main::IconId getIconId() const;
    void setIconId(nds::icondata::main::IconId iconId);
    bool isNullIconId() const;
    void setNullIconId();

private:
    datascript::InPlaceOptionalHolder<nds::icondata::icontemplate::IconTemplateSetId> m_iconTemplateSetId;
    datascript::InPlaceOptionalHolder<nds::icondata::main::UsageTypeMask> m_usageTypeMask;
    datascript::InPlaceOptionalHolder<std::string> m_templateAttributeKey;
    datascript::InPlaceOptionalHolder<std::string> m_templateAttributeValue;
    datascript::InPlaceOptionalHolder<int8_t> m_drawingLayer;
    datascript::InPlaceOptionalHolder<nds::icondata::main::ImageAnchorPos> m_templateConnectionPoint;
    datascript::InPlaceOptionalHolder<nds::icondata::main::IconId> m_iconId;
};

} // namespace icontemplate
} // namespace icondata
} // namespace nds

#endif // NDS_ICONDATA_ICONTEMPLATE_ICON_TEMPLATE_TABLE_ROW_H
