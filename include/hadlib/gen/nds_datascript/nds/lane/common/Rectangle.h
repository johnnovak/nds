/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LANE_COMMON_RECTANGLE_H
#define NDS_LANE_COMMON_RECTANGLE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/geometry3d/Vector3D.h"

namespace nds
{
namespace lane
{
namespace common
{

class Rectangle
{
public:
    Rectangle();
    Rectangle(datascript::BitStreamReader& _in);

    Rectangle(const Rectangle& _other);
    Rectangle& operator=(const Rectangle& _other);

    void initializeChildren();

    nds::common::geometry3d::Vector3D& getSw();
    const nds::common::geometry3d::Vector3D& getSw() const;
    void setSw(const nds::common::geometry3d::Vector3D& sw);

    nds::common::geometry3d::Vector3D& getNe();
    const nds::common::geometry3d::Vector3D& getNe() const;
    void setNe(const nds::common::geometry3d::Vector3D& ne);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const Rectangle& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_areChildrenInitialized;
    datascript::InPlaceOptionalHolder<nds::common::geometry3d::Vector3D> m_sw;
    datascript::InPlaceOptionalHolder<nds::common::geometry3d::Vector3D> m_ne;
};

} // namespace common
} // namespace lane
} // namespace nds

#endif // NDS_LANE_COMMON_RECTANGLE_H
