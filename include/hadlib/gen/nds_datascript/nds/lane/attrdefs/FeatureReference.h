/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LANE_ATTRDEFS_FEATURE_REFERENCE_H
#define NDS_LANE_ATTRDEFS_FEATURE_REFERENCE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/IntOrExtDirectedLinkReference.h"
#include "nds/common/IntOrExtDirectedRoadGeoLineReference.h"
#include "nds/common/flexattr/attrdefs/ConnectLinkType.h"

namespace nds
{
namespace lane
{
namespace attrdefs
{

class FeatureReference
{
public:
    FeatureReference();
    FeatureReference(datascript::BitStreamReader& _in);

    void initializeChildren();

    nds::common::flexattr::attrdefs::ConnectLinkType getGuidanceFeatureType() const;
    void setGuidanceFeatureType(nds::common::flexattr::attrdefs::ConnectLinkType guidanceFeatureType);

    nds::common::IntOrExtDirectedLinkReference& getBaseLinkReference();
    const nds::common::IntOrExtDirectedLinkReference& getBaseLinkReference() const;
    void setBaseLinkReference(const nds::common::IntOrExtDirectedLinkReference& baseLinkReference);
    bool hasBaseLinkReference() const;

    nds::common::IntOrExtDirectedRoadGeoLineReference& getRoadGeoLineReference();
    const nds::common::IntOrExtDirectedRoadGeoLineReference& getRoadGeoLineReference() const;
    void setRoadGeoLineReference(const nds::common::IntOrExtDirectedRoadGeoLineReference& roadGeoLineReference);
    bool hasRoadGeoLineReference() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const FeatureReference& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::flexattr::attrdefs::ConnectLinkType m_guidanceFeatureType;
    datascript::OptimizedOptionalHolder<nds::common::IntOrExtDirectedLinkReference> m_baseLinkReference;
    datascript::OptimizedOptionalHolder<nds::common::IntOrExtDirectedRoadGeoLineReference> m_roadGeoLineReference;
};

} // namespace attrdefs
} // namespace lane
} // namespace nds

#endif // NDS_LANE_ATTRDEFS_FEATURE_REFERENCE_H
