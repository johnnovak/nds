/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_LINK_POSITION_REFERENCE_H
#define NDS_COMMON_LINK_POSITION_REFERENCE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/LinkId.h"
#include "nds/common/geometry/LinePosition.h"

namespace nds
{
namespace common
{

class LinkPositionReference
{
public:
    LinkPositionReference();
    LinkPositionReference(datascript::BitStreamReader& _in);

    nds::common::LinkId getLinkId() const;
    void setLinkId(nds::common::LinkId linkId);

    nds::common::geometry::LinePosition& getPosition();
    const nds::common::geometry::LinePosition& getPosition() const;
    void setPosition(const nds::common::geometry::LinePosition& position);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const LinkPositionReference& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::LinkId m_linkId;
    datascript::InPlaceOptionalHolder<nds::common::geometry::LinePosition> m_position;
};

} // namespace common
} // namespace nds

#endif // NDS_COMMON_LINK_POSITION_REFERENCE_H
