/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_TEXTURE_TEXTURE_MAP_COLLECTION_TABLE_ROW_H
#define NDS_COMMON_TEXTURE_TEXTURE_MAP_COLLECTION_TABLE_ROW_H

#include <string>
#include <datascript/OptionalHolder.h>

#include "nds/common/VersionId.h"
#include "nds/common/texture/TextureMapCollectionId.h"

namespace nds
{
namespace common
{
namespace texture
{

class TextureMapCollectionTableRow
{
public:
    TextureMapCollectionTableRow();
    ~TextureMapCollectionTableRow();

    nds::common::texture::TextureMapCollectionId getCollectionId() const;
    void setCollectionId(nds::common::texture::TextureMapCollectionId collectionId);
    bool isNullCollectionId() const;
    void setNullCollectionId();

    nds::common::VersionId getVersionId() const;
    void setVersionId(nds::common::VersionId versionId);
    bool isNullVersionId() const;
    void setNullVersionId();

    const std::string& getCollectionName() const;
    void setCollectionName(const std::string& collectionName);
    bool isNullCollectionName() const;
    void setNullCollectionName();

private:
    datascript::InPlaceOptionalHolder<nds::common::texture::TextureMapCollectionId> m_collectionId;
    datascript::InPlaceOptionalHolder<nds::common::VersionId> m_versionId;
    datascript::InPlaceOptionalHolder<std::string> m_collectionName;
};

} // namespace texture
} // namespace common
} // namespace nds

#endif // NDS_COMMON_TEXTURE_TEXTURE_MAP_COLLECTION_TABLE_ROW_H
