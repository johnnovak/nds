/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_REGION_METADATA_ID_H
#define NDS_COMMON_REGION_METADATA_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace common
{

typedef int16_t RegionMetadataId;

} // namespace common
} // namespace nds

#endif // NDS_COMMON_REGION_METADATA_ID_H
