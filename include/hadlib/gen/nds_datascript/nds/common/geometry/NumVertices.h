/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_GEOMETRY_NUM_VERTICES_H
#define NDS_COMMON_GEOMETRY_NUM_VERTICES_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>
#include <datascript/Types.h>

#include "nds/common/geometry/ExtNumVertices.h"

namespace nds
{
namespace common
{
namespace geometry
{

class NumVertices
{
public:
    NumVertices();
    NumVertices(datascript::BitStreamReader& _in);

    uint8_t getNumVx4() const;
    void setNumVx4(uint8_t numVx4);

    nds::common::geometry::ExtNumVertices& getExtNumVx();
    const nds::common::geometry::ExtNumVertices& getExtNumVx() const;
    void setExtNumVx(const nds::common::geometry::ExtNumVertices& extNumVx);
    bool hasExtNumVx() const;

    uint16_t value() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const NumVertices& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint8_t m_numVx4;
    datascript::OptimizedOptionalHolder<nds::common::geometry::ExtNumVertices> m_extNumVx;
};

} // namespace geometry
} // namespace common
} // namespace nds

#endif // NDS_COMMON_GEOMETRY_NUM_VERTICES_H
