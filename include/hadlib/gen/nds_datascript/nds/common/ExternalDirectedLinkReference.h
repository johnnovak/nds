/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_EXTERNAL_DIRECTED_LINK_REFERENCE_H
#define NDS_COMMON_EXTERNAL_DIRECTED_LINK_REFERENCE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/DirectedLinkReference.h"
#include "nds/common/SizeOfExternalTileIdList.h"

namespace nds
{
namespace common
{

class ExternalDirectedLinkReference
{
public:
    ExternalDirectedLinkReference();
    ExternalDirectedLinkReference(datascript::BitStreamReader& _in);

    nds::common::SizeOfExternalTileIdList getExtTileIdx() const;
    void setExtTileIdx(nds::common::SizeOfExternalTileIdList extTileIdx);

    nds::common::DirectedLinkReference& getTileExternalLinkReference();
    const nds::common::DirectedLinkReference& getTileExternalLinkReference() const;
    void setTileExternalLinkReference(const nds::common::DirectedLinkReference& tileExternalLinkReference);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const ExternalDirectedLinkReference& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::SizeOfExternalTileIdList m_extTileIdx;
    datascript::InPlaceOptionalHolder<nds::common::DirectedLinkReference> m_tileExternalLinkReference;
};

} // namespace common
} // namespace nds

#endif // NDS_COMMON_EXTERNAL_DIRECTED_LINK_REFERENCE_H
