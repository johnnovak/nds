/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_GEOMETRY3D_NUM_DTM_SURFACES_H
#define NDS_COMMON_GEOMETRY3D_NUM_DTM_SURFACES_H

#include <datascript/Types.h>

namespace nds
{
namespace common
{
namespace geometry3d
{

typedef uint16_t NumDtmSurfaces;

} // namespace geometry3d
} // namespace common
} // namespace nds

#endif // NDS_COMMON_GEOMETRY3D_NUM_DTM_SURFACES_H
