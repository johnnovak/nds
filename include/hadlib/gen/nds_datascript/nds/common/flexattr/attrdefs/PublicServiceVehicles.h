/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_PUBLIC_SERVICE_VEHICLES_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_PUBLIC_SERVICE_VEHICLES_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class PublicServiceVehicles
{
public:
    PublicServiceVehicles();
    PublicServiceVehicles(datascript::BitStreamReader& _in);

    bool getIsPublicBus() const;
    void setIsPublicBus(bool isPublicBus);

    bool getIsSchoolBus() const;
    void setIsSchoolBus(bool isSchoolBus);

    bool getIsTrolley() const;
    void setIsTrolley(bool isTrolley);

    bool getIsTram() const;
    void setIsTram(bool isTram);

    bool getIsTaxi() const;
    void setIsTaxi(bool isTaxi);

    bool getIsEmergency() const;
    void setIsEmergency(bool isEmergency);

    bool getIsMail() const;
    void setIsMail(bool isMail);

    bool getIsInclusive() const;
    void setIsInclusive(bool isInclusive);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const PublicServiceVehicles& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_isPublicBus;
    bool m_isSchoolBus;
    bool m_isTrolley;
    bool m_isTram;
    bool m_isTaxi;
    bool m_isEmergency;
    bool m_isMail;
    bool m_isInclusive;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_PUBLIC_SERVICE_VEHICLES_H
