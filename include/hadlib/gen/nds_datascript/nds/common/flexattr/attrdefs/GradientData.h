/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_GRADIENT_DATA_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_GRADIENT_DATA_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>
#include <datascript/Types.h>

#include "nds/common/geometry/ExtNumVertices.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class GradientData
{
public:
    GradientData();
    GradientData(datascript::BitStreamReader& _in);

    bool getHasRadiusValue() const;
    void setHasRadiusValue(bool hasRadiusValue);

    nds::common::geometry::ExtNumVertices& getChainageOffset();
    const nds::common::geometry::ExtNumVertices& getChainageOffset() const;
    void setChainageOffset(const nds::common::geometry::ExtNumVertices& chainageOffset);

    int8_t getRelativeGradeLineElevation() const;
    void setRelativeGradeLineElevation(int8_t relativeGradeLineElevation);

    uint8_t getRadiusValue() const;
    void setRadiusValue(uint8_t radiusValue);
    bool hasRadiusValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const GradientData& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_hasRadiusValue;
    datascript::InPlaceOptionalHolder<nds::common::geometry::ExtNumVertices> m_chainageOffset;
    int8_t m_relativeGradeLineElevation;
    datascript::InPlaceOptionalHolder<uint8_t> m_radiusValue;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_GRADIENT_DATA_H
