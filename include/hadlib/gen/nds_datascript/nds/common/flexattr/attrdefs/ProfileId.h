/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_PROFILE_ID_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_PROFILE_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

typedef int16_t ProfileId;

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_PROFILE_ID_H
