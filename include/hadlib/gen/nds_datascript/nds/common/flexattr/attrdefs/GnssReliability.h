/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_GNSS_RELIABILITY_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_GNSS_RELIABILITY_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class GnssReliability
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_GnssReliability
    {
        VERY_BAD = UINT8_C(0),
        BAD = UINT8_C(1),
        MEDIUM = UINT8_C(2),
        GOOD = UINT8_C(3),
        VERY_GOOD = UINT8_C(4)
    };

    GnssReliability();
    GnssReliability(e_GnssReliability value);
    explicit GnssReliability(datascript::BitStreamReader& _in);

    operator e_GnssReliability() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const GnssReliability& other) const;
    bool operator==(e_GnssReliability other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static GnssReliability toEnum(uint8_t rawValue);

private:
    e_GnssReliability m_value;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_GNSS_RELIABILITY_H
