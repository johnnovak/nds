/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_NUM_CONNECTED_LANES_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_NUM_CONNECTED_LANES_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class NumConnectedLanes
{
public:
    NumConnectedLanes();
    NumConnectedLanes(datascript::BitStreamReader& _in);

    uint8_t getNumLanes() const;
    void setNumLanes(uint8_t numLanes);

    uint8_t value() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const NumConnectedLanes& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint8_t m_numLanes;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_NUM_CONNECTED_LANES_H
