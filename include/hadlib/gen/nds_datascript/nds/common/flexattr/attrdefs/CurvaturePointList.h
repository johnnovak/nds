/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_CURVATURE_POINT_LIST_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_CURVATURE_POINT_LIST_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/ObjectArray.h>
#include <datascript/Types.h>

#include "nds/common/flexattr/attrdefs/CurvaturePoint.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class CurvaturePointList
{
public:
    CurvaturePointList();
    CurvaturePointList(datascript::BitStreamReader& _in);

    uint16_t getNumOfValues() const;
    void setNumOfValues(uint16_t numOfValues);

    datascript::ObjectArray<nds::common::flexattr::attrdefs::CurvaturePoint>& getCurvaturePoint();
    const datascript::ObjectArray<nds::common::flexattr::attrdefs::CurvaturePoint>& getCurvaturePoint() const;
    void setCurvaturePoint(const datascript::ObjectArray<nds::common::flexattr::attrdefs::CurvaturePoint>& curvaturePoint);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const CurvaturePointList& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint16_t m_numOfValues;
    datascript::ObjectArray<nds::common::flexattr::attrdefs::CurvaturePoint> m_curvaturePoint;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_CURVATURE_POINT_LIST_H
