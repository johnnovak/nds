/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_OFFICIAL_LANGUAGES_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_OFFICIAL_LANGUAGES_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/BasicArray.h>
#include <datascript/Types.h>

#include "nds/common/NumLanguages.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class OfficialLanguages
{
public:
    OfficialLanguages();
    OfficialLanguages(datascript::BitStreamReader& _in);

    nds::common::NumLanguages getNumLanguages() const;
    void setNumLanguages(nds::common::NumLanguages numLanguages);

    datascript::VarUInt16Array& getOfficialLanguages();
    const datascript::VarUInt16Array& getOfficialLanguages() const;
    void setOfficialLanguages(const datascript::VarUInt16Array& officialLanguages);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const OfficialLanguages& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::NumLanguages m_numLanguages;
    datascript::VarUInt16Array m_officialLanguages;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_OFFICIAL_LANGUAGES_H
