/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_SIGNPOST_ELEMENT_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_SIGNPOST_ELEMENT_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>
#include <string>

#include "nds/common/ColorId.h"
#include "nds/common/flexattr/attrdefs/SignpostElementType.h"
#include "nds/common/flexattr/attrdefs/SignpostIconPos.h"
#include "nds/icondata/main/IconSetId.h"
#include "nds/name/common/NamedObjectId.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class SignpostElement
{
public:
    SignpostElement();
    SignpostElement(datascript::BitStreamReader& _in);

    nds::common::flexattr::attrdefs::SignpostElementType getType() const;
    void setType(nds::common::flexattr::attrdefs::SignpostElementType type);

    bool getIsAmbiguousInformation() const;
    void setIsAmbiguousInformation(bool isAmbiguousInformation);

    bool getIsSimpleInfo() const;
    void setIsSimpleInfo(bool isSimpleInfo);
    bool hasIsSimpleInfo() const;

    nds::common::flexattr::attrdefs::SignpostIconPos getIconPos() const;
    void setIconPos(nds::common::flexattr::attrdefs::SignpostIconPos iconPos);
    bool hasIconPos() const;

    std::string& getInfo();
    const std::string& getInfo() const;
    void setInfo(const std::string& info);
    bool hasInfo() const;

    nds::name::common::NamedObjectId getInfoRef() const;
    void setInfoRef(nds::name::common::NamedObjectId infoRef);
    bool hasInfoRef() const;

    nds::icondata::main::IconSetId getIcon() const;
    void setIcon(nds::icondata::main::IconSetId icon);
    bool hasIcon() const;

    bool getHasOwnColor() const;
    void setHasOwnColor(bool hasOwnColor);
    bool hasHasOwnColor() const;

    nds::common::ColorId getBackgroundColor() const;
    void setBackgroundColor(nds::common::ColorId backgroundColor);
    bool hasBackgroundColor() const;

    nds::common::ColorId getTextColor() const;
    void setTextColor(nds::common::ColorId textColor);
    bool hasTextColor() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const SignpostElement& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::flexattr::attrdefs::SignpostElementType m_type;
    bool m_isAmbiguousInformation;
    datascript::InPlaceOptionalHolder<bool> m_isSimpleInfo;
    datascript::InPlaceOptionalHolder<nds::common::flexattr::attrdefs::SignpostIconPos> m_iconPos;
    datascript::OptimizedOptionalHolder<std::string> m_info;
    datascript::InPlaceOptionalHolder<nds::name::common::NamedObjectId> m_infoRef;
    datascript::InPlaceOptionalHolder<nds::icondata::main::IconSetId> m_icon;
    datascript::InPlaceOptionalHolder<bool> m_hasOwnColor;
    datascript::InPlaceOptionalHolder<nds::common::ColorId> m_backgroundColor;
    datascript::InPlaceOptionalHolder<nds::common::ColorId> m_textColor;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_SIGNPOST_ELEMENT_H
