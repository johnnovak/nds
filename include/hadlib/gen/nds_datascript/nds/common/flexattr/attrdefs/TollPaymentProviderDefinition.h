/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_TOLL_PAYMENT_PROVIDER_DEFINITION_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_TOLL_PAYMENT_PROVIDER_DEFINITION_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <string>

#include "nds/common/LanguageCode.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class TollPaymentProviderDefinition
{
public:
    TollPaymentProviderDefinition();
    TollPaymentProviderDefinition(datascript::BitStreamReader& _in);

    std::string& getTollPaymentProviderName();
    const std::string& getTollPaymentProviderName() const;
    void setTollPaymentProviderName(const std::string& tollPaymentProviderName);

    nds::common::LanguageCode getLanguageCode() const;
    void setLanguageCode(nds::common::LanguageCode languageCode);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const TollPaymentProviderDefinition& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    std::string m_tollPaymentProviderName;
    nds::common::LanguageCode m_languageCode;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_TOLL_PAYMENT_PROVIDER_DEFINITION_H
