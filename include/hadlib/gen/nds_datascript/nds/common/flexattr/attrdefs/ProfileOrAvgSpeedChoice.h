/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_PROFILE_OR_AVG_SPEED_CHOICE_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_PROFILE_OR_AVG_SPEED_CHOICE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/AnyHolder.h>
#include <datascript/PreWriteAction.h>

#include "nds/common/fixedattributes/AverageSpeed.h"
#include "nds/common/flexattr/attrdefs/ProfileId.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class ProfileOrAvgSpeedChoice
{
public:
    ProfileOrAvgSpeedChoice();
    ProfileOrAvgSpeedChoice(datascript::BitStreamReader& _in,
        bool isProfile);

    ProfileOrAvgSpeedChoice(const ProfileOrAvgSpeedChoice& _other);
    ProfileOrAvgSpeedChoice& operator=(const ProfileOrAvgSpeedChoice& _other);

    void initialize(
        bool isProfile);

    bool getIsProfile() const;

    nds::common::flexattr::attrdefs::ProfileId getProfileId() const;
    void setProfileId(nds::common::flexattr::attrdefs::ProfileId profileId);

    nds::common::fixedattributes::AverageSpeed getAvgSpeed() const;
    void setAvgSpeed(nds::common::fixedattributes::AverageSpeed avgSpeed);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const ProfileOrAvgSpeedChoice& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_isProfile;
    bool m_isInitialized;
    datascript::AnyHolder m_objectChoice;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_PROFILE_OR_AVG_SPEED_CHOICE_H
