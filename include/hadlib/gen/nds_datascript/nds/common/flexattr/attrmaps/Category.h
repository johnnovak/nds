/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRMAPS_CATEGORY_H
#define NDS_COMMON_FLEXATTR_ATTRMAPS_CATEGORY_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrmaps
{

class Category
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_Category
    {
        SPATIAL = UINT8_C(0),
        TIME_RANGE = UINT8_C(1),
        REGION_METADATA = UINT8_C(2),
        GUIDANCE = UINT8_C(4),
        ECO = UINT8_C(5),
        ROUTING = UINT8_C(6),
        LANE = UINT8_C(7),
        ADAS = UINT8_C(8),
        PHYSICAL_CONDITION = UINT8_C(9),
        VEHICLE_TYPE = UINT8_C(10),
        SIGNPOST = UINT8_C(11),
        INFORMATIONAL = UINT8_C(12),
        TOLL = UINT8_C(13),
        BMD = UINT8_C(14),
        FIXED_ATTRIBUTES_OVERRIDE = UINT8_C(15),
        ICON = UINT8_C(16),
        DISPLAY = UINT8_C(17),
        NAME = UINT8_C(18),
        THREED = UINT8_C(20),
        REFERENCE = UINT8_C(22),
        NDS_EXTENSION = UINT8_C(23),
        TMC = UINT8_C(24)
    };

    Category();
    Category(e_Category value);
    explicit Category(datascript::BitStreamReader& _in);

    operator e_Category() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const Category& other) const;
    bool operator==(e_Category other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static Category toEnum(uint8_t rawValue);

private:
    e_Category m_value;
};

} // namespace attrmaps
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRMAPS_CATEGORY_H
