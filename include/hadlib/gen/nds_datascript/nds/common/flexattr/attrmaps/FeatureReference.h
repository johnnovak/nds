/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRMAPS_FEATURE_REFERENCE_H
#define NDS_COMMON_FLEXATTR_ATTRMAPS_FEATURE_REFERENCE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/AnyHolder.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/DirectedLineReference.h"
#include "nds/common/DirectedLinkReference.h"
#include "nds/common/DirectedRoadGeoLineReference.h"
#include "nds/common/LinkId.h"
#include "nds/common/NumBmdFeatures.h"
#include "nds/common/NumMapAreas.h"
#include "nds/common/NumMapPoints.h"
#include "nds/common/NumRoutingListElements.h"
#include "nds/common/RoadGeoLineId.h"
#include "nds/common/flexattr/attrmaps/ReferenceType.h"
#include "nds/common/flexattr/attrmaps/SpatialReference.h"
#include "nds/common/flexattr/attrmaps/TransitionReferenceSimpleIntersection.h"
#include "nds/lane/attrdefs/LaneGroupId.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrmaps
{

class FeatureReference
{
public:
    FeatureReference();
    FeatureReference(datascript::BitStreamReader& _in,
        nds::common::flexattr::attrmaps::ReferenceType referenceType);

    FeatureReference(const FeatureReference& _other);
    FeatureReference& operator=(const FeatureReference& _other);

    void initialize(
        nds::common::flexattr::attrmaps::ReferenceType referenceType);

    nds::common::flexattr::attrmaps::ReferenceType getReferenceType() const;

    nds::common::NumRoutingListElements getIntersectionRef() const;
    void setIntersectionRef(nds::common::NumRoutingListElements intersectionRef);

    nds::common::DirectedLinkReference& getDirectedLinkRef();
    const nds::common::DirectedLinkReference& getDirectedLinkRef() const;
    void setDirectedLinkRef(const nds::common::DirectedLinkReference& directedLinkRef);

    nds::common::LinkId getLinkId() const;
    void setLinkId(nds::common::LinkId linkId);

    nds::common::DirectedRoadGeoLineReference& getDirectedRoadGeoLineRef();
    const nds::common::DirectedRoadGeoLineReference& getDirectedRoadGeoLineRef() const;
    void setDirectedRoadGeoLineRef(const nds::common::DirectedRoadGeoLineReference& directedRoadGeoLineRef);

    nds::common::RoadGeoLineId getRoadGeoLineId() const;
    void setRoadGeoLineId(nds::common::RoadGeoLineId roadGeoLineId);

    nds::common::flexattr::attrmaps::TransitionReferenceSimpleIntersection& getTransitionReferenceSimpleIntersections();
    const nds::common::flexattr::attrmaps::TransitionReferenceSimpleIntersection& getTransitionReferenceSimpleIntersections() const;
    void setTransitionReferenceSimpleIntersections(const nds::common::flexattr::attrmaps::TransitionReferenceSimpleIntersection& transitionReferenceSimpleIntersections);

    nds::common::DirectedLineReference& getDirectedMapLineRef();
    const nds::common::DirectedLineReference& getDirectedMapLineRef() const;
    void setDirectedMapLineRef(const nds::common::DirectedLineReference& directedMapLineRef);

    nds::common::NumBmdFeatures getBmdLineId() const;
    void setBmdLineId(nds::common::NumBmdFeatures bmdLineId);

    nds::common::NumMapPoints getBmdPointId() const;
    void setBmdPointId(nds::common::NumMapPoints bmdPointId);

    nds::common::NumMapAreas getBmdAreaId() const;
    void setBmdAreaId(nds::common::NumMapAreas bmdAreaId);

    nds::common::flexattr::attrmaps::SpatialReference& getSpatialReference();
    const nds::common::flexattr::attrmaps::SpatialReference& getSpatialReference() const;
    void setSpatialReference(const nds::common::flexattr::attrmaps::SpatialReference& spatialReference);

    nds::lane::attrdefs::LaneGroupId getLaneGroupId() const;
    void setLaneGroupId(nds::lane::attrdefs::LaneGroupId laneGroupId);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const FeatureReference& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::flexattr::attrmaps::ReferenceType m_referenceType;
    bool m_isInitialized;
    datascript::AnyHolder m_objectChoice;
};

} // namespace attrmaps
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRMAPS_FEATURE_REFERENCE_H
