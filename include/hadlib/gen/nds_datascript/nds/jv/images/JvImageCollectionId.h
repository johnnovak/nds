/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_JV_IMAGES_JV_IMAGE_COLLECTION_ID_H
#define NDS_JV_IMAGES_JV_IMAGE_COLLECTION_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace jv
{
namespace images
{

typedef int8_t JvImageCollectionId;

} // namespace images
} // namespace jv
} // namespace nds

#endif // NDS_JV_IMAGES_JV_IMAGE_COLLECTION_ID_H
