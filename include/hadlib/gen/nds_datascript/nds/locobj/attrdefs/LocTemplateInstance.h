/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LOCOBJ_ATTRDEFS_LOC_TEMPLATE_INSTANCE_H
#define NDS_LOCOBJ_ATTRDEFS_LOC_TEMPLATE_INSTANCE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/geometry3d/TemplateVector3D.h"
#include "nds/locobj/attrdefs/LocTemplateId.h"
#include "nds/objects3d/bodygeometry/Scale.h"

namespace nds
{
namespace locobj
{
namespace attrdefs
{

class LocTemplateInstance
{
public:
    LocTemplateInstance();
    LocTemplateInstance(datascript::BitStreamReader& _in);

    void initializeChildren();

    nds::locobj::attrdefs::LocTemplateId getId() const;
    void setId(nds::locobj::attrdefs::LocTemplateId id);

    nds::common::geometry3d::TemplateVector3D& getPosition();
    const nds::common::geometry3d::TemplateVector3D& getPosition() const;
    void setPosition(const nds::common::geometry3d::TemplateVector3D& position);

    nds::common::geometry3d::TemplateVector3D& getDirection();
    const nds::common::geometry3d::TemplateVector3D& getDirection() const;
    void setDirection(const nds::common::geometry3d::TemplateVector3D& direction);

    nds::objects3d::bodygeometry::Scale getScaleX() const;
    void setScaleX(nds::objects3d::bodygeometry::Scale scaleX);

    nds::objects3d::bodygeometry::Scale getScaleY() const;
    void setScaleY(nds::objects3d::bodygeometry::Scale scaleY);

    nds::objects3d::bodygeometry::Scale getScaleZ() const;
    void setScaleZ(nds::objects3d::bodygeometry::Scale scaleZ);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const LocTemplateInstance& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::locobj::attrdefs::LocTemplateId m_id;
    datascript::InPlaceOptionalHolder<nds::common::geometry3d::TemplateVector3D> m_position;
    datascript::InPlaceOptionalHolder<nds::common::geometry3d::TemplateVector3D> m_direction;
    nds::objects3d::bodygeometry::Scale m_scaleX;
    nds::objects3d::bodygeometry::Scale m_scaleY;
    nds::objects3d::bodygeometry::Scale m_scaleZ;
};

} // namespace attrdefs
} // namespace locobj
} // namespace nds

#endif // NDS_LOCOBJ_ATTRDEFS_LOC_TEMPLATE_INSTANCE_H
