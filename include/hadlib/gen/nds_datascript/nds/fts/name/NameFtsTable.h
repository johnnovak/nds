/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_FTS_NAME_NAME_FTS_TABLE_H
#define NDS_FTS_NAME_NAME_FTS_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/fts/name/NameFtsTableRow.h"
#include "nds/common/MortonCode.h"
#include "nds/common/NamedObjectClass.h"
#include "nds/common/UpdateRegionId.h"
#include "nds/name/common/NamedObjectId.h"

namespace nds
{
namespace fts
{
namespace name
{

class NameFtsTable
{
public:
    NameFtsTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~NameFtsTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<NameFtsTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<NameFtsTableRow>& rows) const;
    void write(const std::vector<NameFtsTableRow>& rows);
    void update(const NameFtsTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<NameFtsTableRow>& rows);
    static void writeRow(const NameFtsTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace name
} // namespace fts
} // namespace nds

#endif // NDS_FTS_NAME_NAME_FTS_TABLE_H
