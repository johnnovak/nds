/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_VOLATILEDATA_MAIN_VOLATILE_POI_RELATION_TABLE_H
#define NDS_VOLATILEDATA_MAIN_VOLATILE_POI_RELATION_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/volatiledata/main/VolatilePoiRelationTableRow.h"
#include "nds/common/VersionId.h"
#include "nds/poi/main/PoiId.h"
#include "nds/volatiledata/attrdefs/VolatileLocationId.h"

namespace nds
{
namespace volatiledata
{
namespace main
{

class VolatilePoiRelationTable
{
public:
    VolatilePoiRelationTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~VolatilePoiRelationTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<VolatilePoiRelationTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<VolatilePoiRelationTableRow>& rows) const;
    void write(const std::vector<VolatilePoiRelationTableRow>& rows);
    void update(const VolatilePoiRelationTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<VolatilePoiRelationTableRow>& rows);
    static void writeRow(const VolatilePoiRelationTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace main
} // namespace volatiledata
} // namespace nds

#endif // NDS_VOLATILEDATA_MAIN_VOLATILE_POI_RELATION_TABLE_H
