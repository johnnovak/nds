/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_VOLATILEDATA_MAIN_VOLATILE_OPEN_LRACCESS_TABLE_ROW_H
#define NDS_VOLATILEDATA_MAIN_VOLATILE_OPEN_LRACCESS_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/DateTime.h"
#include "nds/common/OpenLRReference.h"
#include "nds/common/PackedTileId.h"
#include "nds/common/flexattr/attrmaps/GroupedAttributeList.h"
#include "nds/poi/accesspoints/OpenLRVersion.h"
#include "nds/volatiledata/attrdefs/VolatileConfidenceCategory.h"
#include "nds/volatiledata/attrdefs/VolatileDataProviderId.h"

namespace nds
{
namespace volatiledata
{
namespace main
{

class VolatileOpenLRAccessTableRow
{
public:
    VolatileOpenLRAccessTableRow();
    ~VolatileOpenLRAccessTableRow();

    nds::common::PackedTileId getTileId() const;
    void setTileId(nds::common::PackedTileId tileId);
    bool isNullTileId() const;
    void setNullTileId();

    const nds::common::OpenLRReference& getReference() const;
    void setReference(const nds::common::OpenLRReference& reference);
    bool isNullReference() const;
    void setNullReference();

    nds::poi::accesspoints::OpenLRVersion getVersion() const;
    void setVersion(nds::poi::accesspoints::OpenLRVersion version);
    bool isNullVersion() const;
    void setNullVersion();

    nds::volatiledata::attrdefs::VolatileDataProviderId getVolatileProviderId() const;
    void setVolatileProviderId(nds::volatiledata::attrdefs::VolatileDataProviderId volatileProviderId);
    bool isNullVolatileProviderId() const;
    void setNullVolatileProviderId();

    const nds::common::DateTime& getStartTime() const;
    void setStartTime(const nds::common::DateTime& startTime);
    bool isNullStartTime() const;
    void setNullStartTime();

    const nds::common::DateTime& getTimestamp() const;
    void setTimestamp(const nds::common::DateTime& timestamp);
    bool isNullTimestamp() const;
    void setNullTimestamp();

    const nds::common::DateTime& getExpirationTime() const;
    void setExpirationTime(const nds::common::DateTime& expirationTime);
    bool isNullExpirationTime() const;
    void setNullExpirationTime();

    nds::volatiledata::attrdefs::VolatileConfidenceCategory getConfidenceCategory() const;
    void setConfidenceCategory(nds::volatiledata::attrdefs::VolatileConfidenceCategory confidenceCategory);
    bool isNullConfidenceCategory() const;
    void setNullConfidenceCategory();

    const nds::common::flexattr::attrmaps::GroupedAttributeList& getVolatileAttrGroupValues() const;
    void setVolatileAttrGroupValues(const nds::common::flexattr::attrmaps::GroupedAttributeList& volatileAttrGroupValues);
    bool isNullVolatileAttrGroupValues() const;
    void setNullVolatileAttrGroupValues();

private:
    datascript::InPlaceOptionalHolder<nds::common::PackedTileId> m_tileId;
    datascript::InPlaceOptionalHolder<nds::common::OpenLRReference> m_reference;
    datascript::InPlaceOptionalHolder<nds::poi::accesspoints::OpenLRVersion> m_version;
    datascript::InPlaceOptionalHolder<nds::volatiledata::attrdefs::VolatileDataProviderId> m_volatileProviderId;
    datascript::InPlaceOptionalHolder<nds::common::DateTime> m_startTime;
    datascript::InPlaceOptionalHolder<nds::common::DateTime> m_timestamp;
    datascript::InPlaceOptionalHolder<nds::common::DateTime> m_expirationTime;
    datascript::InPlaceOptionalHolder<nds::volatiledata::attrdefs::VolatileConfidenceCategory> m_confidenceCategory;
    datascript::InPlaceOptionalHolder<nds::common::flexattr::attrmaps::GroupedAttributeList> m_volatileAttrGroupValues;
};

} // namespace main
} // namespace volatiledata
} // namespace nds

#endif // NDS_VOLATILEDATA_MAIN_VOLATILE_OPEN_LRACCESS_TABLE_ROW_H
