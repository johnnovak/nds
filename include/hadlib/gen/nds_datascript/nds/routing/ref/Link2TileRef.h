/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ROUTING_REF_LINK2_TILE_REF_H
#define NDS_ROUTING_REF_LINK2_TILE_REF_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/LinkId.h"
#include "nds/routing/ref/ExtTileIdxColl.h"

namespace nds
{
namespace routing
{
namespace ref
{

class Link2TileRef
{
public:
    Link2TileRef();
    Link2TileRef(datascript::BitStreamReader& _in);

    nds::common::LinkId getLinkId() const;
    void setLinkId(nds::common::LinkId linkId);

    nds::routing::ref::ExtTileIdxColl& getExtTileColl();
    const nds::routing::ref::ExtTileIdxColl& getExtTileColl() const;
    void setExtTileColl(const nds::routing::ref::ExtTileIdxColl& extTileColl);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const Link2TileRef& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::LinkId m_linkId;
    datascript::InPlaceOptionalHolder<nds::routing::ref::ExtTileIdxColl> m_extTileColl;
};

} // namespace ref
} // namespace routing
} // namespace nds

#endif // NDS_ROUTING_REF_LINK2_TILE_REF_H
