/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ROUTING_REF_ROUTE_UP_LINK_LIST_H
#define NDS_ROUTING_REF_ROUTE_UP_LINK_LIST_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/ObjectArray.h>

#include "nds/common/NumRoutingListElements.h"
#include "nds/routing/ref/RouteUpLinkRef.h"

namespace nds
{
namespace routing
{
namespace ref
{

class RouteUpLinkList
{
public:
    RouteUpLinkList();
    RouteUpLinkList(datascript::BitStreamReader& _in);

    nds::common::NumRoutingListElements getNumRefs() const;
    void setNumRefs(nds::common::NumRoutingListElements numRefs);

    datascript::ObjectArray<nds::routing::ref::RouteUpLinkRef>& getRouteUpLinkRef();
    const datascript::ObjectArray<nds::routing::ref::RouteUpLinkRef>& getRouteUpLinkRef() const;
    void setRouteUpLinkRef(const datascript::ObjectArray<nds::routing::ref::RouteUpLinkRef>& routeUpLinkRef);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const RouteUpLinkList& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    void checkConstraints();

    nds::common::NumRoutingListElements m_numRefs;
    datascript::ObjectArray<nds::routing::ref::RouteUpLinkRef> m_routeUpLinkRef;
};

} // namespace ref
} // namespace routing
} // namespace nds

#endif // NDS_ROUTING_REF_ROUTE_UP_LINK_LIST_H
