#ifndef DATASCRIPT_BASIC_ARRAY_H_INC
#define DATASCRIPT_BASIC_ARRAY_H_INC

#include <string>

#include "ArrayBase.h"
#include "Types.h"
#include "BitStreamWriter.h"
#include "BitStreamReader.h"
#include "BitSizeOfCalculator.h"
#include "StringConvertUtil.h"

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    #include "inspector/BlobInspectorTree.h"
    #include "inspector/BlobTreeArrayHandler.h"
#endif

namespace datascript
{

template <class ARRAY_TRAITS>
class BasicArray : public ArrayBase<ARRAY_TRAITS>
{
public:
    // constructors
    BasicArray() {}
    explicit BasicArray(size_t size) : ArrayBase<ARRAY_TRAITS>(size) {}

    BasicArray(datascript::BitStreamReader& in, size_t size)
    {
        read(in, size);
    }

    template <typename LABEL_OFFSET_CHECKER>
    BasicArray(datascript::BitStreamReader& in, size_t size, LABEL_OFFSET_CHECKER checker)
    {
        read(in, size, checker);
    }

    BasicArray(datascript::BitStreamReader& in, ImplicitLength size)
    {
        read(in, size);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    BasicArray(const datascript::BlobInspectorNode& arrayNode)
    {
        read(arrayNode);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    // RDS interface
    typename ArrayBase<ARRAY_TRAITS>::element_type sum() const
    {
        return ArrayBase<ARRAY_TRAITS>::sumImpl();
    }

    int hashCode() const
    {
        return ArrayBase<ARRAY_TRAITS>::hashCodeImpl();
    }

    size_t bitsizeOf(size_t bitPosition) const
    {
        if (ARRAY_TRAITS::IS_BITSIZEOF_CONSTANT)
            return ARRAY_TRAITS::BIT_SIZE * ArrayBase<ARRAY_TRAITS>::size();

        return ArrayBase<ARRAY_TRAITS>::bitsizeOfImpl(bitPosition, detail::DummyBitSizeOfAligner());
    }

    size_t bitsizeOf(size_t bitPosition, Aligned) const
    {
        return ArrayBase<ARRAY_TRAITS>::bitsizeOfImpl(bitPosition, detail::BitSizeOfAligner());
    }

    size_t setLabels(size_t bitPosition)
    {
        if (ARRAY_TRAITS::IS_BITSIZEOF_CONSTANT)
            return bitPosition + ARRAY_TRAITS::BIT_SIZE * ArrayBase<ARRAY_TRAITS>::size();

        return ArrayBase<ARRAY_TRAITS>::setLabelsImpl(bitPosition, detail::DummyLabelOffsetSetterWrapper());
    }

    template <typename LABEL_OFFSET_SETTER>
    size_t setLabels(size_t bitPosition, LABEL_OFFSET_SETTER setter)
    {
        return ArrayBase<ARRAY_TRAITS>::setLabelsImpl(bitPosition,
                detail::LabelOffsetSetterWrapper<LABEL_OFFSET_SETTER>(setter));
    }

    void read(datascript::BitStreamReader& in, size_t size)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(in, size, detail::DummyElementFactory(),
                detail::DummyLabelOffsetCheckerWrapper());
    }

    template <typename LABEL_OFFSET_CHECKER>
    void read(datascript::BitStreamReader& in, size_t size, LABEL_OFFSET_CHECKER checker)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(in, size, detail::DummyElementFactory(),
                detail::LabelOffsetCheckerWrapper<LABEL_OFFSET_CHECKER>(checker));
    }

    void read(datascript::BitStreamReader& in, ImplicitLength size)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(in, size, detail::DummyElementFactory());
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    void read(const datascript::BlobInspectorNode& arrayNode)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(arrayNode, detail::DummyElementFactory());
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    void write(datascript::BitStreamWriter& out)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out, detail::DummyLabelOffsetCheckerWrapper());
    }

    template <typename LABEL_OFFSET_CHECKER>
    void write(datascript::BitStreamWriter& out, LABEL_OFFSET_CHECKER checker)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out,
                detail::LabelOffsetCheckerWrapper<LABEL_OFFSET_CHECKER>(checker));
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    void write(datascript::BitStreamWriter& out, datascript::BlobInspectorNode& arrayNode,
            const datascript::StringHolder& elementDsTypeName)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out,
            detail::BlobTreeNumericArrayHandler(arrayNode, ArrayBase<ARRAY_TRAITS>::size(), elementDsTypeName),
            detail::DummyLabelOffsetCheckerWrapper());
    }

    template <typename LABEL_OFFSET_CHECKER>
    void write(datascript::BitStreamWriter& out, datascript::BlobInspectorNode& arrayNode,
            const datascript::StringHolder& elementDsTypeName, LABEL_OFFSET_CHECKER checker)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out,
            detail::BlobTreeNumericArrayHandler(arrayNode, ArrayBase<ARRAY_TRAITS>::size(), elementDsTypeName),
            detail::LabelOffsetCheckerWrapper<LABEL_OFFSET_CHECKER>(checker));
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
};

namespace detail
{

template <typename T>
struct var_int_array_traits;

template <>
struct var_int_array_traits<int16_t>
{
    typedef int16_t type;

    static size_t bitsizeOf(size_t, type value, uint8_t) { return getBitSizeOfVarInt16(value); }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readVarInt16();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeVarInt16(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

template <>
struct var_int_array_traits<int32_t>
{
    typedef int32_t type;

    static size_t bitsizeOf(size_t, type value, uint8_t) { return getBitSizeOfVarInt32(value); }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readVarInt32();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeVarInt32(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

template <>
struct var_int_array_traits<int64_t>
{
    typedef int64_t type;

    static size_t bitsizeOf(size_t, type value, uint8_t) { return getBitSizeOfVarInt64(value); }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readVarInt64();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeVarInt64(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

template <>
struct var_int_array_traits<uint16_t>
{
    typedef uint16_t type;

    static size_t bitsizeOf(size_t, type value, uint8_t) { return getBitSizeOfVarUInt16(value); }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readVarUInt16();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeVarUInt16(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

template <>
struct var_int_array_traits<uint32_t>
{
    typedef uint32_t type;

    static size_t bitsizeOf(size_t, type value, uint8_t) { return getBitSizeOfVarUInt32(value); }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readVarUInt32();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeVarUInt32(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

template <>
struct var_int_array_traits<uint64_t>
{
    typedef uint64_t type;

    static size_t bitsizeOf(size_t, type value, uint8_t) { return getBitSizeOfVarUInt64(value); }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readVarUInt64();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeVarUInt64(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

struct float_array_traits
{
    typedef float type;

    static size_t bitsizeOf(size_t, type, uint8_t) { return BIT_SIZE; }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = in.readFloat16();
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeFloat16(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = true;
    static const size_t BIT_SIZE = 16;
};

struct bool_array_traits
{
    typedef uint8_t type;

    static size_t bitsizeOf(size_t, type, uint8_t) { return BIT_SIZE; }

    static size_t setLabels(size_t bitPosition, type value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(type* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        *storage = static_cast<type>(in.readBool());
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        if (value > 1)
            throw CppRuntimeException("bool value '" + convertToString(value) + "' is out of range");
        out.writeBool((value != 0) ? true : false);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = true;
    static const size_t BIT_SIZE = 1;
};

struct string_array_traits
{
    typedef std::string type;

    static size_t bitsizeOf(size_t, const type& value, uint8_t) { return getBitSizeOfString(value); }

    static size_t setLabels(size_t bitPosition, const type& value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(void* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        new (storage) type(in.readString());
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeString(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

struct utf8_string_array_traits
{
    typedef std::string type;

    static size_t bitsizeOf(size_t, const type& value, uint8_t) { return getBitSizeOfUtf8string(value); }

    static size_t setLabels(size_t bitPosition, const type& value, uint8_t numBits)
    {
        return bitPosition + bitsizeOf(bitPosition, value, numBits);
    }

    template <typename ELEMENT_FACTORY>
    static void read(void* storage, BitStreamReader& in, size_t, ELEMENT_FACTORY, uint8_t)
    {
        new (storage) type(in.readUtf8string());
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t, ELEMENT_FACTORY, uint8_t)
    {
        node.getValue().get(*storage);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type value, uint8_t)
    {
        out.writeUtf8string(value);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type value, uint8_t numBits)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        write(out, value, numBits);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static const bool IS_BITSIZEOF_CONSTANT = false;
    static const size_t BIT_SIZE = 0;
};

} // namespace detail

typedef BasicArray<detail::var_int_array_traits<uint16_t> > VarUInt16Array;
typedef BasicArray<detail::var_int_array_traits<uint32_t> > VarUInt32Array;
typedef BasicArray<detail::var_int_array_traits<uint64_t> > VarUInt64Array;

typedef BasicArray<detail::var_int_array_traits<int16_t> >  VarInt16Array;
typedef BasicArray<detail::var_int_array_traits<int32_t> >  VarInt32Array;
typedef BasicArray<detail::var_int_array_traits<int64_t> >  VarInt64Array;

typedef BasicArray<detail::float_array_traits>              FloatArray;
typedef BasicArray<detail::bool_array_traits>               BoolArray;

typedef BasicArray<detail::string_array_traits>             StringArray;
typedef BasicArray<detail::utf8_string_array_traits>        Utf8stringArray;

} // namespace datascript

#endif // DATASCRIPT_BASIC_ARRAY_H_INC
