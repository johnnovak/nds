#ifndef DATASCRIPT_BIT_STREAM_EXCEPTION_H_INC
#define DATASCRIPT_BIT_STREAM_EXCEPTION_H_INC

#include <string>

#include "CppRuntimeException.h"

namespace datascript
{

class BitStreamException : public CppRuntimeException
{
public:
    explicit BitStreamException(const std::string& message) : CppRuntimeException(message) {}
};

} // namespace datascript

#endif // ifndef DATASCRIPT_BIT_STREAM_EXCEPTION_H_INC
