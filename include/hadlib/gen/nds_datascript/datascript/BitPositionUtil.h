#ifndef DATASCRIPT_BITPOSITION_UTIL_H_INC
#define DATASCRIPT_BITPOSITION_UTIL_H_INC

#include <cstddef>

namespace datascript
{
static const size_t NUM_BITS_PER_BYTE = 8;

size_t alignTo(size_t alignmentValue, size_t bitSize);
size_t bitsToBytes(size_t numBits);
size_t bytesToBits(size_t numBytes);

} // namespace datascript

#endif // ifndef DATASCRIPT_BITPOSITION_UTIL_H_INC
