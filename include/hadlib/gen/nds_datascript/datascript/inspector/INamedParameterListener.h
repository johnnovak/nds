#ifndef DATASCRIPT_INAMED_PARAMETER_LISTENER_H_INC
#define DATASCRIPT_INAMED_PARAMETER_LISTENER_H_INC

#include <string>

#include "../Types.h"
#include "../BitStreamReader.h"

namespace datascript
{

/**
 * A listener interface for retrieving parameter values by their name.
 */
class INamedParameterListener
{
public:
    /**
     * Enum specifying the size of a varint or varuint DataScript type.
     */
    enum EVarIntSize
    {
        VarInt16,
        VarInt32,
        VarInt64
    };

    /**
     * Requests a value for an unsigned integral parameter.
     *
     * This is called both for uintN types and unsigned bitfield types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] nBits             Number of bits. A value in the range 1..64.
     * \return The value to be used for the parameter. Must not use more than nBits bits.
     */
    virtual uint64_t onUnsignedInt(const std::string& parentName, const std::string& parameterName,
            unsigned nBits) = 0;

    /**
     * Requests a value for a signed integral parameter.
     *
     * This is called both for intN types and signed bitfield types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] nBits             Number of bits. A value in the range 1..64.
     * \return The value to be used for the parameter. Must not use more than nBits bits.
     */
    virtual int64_t onInt(const std::string& parentName, const std::string& parameterName,
            unsigned nBits) = 0;

    /**
     * Requests a value for an unsigned variable integral parameter.
     *
     * This is called for varuintN types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] size              Enum specifying the size of the underlying variable unsigned integral
     *                              type.
     * \return The value to be used for the parameter. Must fit in the appropriate varuintN DataScript type.
     */
    virtual uint64_t onVarUnsignedInt(const std::string& parentName,
            const std::string& parameterName, EVarIntSize size) = 0;

    /**
     * Requests a value for a signed variable integral parameter.
     *
     * This is called for varintN types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] size              Enum specifying the size of the underlying variable signed integral
     *                              type.
     * \return The value to be used for the parameter. Must fit in the appropriate varintN DataScript type.
     */
    virtual int64_t onVarInt(const std::string& parentName, const std::string& parameterName,
            EVarIntSize size) = 0;

    /**
     * Requests a value for an enum based on an unsigned integral parameter.
     *
     * This is called both for enums based on both uintN types and unsigned bitfield types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] nBits             Number of bits. A value in the range 1..64.
     * \return The value to be used for the parameter. Must not use more than nBits bits.
     */
    virtual uint64_t onUnsignedIntEnum(const std::string& parentName, const std::string& parameterName,
            unsigned nBits) = 0;

    /**
     * Requests a value for an enum based on a signed integral parameter.
     *
     * This is called both for enums based on both intN types and signed bitfield types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] nBits             Number of bits. A value in the range 1..64.
     * \return The value to be used for the parameter. Must not use more than nBits bits.
     */
    virtual int64_t onIntEnum(const std::string& parentName, const std::string& parameterName,
            unsigned nBits) = 0;

    /**
     * Requests a value for an enum based on an unsigned variable integral parameter.
     *
     * This is called for enums based on varuintN types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] size              Enum specifying the size of the underlying variable unsigned integral
     *                              type.
     * \return The value to be used for the parameter. Must fit in the appropriate varuintN DataScript type.
     */
    virtual uint64_t onVarUnsignedIntEnum(const std::string& parentName, const std::string& parameterName,
            EVarIntSize size) = 0;

    /**
     * Requests a value for an enum based on a signed variable integral parameter.
     *
     * This is called for enums based on varintN types.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \param[in] size              Enum specifying the size of the underlying variable signed integral
     *                              type.
     * \return The value to be used for the parameter. Must fit in the appropriate varintN DataScript type.
     */
    virtual int64_t onVarIntEnum(const std::string& parentName, const std::string& parameterName,
            EVarIntSize size) = 0;

    /**
     * Requests a value for a float parameter.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \return The value to be used for the parameter.
     */
    virtual float onFloat(const std::string& parentName, const std::string& parameterName) = 0;

    /**
     * Requests a value for a bool parameter.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \return The value to be used for the parameter.
     */
    virtual bool onBool(const std::string& parentName, const std::string& parameterName) = 0;

    /**
     * Requests a value for a string parameter.
     *
     * The string must be UTF-8 encoded and it must not contain a 0-byte.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \return The value to be used for the parameter.
     */
    virtual std::string onString(const std::string& parentName, const std::string& parameterName) = 0;

    /**
     * Requests a value for a string parameter.
     *
     * The string must be UTF-8 encoded. Its byte length must fit in a varuint16 type.
     * 0-bytes in content are allowed.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \return The value to be used for the parameter.
     */
    virtual std::string onUtf8String(const std::string& parentName, const std::string& parameterName) = 0;

    /**
     * Requests a bitstream for a parameter of a compound type (sequence, union or choice).
     *
     * The bitstream is used to create an instance of the compound type. The actual type of the
     * parameter (the compound type) must not require any parameters and it must not depend on
     * any parent types.
     *
     * The data pointed to by the bitstream reader must remain valid until the encompassing
     * walkBlob() call (that triggered this callback) returns.
     *
     * \param[in] parentName        Full DataScript name of the type that requires this parameter.
     * \param[in] parameterName     Name of the parameter.
     * \return Bitstream used to create an instance of the compound type.
     */
    virtual datascript::BitStreamReader onCompoundType(const std::string& parentName,
            const std::string& parameterName) = 0;

    virtual ~INamedParameterListener()
    {}
};

} // namespace datascript

#endif // ifndef DATASCRIPT_INAMED_PARAMETER_LISTENER_H_INC
