#ifndef DATASCRIPT_BLOB_TREE_ARRAY_HANDLER_H_INC
#define DATASCRIPT_BLOB_TREE_ARRAY_HANDLER_H_INC

#include "../BitStreamWriter.h"
#include "../StringConvertUtil.h"
#include "../PreWriteAction.h"

#include "BlobInspectorTree.h"

namespace datascript
{
namespace detail
{

class BlobTreeArrayHandlerBase
{
public:
    BlobTreeArrayHandlerBase(BlobInspectorNode& arrayNode, size_t arraySize,
            const StringHolder& elementDsTypeName, BlobInspectorNode::NodeType elementNodeType) :
                m_arrayNode(arrayNode),
                m_elementDsTypeName(elementDsTypeName),
                m_elementNodeType(elementNodeType),
                m_startBitPosition(BlobInspectorNode::RdsDescriptor::UNDEFINED_BIT_POSITION)
    {
        arrayNode.reserveChildren(arraySize);
    }

    BlobInspectorNode& createElementNode(const BitStreamWriter& out)
    {
        m_startBitPosition = out.getBitPosition();

        return m_arrayNode.createChild(m_elementNodeType, m_elementDsTypeName, m_arrayNode.getDsName());
    }

protected:
    void fillRdsDescriptor(BlobInspectorNode& elementNode, const BitStreamWriter& out)
    {
        elementNode.setRdsDescriptor(BlobInspectorNode::RdsDescriptor(m_startBitPosition,
                out.getBitPosition()));
    }

private:
    BlobInspectorNode&          m_arrayNode;
    StringHolder                m_elementDsTypeName;
    BlobInspectorNode::NodeType m_elementNodeType;
    size_t                      m_startBitPosition;
};

class BlobTreeNumericArrayHandler : public BlobTreeArrayHandlerBase
{
public:
    BlobTreeNumericArrayHandler(BlobInspectorNode& arrayNode,
            size_t arraySize,
            const StringHolder& elementDsTypeName) : BlobTreeArrayHandlerBase(arrayNode, arraySize,
                    elementDsTypeName, BlobInspectorNode::NT_VALUE)
    {
    }

    template<typename VALUE_TYPE>
    void fillElementNode(BlobInspectorNode& elementNode, const BitStreamWriter& out, const VALUE_TYPE& value)
    {
        elementNode.setValue(BlobInspectorValue(value));
        fillRdsDescriptor(elementNode, out);
    }
};

class BlobTreeObjectArrayHandler : public BlobTreeArrayHandlerBase
{
public:
    BlobTreeObjectArrayHandler(BlobInspectorNode& arrayNode,
            size_t arraySize,
            const StringHolder& elementDsTypeName) : BlobTreeArrayHandlerBase(arrayNode, arraySize,
                    elementDsTypeName, BlobInspectorNode::NT_CONTAINER)
    {
    }

    template<typename VALUE_TYPE>
    void fillElementNode(BlobInspectorNode& elementNode, const BitStreamWriter& out, const VALUE_TYPE&)
    {
        fillRdsDescriptor(elementNode, out);
    }
};

} // namespace detail
} // namespace datascript

#endif // ifndef DATASCRIPT_BLOB_TREE_ARRAY_HANDLER_H_INC
