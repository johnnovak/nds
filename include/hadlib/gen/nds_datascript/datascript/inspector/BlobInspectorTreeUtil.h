#ifndef DATASCRIPT_BLOB_INSPECTOR_NODE_UTIL_H_INC
#define DATASCRIPT_BLOB_INSPECTOR_NODE_UTIL_H_INC

#include <string>

#include "BlobInspectorTree.h"

namespace datascript
{

const BlobInspectorNode& getBlobInspectorNode(const BlobInspectorTree& tree, size_t childIndex,
        BlobInspectorNode::NodeType expectedNodeType);

void findBlobInspectorNodes(const BlobInspectorTree& tree, bool recursiveSearch,
        const std::string& expectedDsTypeName, const std::string& expectedDsName,
        Container<BlobInspectorNode>& searchedNodes);

} // namespace datascript

#endif // ifndef DATASCRIPT_BLOB_INSPECTOR_NODE_UTIL_H_INC
