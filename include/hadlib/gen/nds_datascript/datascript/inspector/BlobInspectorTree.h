#ifndef DATASCRIPT_BLOB_INSPECTOR_TREE_H_INC
#define DATASCRIPT_BLOB_INSPECTOR_TREE_H_INC

#include <string>

#include "../Container.h"
#include "../StringHolder.h"

#include "BlobInspectorValue.h"

namespace datascript
{

class BlobInspectorNode
{
public:
    enum NodeType
    {
        NT_CONTAINER,   // m_value = VT_UNDEFINED, m_children.size() > 0
        NT_VALUE,       // m_value = value, m_children.size() = 0
        NT_ARRAY        // m_value = VT_UNDEFINED, m_children.size() = array size
    };

    struct RdsDescriptor
    {
        RdsDescriptor(size_t startPos = UNDEFINED_BIT_POSITION, size_t endPos = UNDEFINED_BIT_POSITION) :
            startBitPosition(startPos), endBitPosition(endPos) {}

        bool operator==(const RdsDescriptor& other) const
        {
            return (startBitPosition == other.startBitPosition && endBitPosition == other.endBitPosition);
        }

        size_t              startBitPosition;
        size_t              endBitPosition;

        static const size_t UNDEFINED_BIT_POSITION = static_cast<size_t>(-1);
    };

    struct RdsFunction
    {
        RdsFunction(const StringHolder& returnTypeName, const StringHolder& functionName) :
                        dsReturnTypeName(returnTypeName), dsFunctionName(functionName) {}
        RdsFunction(const StringHolder& returnTypeName, const StringHolder& functionName,
                const BlobInspectorValue& value) :
                        dsReturnTypeName(returnTypeName), dsFunctionName(functionName), returnValue(value) {}

        bool operator==(const RdsFunction& other) const
        {
            return (dsReturnTypeName == other.dsReturnTypeName && dsFunctionName == other.dsFunctionName &&
                    returnValue == other.returnValue);
        }

        StringHolder        dsReturnTypeName;
        StringHolder        dsFunctionName;
        BlobInspectorValue  returnValue;
    };

    explicit BlobInspectorNode(NodeType nodeType = NT_CONTAINER);
    BlobInspectorNode(NodeType nodeType, const StringHolder& dsTypeName, const StringHolder& dsName);

    // default destructor, copy constructor and copy assignment operator is fine

    void setDsTypeName(const StringHolder& dsTypeName);
    void setDsName(const StringHolder& dsName);

    void setValue(const BlobInspectorValue& value);
    BlobInspectorValue& getValue();

    void setRdsDescriptor(const RdsDescriptor& rdsDescriptor);
    void setRdsDescriptor(size_t startBitPosition, size_t endBitPosition);

    void reserveChildren(size_t numChildren);
    BlobInspectorNode& createChild(NodeType nodeType, const StringHolder& dsTypeName,
            const StringHolder& dsName);
    Container<BlobInspectorNode>& getChildren();

    void reserveRdsFunctions(size_t numRdsFunctions);
    RdsFunction& createRdsFunction(const StringHolder& returnTypeName, const StringHolder& functionName);
    Container<RdsFunction>& getRdsFunctions();

    NodeType getNodeType() const;
    const StringHolder& getDsTypeName() const;
    const StringHolder& getDsName() const;
    const BlobInspectorValue& getValue() const;
    const RdsDescriptor& getRdsDescriptor() const;
    const Container<BlobInspectorNode>& getChildren() const;
    const Container<RdsFunction>& getRdsFunctions() const;

    static const char* convertNodeTypeToString(NodeType nodeType);

private:
    NodeType                        m_nodeType;
    StringHolder                    m_dsTypeName;
    StringHolder                    m_dsName;
    BlobInspectorValue              m_value;
    Container<BlobInspectorNode>    m_children;
    RdsDescriptor                   m_rdsDescriptor;
    Container<RdsFunction>          m_rdsFunctions;
};

typedef BlobInspectorNode BlobInspectorTree;

} // namespace datascript

#endif // ifndef DATASCRIPT_BLOB_INSPECTOR_TREE_H_INC
