#ifndef DATASCRIPT_OBJECT_ARRAY_H_INC
#define DATASCRIPT_OBJECT_ARRAY_H_INC

#include "ArrayBase.h"
#include "BitStreamWriter.h"
#include "BitStreamReader.h"
#include "PreWriteAction.h"

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    #include "inspector/BlobTreeArrayHandler.h"
    #include "inspector/BlobInspectorTree.h"
#endif

namespace datascript
{

namespace detail
{

template <class OBJECT>
struct object_array_traits
{
    typedef OBJECT type;

    static size_t bitsizeOf(size_t bitPosition, const type& value, uint8_t)
    {
        return value.bitsizeOf(bitPosition);
    }

    static size_t setLabels(size_t bitPosition, type& value, uint8_t) { return value.setLabels(bitPosition); }

    template <typename ELEMENT_FACTORY>
    static void read(void* storage, BitStreamReader& in, size_t index, ELEMENT_FACTORY elementFactory, uint8_t)
    {
        elementFactory.create(storage, in, index);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    static void read(type* storage, const datascript::BlobInspectorNode& node, size_t index,
            ELEMENT_FACTORY elementFactory, uint8_t)
    {
        elementFactory.create(storage, node, index);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    static void write(BitStreamWriter& out, type& value, uint8_t)
    {
        value.write(out, NO_PRE_WRITE_ACTION);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER>
    static void write(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler, type& value, uint8_t)
    {
        BlobInspectorNode& elementNode = blobTreeHandler.createElementNode(out);
        value.write(out, elementNode, NO_PRE_WRITE_ACTION);
        blobTreeHandler.fillElementNode(elementNode, out, value);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
};

} // namespace detail

template <class OBJECT>
class ObjectArray : public ArrayBase<detail::object_array_traits<OBJECT> >
{
public:
    // constructors
    ObjectArray() {}
    explicit ObjectArray(size_t size) : ArrayBase<ARRAY_TRAITS>(size) {}

    template <typename ELEMENT_FACTORY>
    ObjectArray(datascript::BitStreamReader& in, size_t size, ELEMENT_FACTORY elementFactory)
    {
        read(in, size, elementFactory);
    }

    template <typename ELEMENT_FACTORY, typename LABEL_OFFSET_CHECKER>
    ObjectArray(datascript::BitStreamReader& in, size_t size, ELEMENT_FACTORY elementFactory,
            LABEL_OFFSET_CHECKER checker)
    {
        read(in, size, elementFactory, checker);
    }

    template <typename ELEMENT_FACTORY>
    ObjectArray(datascript::BitStreamReader& in, ImplicitLength size, ELEMENT_FACTORY elementFactory)
    {
        read(in, size, elementFactory);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    ObjectArray(const datascript::BlobInspectorNode& arrayNode, ELEMENT_FACTORY elementFactory)
    {
        read(arrayNode, elementFactory);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    // RDS interface
    template <typename ELEMENT_INITIALIZER>
    void initializeElements(ELEMENT_INITIALIZER elementInitializer)
    {
        ArrayBase<ARRAY_TRAITS>::initializeElementsImpl(elementInitializer);
    }

    int hashCode() const
    {
        return ArrayBase<ARRAY_TRAITS>::hashCodeImpl();
    }

    size_t bitsizeOf(size_t bitPosition) const
    {
        return ArrayBase<ARRAY_TRAITS>::bitsizeOfImpl(bitPosition, detail::DummyBitSizeOfAligner());
    }

    size_t bitsizeOf(size_t bitPosition, Aligned) const
    {
        return ArrayBase<ARRAY_TRAITS>::bitsizeOfImpl(bitPosition, detail::BitSizeOfAligner());
    }

    size_t setLabels(size_t bitPosition)
    {
        return ArrayBase<ARRAY_TRAITS>::setLabelsImpl(bitPosition, detail::DummyLabelOffsetSetterWrapper());
    }

    template <typename LABEL_OFFSET_SETTER>
    size_t setLabels(size_t bitPosition, LABEL_OFFSET_SETTER setter)
    {
        return ArrayBase<ARRAY_TRAITS>::setLabelsImpl(bitPosition,
                detail::LabelOffsetSetterWrapper<LABEL_OFFSET_SETTER>(setter));
    }

    template <typename ELEMENT_FACTORY>
    void read(datascript::BitStreamReader& in, size_t size, ELEMENT_FACTORY elementFactory)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(in, size, elementFactory,
                detail::DummyLabelOffsetCheckerWrapper());
    }

    template <typename ELEMENT_FACTORY, typename LABEL_OFFSET_CHECKER>
    void read(datascript::BitStreamReader& in, size_t size, ELEMENT_FACTORY elementFactory,
            LABEL_OFFSET_CHECKER checker)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(in, size, elementFactory,
                detail::LabelOffsetCheckerWrapper<LABEL_OFFSET_CHECKER>(checker));
    }

    template <typename ELEMENT_FACTORY>
    void read(datascript::BitStreamReader& in, ImplicitLength size, ELEMENT_FACTORY elementFactory)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(in, size, elementFactory);
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    void read(const datascript::BlobInspectorNode& arrayNode, ELEMENT_FACTORY elementFactory)
    {
        ArrayBase<ARRAY_TRAITS>::readImpl(arrayNode, elementFactory);
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    void write(datascript::BitStreamWriter& out)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out, detail::DummyLabelOffsetCheckerWrapper());
    }

    template <typename LABEL_OFFSET_CHECKER>
    void write(datascript::BitStreamWriter& out, LABEL_OFFSET_CHECKER checker)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out,
                detail::LabelOffsetCheckerWrapper<LABEL_OFFSET_CHECKER>(checker));
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    void write(datascript::BitStreamWriter& out, datascript::BlobInspectorNode& arrayNode,
            const datascript::StringHolder& elementDsTypeName)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out,
            detail::BlobTreeObjectArrayHandler(arrayNode, ArrayBase<ARRAY_TRAITS>::size(), elementDsTypeName),
            detail::DummyLabelOffsetCheckerWrapper());
    }

    template <typename LABEL_OFFSET_CHECKER>
    void write(datascript::BitStreamWriter& out, datascript::BlobInspectorNode& arrayNode,
            const datascript::StringHolder& elementDsTypeName, LABEL_OFFSET_CHECKER checker)
    {
        ArrayBase<ARRAY_TRAITS>::writeImpl(out,
            detail::BlobTreeObjectArrayHandler(arrayNode, ArrayBase<ARRAY_TRAITS>::size(), elementDsTypeName),
            detail::LabelOffsetCheckerWrapper<LABEL_OFFSET_CHECKER>(checker));
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

private:
    typedef detail::object_array_traits<OBJECT> ARRAY_TRAITS;
};

} // namespace datascript

#endif // DATASCRIPT_OBJECT_ARRAY_H_INC
