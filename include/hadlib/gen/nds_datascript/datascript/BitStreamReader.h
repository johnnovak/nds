#ifndef DATASCRIPT_BIT_STREAM_READER_H_INC
#define DATASCRIPT_BIT_STREAM_READER_H_INC

#include <cstddef>
#include <string>

#include "Types.h"

namespace datascript
{

class BitStreamReader
{
public:
    typedef size_t BitPosType;

    BitStreamReader(const uint8_t* data, size_t bufferByteSize);
    ~BitStreamReader();

    uint32_t readBits(uint8_t numBits = 32);
    uint64_t readBits64(uint8_t numBits = 64);

    int64_t readSignedBits64(uint8_t numBits = 64);
    int32_t readSignedBits(uint8_t numBits = 32);

    int64_t readVarInt64();
    int32_t readVarInt32();
    int16_t readVarInt16();

    uint64_t readVarUInt64();
    uint32_t readVarUInt32();
    uint16_t readVarUInt16();

    float readFloat16();
    std::string readString();
    std::string readUtf8string();
    bool readBool();

    BitPosType getBitPosition() const { return m_bitIndex; }
    void setBitPosition(BitPosType pos);

    void alignTo(size_t alignment);

private:
    uint32_t getCacheNext(BitPosType bitIndex, uint32_t& cacheBitsFree);
    int64_t readVarNum(bool isSigned, size_t  maxVarBytes);

    const uint8_t*  m_buffer;
    const size_t    m_bufferByteSize;

    uint32_t        m_cacheBuffer;
    uint32_t        m_cacheNumBits;

    BitPosType      m_bitIndex;
};

} // namespace datascript

#endif // ifndef DATASCRIPT_BIT_STREAM_READER_H_INC
