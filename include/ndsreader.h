#ifndef _ndsreader_h_
#define _ndsreader_h_

#include <hadlib/hadlib.h>
#include <parser/src/CParser.hpp>
#include <database/src/CRoot.hpp>
#include <database/src/CProduct.hpp>
#include <database/src/CLaneBuildingBlock.hpp>
#include <datascript/BitStreamReader.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>

typedef boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian> point_t;
typedef boost::geometry::model::linestring<point_t> linestring_t;
typedef std::vector< linestring_t * > vecLineStrings;

uint64_t MortonCode_shuffle_new( const uint32_t x, const uint32_t y );
void MortonCode_unshuffle( const uint64_t m, uint32_t &x, uint32_t &y );

int64_t MortonCode_shuffle(int32_t x, int32_t y);

void getTileAnchor( int64_t tileNum, int32_t levelNum, int32_t &x, int32_t &y );
void getTileCenter( int64_t tileNum, int32_t levelNum, int32_t &dx, int32_t &dy );

// Tile number and level -> tileId for dedicated level (key value in all tables to get the data)

int64_t getPackedTileId( int64_t tileNum, int32_t levelNum );

int32_t getBitCountForTileNum(int32_t level);

// Calculating tile ID for this position at each level(returns IDs for each level).
int64_t getTile( int32_t x, int32_t y, int32_t level);

/*
	Transform "WGS84" Coordinates into NDS Coordinates
		0. "WGS84" Coordinates
		1. Basic Input Position in decimal degree -> convert into Integer WGS84_7 (7 digit) representation
			- multiply decimal degree value 10000000 for 7 digit shift
				- 7 digit shift is maximum for integer representation based on 180° maximum of range
		2. Convert to NDS Integer value

	NOTE:	(c/c++ based code sample)
*/

//create integer representation of position coordinates
void coord_dec2int(double x_in, double y_in, int32_t &xWGS84, int32_t &yWGS84);

//create integer representation of position coordinates
void coord_int2dec(int32_t xWGS84, int32_t yWGS84, double &x, double &y);

// convert into NDS integer value representation
void wgs84_7ToNDS(int32_t xWGS84, int32_t yWGS84, int32_t &xNDS_Result, int32_t &yNDS_Result);

// convert into NDS integer value representation
void NDS_Towgs84_7(int32_t xNDS, int32_t yNDS, int32_t &xWGS84, int32_t &yWGS84 );

void wgs84To3857( double lon, double lat, double &x, double &y);

NDatabase::CLaneBuildingBlock *setupDB( std::string &DBPath, long tileID );

#endif // _ndsreader_h_
