#ifndef DATASCRIPT_BITFIELD_UTIL_H_INC
#define DATASCRIPT_BITFIELD_UTIL_H_INC

#include <cstddef>

#include "Types.h"

namespace datascript
{

int64_t getBitFieldLowerBound(size_t length, bool isSigned);
uint64_t getBitFieldUpperBound(size_t length, bool isSigned);

} // namespace datascript

#endif // ifndef DATASCRIPT_BITFIELD_UTIL_H_INC
