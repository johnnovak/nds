#ifndef DATASCRIPT_CPP_RUNTIME_EXCEPTION_H_INC
#define DATASCRIPT_CPP_RUNTIME_EXCEPTION_H_INC

#include <string>
#include <stdexcept>

namespace datascript
{

class CppRuntimeException : public std::runtime_error
{
public:
    explicit CppRuntimeException(const std::string& message) : std::runtime_error(message) {}
};

} // namespace datascript

#endif // ifndef DATASCRIPT_CPP_RUNTIME_EXCEPTION_H_INC
