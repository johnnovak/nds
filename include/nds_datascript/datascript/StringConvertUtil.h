#ifndef DATASCRIPT_STRING_CONVERT_UTIL_H_INC
#define DATASCRIPT_STRING_CONVERT_UTIL_H_INC

#include <string>
#include <sstream>

namespace datascript
{

template<typename T>
std::string convertToString(T value)
{
    std::stringstream stream;
    stream << value;

    return stream.str();
}

template<>
std::string convertToString<bool>(bool value);

template<>
std::string convertToString<char>(char value);

template<>
std::string convertToString<signed char>(signed char value);

template<>
std::string convertToString<unsigned char>(unsigned char value);

template<>
std::string convertToString<unsigned int>(unsigned int value);

template<>
std::string convertToString<int>(int value);

} // namespace datascript

#endif // ifndef DATASCRIPT_STRING_CONVERT_UTIL_H_INC
