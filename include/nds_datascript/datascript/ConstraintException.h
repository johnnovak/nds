#ifndef DATASCRIPT_CONSTRAINT_EXCEPTION_H_INC
#define DATASCRIPT_CONSTRAINT_EXCEPTION_H_INC

#include <string>

#include "CppRuntimeException.h"

namespace datascript
{

class ConstraintException : public CppRuntimeException
{
public:
    ConstraintException(const std::string& message) : CppRuntimeException(message) {}
};

} // namespace datascript

#endif // ifndef DATASCRIPT_CONSTRAINT_EXCEPTION_H_INC
