#ifndef DATASCRIPT_SQL_DATABASE_H_INC
#define DATASCRIPT_SQL_DATABASE_H_INC

#include <string>
#include <nds_sqlite3.h>

#include "DbAccessMode.h"
#include "BitStreamReader.h"

namespace datascript
{

class SqlDatabase
{
public:
    SqlDatabase();
    virtual ~SqlDatabase();

    void open(sqlite3* externalConnection);
    void open(const std::string& fileName, DbAccessMode::ModeEnum mode);
    bool isOpen() const;
    void close();

    sqlite3* getConnection();

    void executeUpdate(const std::string& sqlQuery);
    sqlite3_stmt* prepareStatement(const std::string& sqlQuery);

    virtual void createSchema() = 0;
    virtual void deleteSchema() = 0;
    virtual void createIndices() = 0;
    virtual void deleteIndices() = 0;

protected:
    virtual void doOpen(sqlite3*) {}

    bool startTransaction();
    void endTransaction(bool wasTransactionStarted);

private:
    // disable copy constructor and assignment operator
    SqlDatabase(const SqlDatabase&);
    SqlDatabase& operator=(const SqlDatabase&);

    static int dbAccesModeToSQLite(DbAccessMode::ModeEnum mode);

    class SqliteConnection
    {
    public:
        enum ConnectionType
        {
            INTERNAL_CONNECTION,
            EXTERNAL_CONNECTION
        };

        SqliteConnection(sqlite3* connection = NULL, ConnectionType connectionType = INTERNAL_CONNECTION);
        ~SqliteConnection();
        bool isOpen() const;
        void reset(sqlite3* connection = NULL, ConnectionType connectionType = INTERNAL_CONNECTION);

        sqlite3* get();

    private:
        sqlite3* m_connection;
        ConnectionType m_connectionType;
    };

    SqliteConnection m_db;
};

} // namespace datascript

#endif // ifndef DATASCRIPT_SQL_DATABASE_H_INC
