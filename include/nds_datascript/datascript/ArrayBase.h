#ifndef DATASCRIPT_ARRAY_BASE_H_INC
#define DATASCRIPT_ARRAY_BASE_H_INC

#include <cstddef>
#include <numeric>

#include "Container.h"
#include "HashCodeUtil.h"
#include "BitStreamWriter.h"
#include "BitStreamReader.h"
#include "BitPositionUtil.h"
#include "BitStreamException.h"
#include "ConstraintException.h"

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    #include "inspector/BlobInspectorTree.h"
#endif

namespace datascript
{

struct ImplicitLength
{
};

struct Aligned
{
};

template <class ARRAY_TRAITS>
class ArrayBase : public Container<typename ARRAY_TRAITS::type>
{
public:
    // typedefs
    typedef typename ARRAY_TRAITS::type element_type;
    typedef Container<element_type>     container_type;

    // constructors
    ArrayBase()
    {
    }

    explicit ArrayBase(size_t size) : container_type(size)
    {
    }

protected:
    template <typename ELEMENT_INITIALIZER>
    void initializeElementsImpl(ELEMENT_INITIALIZER elementInitializer)
    {
        size_t index = 0;
        for (typename container_type::iterator it = container_type::begin(); it != container_type::end(); ++it)
        {
            elementInitializer.initialize(*it, index);
            index++;
        }
    }

    element_type sumImpl() const
    {
        return std::accumulate(container_type::begin(), container_type::end(), element_type());
    }

    int hashCodeImpl() const
    {
        int result = HASH_SEED;
        for (typename container_type::const_iterator it = container_type::begin(); it != container_type::end();
                ++it)
            result = calcHashCode(result, *it);

        return result;
    }

    template <typename BITSIZEOF_ALIGNER>
    size_t bitsizeOfImpl(size_t bitPosition, BITSIZEOF_ALIGNER aligner, uint8_t numBits = 0) const
    {
        size_t endBitPosition = bitPosition;
        for (typename container_type::const_iterator it = container_type::begin(); it != container_type::end();
                ++it)
        {
            endBitPosition = aligner.align(endBitPosition);
            endBitPosition += ARRAY_TRAITS::bitsizeOf(endBitPosition, *it, numBits);
        }

        return endBitPosition - bitPosition;
    }

    template <typename LABEL_OFFSET_SETTER_WRAPPER>
    size_t setLabelsImpl(size_t bitPosition, LABEL_OFFSET_SETTER_WRAPPER setterWrapper, uint8_t numBits = 0)
    {
        size_t endBitPosition = bitPosition;
        size_t index = 0;
        for (typename container_type::iterator it = container_type::begin(); it != container_type::end(); ++it)
        {
            endBitPosition = setterWrapper.alignAndSetOffset(index, endBitPosition);
            endBitPosition = ARRAY_TRAITS::setLabels(endBitPosition, *it, numBits);
            index++;
        }

        return endBitPosition;
    }

    template <typename ELEMENT_FACTORY, typename LABEL_OFFSET_CHECKER_WRAPPER>
    void readImpl(BitStreamReader& in, size_t size, ELEMENT_FACTORY elementFactory,
            LABEL_OFFSET_CHECKER_WRAPPER checkerWrapper, uint8_t numBits = 0)
    {
        container_type::clear();
        container_type::reserve(size);
        for (size_t index = 0; index < size; ++index)
        {
            checkerWrapper.alignAndCheckOffset(index, in);
            element_type* storage = reinterpret_cast<element_type*>(container_type::get_next_storage());
            ARRAY_TRAITS::read(storage, in, index, elementFactory, numBits);
            container_type::commit_storage(storage);
        }
    }

    template <typename ELEMENT_FACTORY>
    void readImpl(BitStreamReader& in, ImplicitLength, ELEMENT_FACTORY elementFactory, uint8_t numBits = 0)
    {
        container_type::clear();
        BitStreamReader::BitPosType bitPosition;
        try
        {
            while (true)
            {
                bitPosition = in.getBitPosition();
                const size_t index = container_type::size();
                element_type* storage = reinterpret_cast<element_type*>(container_type::get_next_storage());
                ARRAY_TRAITS::read(storage, in, index, elementFactory, numBits);
                container_type::commit_storage(storage);
            }
        }
        catch (BitStreamException&)
        {
            // implicit length arrays can be only at the end of stream, so walker is not confused
            in.setBitPosition(bitPosition);
        }
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename ELEMENT_FACTORY>
    void readImpl(const datascript::BlobInspectorNode& arrayNode, ELEMENT_FACTORY elementFactory,
            uint8_t numBits = 0)
    {
        container_type::clear();
        const Container<datascript::BlobInspectorNode>& arrayElements = arrayNode.getChildren();
        const size_t numArrayElements = arrayElements.size();
        container_type::reserve(numArrayElements);
        for (size_t index = 0; index < numArrayElements; ++index)
        {
            element_type* storage = reinterpret_cast<element_type*>(container_type::get_next_storage());
            ARRAY_TRAITS::read(storage, arrayElements[index], index, elementFactory, numBits);
            container_type::commit_storage(storage);
        }
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR

    template <typename LABEL_OFFSET_CHECKER_WRAPPER>
    void writeImpl(BitStreamWriter& out, LABEL_OFFSET_CHECKER_WRAPPER checkerWrapper, uint8_t numBits = 0)
    {
        size_t index = 0;
        for (typename container_type::iterator it = container_type::begin(); it != container_type::end(); ++it)
        {
            checkerWrapper.alignAndCheckOffset(index, out);
            ARRAY_TRAITS::write(out, *it, numBits);
            index++;
        }
    }

#ifdef DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
    template <typename BLOB_TREE_HANDLER, typename LABEL_OFFSET_CHECKER_WRAPPER>
    void writeImpl(BitStreamWriter& out, BLOB_TREE_HANDLER blobTreeHandler,
            LABEL_OFFSET_CHECKER_WRAPPER checkerWrapper, uint8_t numBits = 0)
    {
        size_t index = 0;
        for (typename container_type::iterator it = container_type::begin(); it != container_type::end(); ++it)
        {
            checkerWrapper.alignAndCheckOffset(index, out);
            ARRAY_TRAITS::write(out, blobTreeHandler, *it, numBits);
            index++;
        }
    }
#endif // DATASCRIPT_RUNTIME_INCLUDE_INSPECTOR
};

namespace detail
{

/**
 * A wrapper for real offset checker.
 *
 * Before the checker is called, the reader or writer needs to be aligned so using this actually has a side
 * effect.
 */
template <typename T>
class LabelOffsetCheckerWrapper
{
public:
    LabelOffsetCheckerWrapper(T &checker) : m_checker(checker)
    {}

    void alignAndCheckOffset(size_t index, BitStreamReader& in)
    {
        in.alignTo(NUM_BITS_PER_BYTE);
        m_checker.checkOffset(index, bitsToBytes(in.getBitPosition()));
    }

    void alignAndCheckOffset(size_t index, BitStreamWriter& out)
    {
        out.alignTo(NUM_BITS_PER_BYTE);
        m_checker.checkOffset(index, bitsToBytes(out.getBitPosition()));
    }

private:
    T& m_checker;
};

/**
 * A dummy check wrapper that does nothing.
 *
 * Unlike the real implementation this does not align the streams.
 */
struct DummyLabelOffsetCheckerWrapper
{
    void alignAndCheckOffset(size_t, BitStreamReader&)
    {}

    void alignAndCheckOffset(size_t, BitStreamWriter&)
    {}
};

/**
 * A wrapper for real offset setter.
 *
 * Before the setter is called, the bit position needs to be aligned.
 */
template <typename T>
class LabelOffsetSetterWrapper
{
public:
    LabelOffsetSetterWrapper(T &setter) : m_setter(setter)
    {}

    size_t alignAndSetOffset(size_t index, size_t bitPosition)
    {
        const size_t endBitPosition = alignTo(NUM_BITS_PER_BYTE, bitPosition);
        m_setter.setOffset(index, bitsToBytes(endBitPosition));

        return endBitPosition;
    }

private:
    T& m_setter;
};

/**
 * A dummy offset setter wrapper that does nothing.
 *
 * Unlike the real implementation this does not align the bit position.
 */
class DummyLabelOffsetSetterWrapper
{
public:
    size_t alignAndSetOffset(size_t, size_t bitPosition)
    {
        return bitPosition;
    }
};

/**
 * Real bitsizeof aligner.
 */
class BitSizeOfAligner
{
public:
    size_t align(size_t bitPosition)
    {
        return datascript::alignTo(NUM_BITS_PER_BYTE, bitPosition);
    }
};

/**
 * A dummy bitsizeof aligner that does nothing.
 *
 * Unlike the real implementation this does not align the bit position.
 */
class DummyBitSizeOfAligner
{
public:
    size_t align(size_t bitPosition)
    {
        return bitPosition;
    }
};

/**
 * A dummy element factory that does nothing.
 */
struct DummyElementFactory
{
};

} // namespace detail

} // namespace datascript

#endif // DATASCRIPT_ARRAY_BASE_H_INC
