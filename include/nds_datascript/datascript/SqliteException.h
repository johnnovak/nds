#ifndef DATASCRIPT_SQLITE_EXCEPTION_H_INC
#define DATASCRIPT_SQLITE_EXCEPTION_H_INC

#include <string>
#include <nds_sqlite3.h>

#include "CppRuntimeException.h"

namespace datascript
{

class SqliteException : public CppRuntimeException
{
public:
    explicit SqliteException(const std::string& message) : CppRuntimeException(message) {}

    SqliteException(const std::string& message, int sqliteCode) :
        CppRuntimeException(message + ": " + sqlite3_errstr(sqliteCode)) {}
};

} // namespace datascript

#endif // ifndef DATASCRIPT_SQLITE_EXCEPTION_H_INC
