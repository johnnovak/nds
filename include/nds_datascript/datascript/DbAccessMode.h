#ifndef DATASCRIPT_DB_ACCESS_MODE_H_INC
#define DATASCRIPT_DB_ACCESS_MODE_H_INC

namespace datascript
{

namespace DbAccessMode
{
    enum ModeEnum
    {
        CREATE = 1,
        READ   = 2,
        WRITE  = 3
    };
} // namespace DbAccessMode

} // namespace datascript

#endif // ifndef DATASCRIPT_DB_ACCESS_MODE_H_INC
