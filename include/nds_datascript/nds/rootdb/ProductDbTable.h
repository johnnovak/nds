/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ROOTDB_PRODUCT_DB_TABLE_H
#define NDS_ROOTDB_PRODUCT_DB_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/rootdb/ProductDbTableRow.h"
#include "nds/common/DataModelVersion.h"
#include "nds/common/NdsDbSupplierId.h"
#include "nds/common/ProductId.h"
#include "nds/common/VersionId.h"
#include "nds/rootdb/ProductRelationMask.h"
#include "nds/rootdb/ProductTypeMask.h"

namespace nds
{
namespace rootdb
{

class ProductDbTable
{
public:
    ProductDbTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~ProductDbTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<ProductDbTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<ProductDbTableRow>& rows) const;
    void write(const std::vector<ProductDbTableRow>& rows);
    void update(const ProductDbTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<ProductDbTableRow>& rows);
    static void writeRow(const ProductDbTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace rootdb
} // namespace nds

#endif // NDS_ROOTDB_PRODUCT_DB_TABLE_H
