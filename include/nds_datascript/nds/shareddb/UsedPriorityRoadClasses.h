/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SHAREDDB_USED_PRIORITY_ROAD_CLASSES_H
#define NDS_SHAREDDB_USED_PRIORITY_ROAD_CLASSES_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/BitFieldArray.h>
#include <datascript/Types.h>

namespace nds
{
namespace shareddb
{

class UsedPriorityRoadClasses
{
public:
    UsedPriorityRoadClasses();
    UsedPriorityRoadClasses(datascript::BitStreamReader& _in);

    uint8_t getNumUsedPriorityRoadClasses() const;
    void setNumUsedPriorityRoadClasses(uint8_t numUsedPriorityRoadClasses);

    datascript::UInt8Array& getUsedPriorityRoadClass();
    const datascript::UInt8Array& getUsedPriorityRoadClass() const;
    void setUsedPriorityRoadClass(const datascript::UInt8Array& usedPriorityRoadClass);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const UsedPriorityRoadClasses& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint8_t m_numUsedPriorityRoadClasses;
    datascript::UInt8Array m_usedPriorityRoadClass;
};

} // namespace shareddb
} // namespace nds

#endif // NDS_SHAREDDB_USED_PRIORITY_ROAD_CLASSES_H
