/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_JV_MAIN_JV_PATH_NUMBER_H
#define NDS_JV_MAIN_JV_PATH_NUMBER_H

#include <datascript/Types.h>

namespace nds
{
namespace jv
{
namespace main
{

typedef uint8_t JvPathNumber;

} // namespace main
} // namespace jv
} // namespace nds

#endif // NDS_JV_MAIN_JV_PATH_NUMBER_H
