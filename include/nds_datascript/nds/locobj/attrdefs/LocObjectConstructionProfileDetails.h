/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LOCOBJ_ATTRDEFS_LOC_OBJECT_CONSTRUCTION_PROFILE_DETAILS_H
#define NDS_LOCOBJ_ATTRDEFS_LOC_OBJECT_CONSTRUCTION_PROFILE_DETAILS_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

#include "nds/locobj/attrdefs/LocObjectConstructionProfileType.h"

namespace nds
{
namespace locobj
{
namespace attrdefs
{

class LocObjectConstructionProfileDetails
{
public:
    LocObjectConstructionProfileDetails();
    LocObjectConstructionProfileDetails(datascript::BitStreamReader& _in);

    nds::locobj::attrdefs::LocObjectConstructionProfileType getProfileType() const;
    void setProfileType(nds::locobj::attrdefs::LocObjectConstructionProfileType profileType);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const LocObjectConstructionProfileDetails& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::locobj::attrdefs::LocObjectConstructionProfileType m_profileType;
};

} // namespace attrdefs
} // namespace locobj
} // namespace nds

#endif // NDS_LOCOBJ_ATTRDEFS_LOC_OBJECT_CONSTRUCTION_PROFILE_DETAILS_H
