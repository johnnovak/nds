/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LOCOBJ_ATTRDEFS_LOC_TEMPLATE_ID_H
#define NDS_LOCOBJ_ATTRDEFS_LOC_TEMPLATE_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace locobj
{
namespace attrdefs
{

typedef int32_t LocTemplateId;

} // namespace attrdefs
} // namespace locobj
} // namespace nds

#endif // NDS_LOCOBJ_ATTRDEFS_LOC_TEMPLATE_ID_H
