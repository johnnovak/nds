/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ROUTING_INTERSECTION_SIMPLE_INTERSECTION_LIST_H
#define NDS_ROUTING_INTERSECTION_SIMPLE_INTERSECTION_LIST_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/ObjectArray.h>

#include "nds/common/CoordWidth.h"
#include "nds/common/NumRoutingListElements.h"
#include "nds/routing/intersection/SimpleIntersection.h"

namespace nds
{
namespace routing
{
namespace intersection
{

class SimpleIntersectionList
{
public:
    SimpleIntersectionList();
    SimpleIntersectionList(datascript::BitStreamReader& _in,
        nds::common::CoordWidth coordWidth);

    SimpleIntersectionList(const SimpleIntersectionList& _other);
    SimpleIntersectionList& operator=(const SimpleIntersectionList& _other);

    void initialize(
        nds::common::CoordWidth coordWidth);
    void initializeChildren();

    nds::common::CoordWidth getCoordWidth() const;

    nds::common::NumRoutingListElements getNumIntersections() const;
    void setNumIntersections(nds::common::NumRoutingListElements numIntersections);

    datascript::ObjectArray<nds::routing::intersection::SimpleIntersection>& getSimpleIntersection();
    const datascript::ObjectArray<nds::routing::intersection::SimpleIntersection>& getSimpleIntersection() const;
    void setSimpleIntersection(const datascript::ObjectArray<nds::routing::intersection::SimpleIntersection>& simpleIntersection);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const SimpleIntersectionList& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::CoordWidth m_coordWidth;
    bool m_isInitialized;
    nds::common::NumRoutingListElements m_numIntersections;
    datascript::ObjectArray<nds::routing::intersection::SimpleIntersection> m_simpleIntersection;
};

} // namespace intersection
} // namespace routing
} // namespace nds

#endif // NDS_ROUTING_INTERSECTION_SIMPLE_INTERSECTION_LIST_H
