/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ROUTING_MAIN_ROUTING_TILE_HEADER_H
#define NDS_ROUTING_MAIN_ROUTING_TILE_HEADER_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/RelativeBlobOffset.h"
#include "nds/routing/main/RoutingContentMask.h"

namespace nds
{
namespace routing
{
namespace main
{

class RoutingTileHeader
{
public:
    RoutingTileHeader();
    RoutingTileHeader(datascript::BitStreamReader& _in);

    nds::routing::main::RoutingContentMask& getContentMask();
    const nds::routing::main::RoutingContentMask& getContentMask() const;
    void setContentMask(const nds::routing::main::RoutingContentMask& contentMask);

    nds::common::RelativeBlobOffset getFixedRoadAttributeSetListOffset() const;
    void setFixedRoadAttributeSetListOffset(nds::common::RelativeBlobOffset fixedRoadAttributeSetListOffset);
    bool hasFixedRoadAttributeSetListOffset() const;

    nds::common::RelativeBlobOffset getSimpleIntersectionOffset() const;
    void setSimpleIntersectionOffset(nds::common::RelativeBlobOffset simpleIntersectionOffset);
    bool hasSimpleIntersectionOffset() const;

    nds::common::RelativeBlobOffset getLinksOffset() const;
    void setLinksOffset(nds::common::RelativeBlobOffset linksOffset);
    bool hasLinksOffset() const;

    nds::common::RelativeBlobOffset getRouteUpLinksOffset() const;
    void setRouteUpLinksOffset(nds::common::RelativeBlobOffset routeUpLinksOffset);
    bool hasRouteUpLinksOffset() const;

    nds::common::RelativeBlobOffset getRouteDownLinksOffset() const;
    void setRouteDownLinksOffset(nds::common::RelativeBlobOffset routeDownLinksOffset);
    bool hasRouteDownLinksOffset() const;

    nds::common::RelativeBlobOffset getLink2TileListOffset() const;
    void setLink2TileListOffset(nds::common::RelativeBlobOffset link2TileListOffset);
    bool hasLink2TileListOffset() const;

    nds::common::RelativeBlobOffset getLinkIdRangeListOffset() const;
    void setLinkIdRangeListOffset(nds::common::RelativeBlobOffset linkIdRangeListOffset);
    bool hasLinkIdRangeListOffset() const;

    nds::common::RelativeBlobOffset getSimplIntIdRangeListOffset() const;
    void setSimplIntIdRangeListOffset(nds::common::RelativeBlobOffset simplIntIdRangeListOffset);
    bool hasSimplIntIdRangeListOffset() const;

    nds::common::RelativeBlobOffset getAttributeMapsOffset() const;
    void setAttributeMapsOffset(nds::common::RelativeBlobOffset attributeMapsOffset);
    bool hasAttributeMapsOffset() const;

    nds::common::RelativeBlobOffset getExternalTileIdListOffset() const;
    void setExternalTileIdListOffset(nds::common::RelativeBlobOffset externalTileIdListOffset);
    bool hasExternalTileIdListOffset() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const RoutingTileHeader& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    datascript::InPlaceOptionalHolder<nds::routing::main::RoutingContentMask> m_contentMask;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_fixedRoadAttributeSetListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_simpleIntersectionOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_linksOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_routeUpLinksOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_routeDownLinksOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_link2TileListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_linkIdRangeListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_simplIntIdRangeListOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_attributeMapsOffset;
    datascript::InPlaceOptionalHolder<nds::common::RelativeBlobOffset> m_externalTileIdListOffset;
};

} // namespace main
} // namespace routing
} // namespace nds

#endif // NDS_ROUTING_MAIN_ROUTING_TILE_HEADER_H
