/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ROUTING_MAIN_TOLL_COST_TABLE_ROW_H
#define NDS_ROUTING_MAIN_TOLL_COST_TABLE_ROW_H

#include <datascript/Types.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/GatewayNumber.h"
#include "nds/common/TollGateId.h"
#include "nds/common/flexattr/attrmaps/GroupedAttributeList.h"

namespace nds
{
namespace routing
{
namespace main
{

class TollCostTableRow
{
public:
    TollCostTableRow();
    ~TollCostTableRow();

    nds::common::TollGateId getGateIdFrom() const;
    void setGateIdFrom(nds::common::TollGateId gateIdFrom);
    bool isNullGateIdFrom() const;
    void setNullGateIdFrom();

    uint16_t getSequenceNumber() const;
    void setSequenceNumber(uint16_t sequenceNumber);
    bool isNullSequenceNumber() const;
    void setNullSequenceNumber();

    nds::common::TollGateId getGateIdTo() const;
    void setGateIdTo(nds::common::TollGateId gateIdTo);
    bool isNullGateIdTo() const;
    void setNullGateIdTo();

    nds::common::GatewayNumber getTollGatewayId() const;
    void setTollGatewayId(nds::common::GatewayNumber tollGatewayId);
    bool isNullTollGatewayId() const;
    void setNullTollGatewayId();

    const nds::common::flexattr::attrmaps::GroupedAttributeList& getPaymentConditions() const;
    void setPaymentConditions(const nds::common::flexattr::attrmaps::GroupedAttributeList& paymentConditions);
    bool isNullPaymentConditions() const;
    void setNullPaymentConditions();

private:
    datascript::InPlaceOptionalHolder<nds::common::TollGateId> m_gateIdFrom;
    datascript::InPlaceOptionalHolder<uint16_t> m_sequenceNumber;
    datascript::InPlaceOptionalHolder<nds::common::TollGateId> m_gateIdTo;
    datascript::InPlaceOptionalHolder<nds::common::GatewayNumber> m_tollGatewayId;
    datascript::InPlaceOptionalHolder<nds::common::flexattr::attrmaps::GroupedAttributeList> m_paymentConditions;
};

} // namespace main
} // namespace routing
} // namespace nds

#endif // NDS_ROUTING_MAIN_TOLL_COST_TABLE_ROW_H
