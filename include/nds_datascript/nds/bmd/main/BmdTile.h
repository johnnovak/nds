/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_BMD_MAIN_BMD_TILE_H
#define NDS_BMD_MAIN_BMD_TILE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/bmd/areas/AreaFeatureData.h"
#include "nds/bmd/lines/LineFeatureData.h"
#include "nds/bmd/main/BmdIdRangeList.h"
#include "nds/bmd/main/BmdTileHeader.h"
#include "nds/bmd/points/PointFeatureData.h"
#include "nds/common/CoordWidth.h"
#include "nds/common/flexattr/attrmaps/AttributeMapList.h"

namespace nds
{
namespace bmd
{
namespace main
{

class BmdTile
{
public:
    BmdTile();
    BmdTile(datascript::BitStreamReader& _in,
        nds::common::CoordWidth coordWidth);

    BmdTile(const BmdTile& _other);
    BmdTile& operator=(const BmdTile& _other);

    void initialize(
        nds::common::CoordWidth coordWidth);
    void initializeChildren();

    nds::common::CoordWidth getCoordWidth() const;

    nds::bmd::main::BmdTileHeader& getHeader();
    const nds::bmd::main::BmdTileHeader& getHeader() const;
    void setHeader(const nds::bmd::main::BmdTileHeader& header);

    nds::bmd::areas::AreaFeatureData& getAreas();
    const nds::bmd::areas::AreaFeatureData& getAreas() const;
    void setAreas(const nds::bmd::areas::AreaFeatureData& areas);
    bool hasAreas() const;

    nds::bmd::main::BmdIdRangeList& getAreaIdRangeList();
    const nds::bmd::main::BmdIdRangeList& getAreaIdRangeList() const;
    void setAreaIdRangeList(const nds::bmd::main::BmdIdRangeList& areaIdRangeList);
    bool hasAreaIdRangeList() const;

    nds::bmd::lines::LineFeatureData& getLines();
    const nds::bmd::lines::LineFeatureData& getLines() const;
    void setLines(const nds::bmd::lines::LineFeatureData& lines);
    bool hasLines() const;

    nds::bmd::main::BmdIdRangeList& getLineIdRangeList();
    const nds::bmd::main::BmdIdRangeList& getLineIdRangeList() const;
    void setLineIdRangeList(const nds::bmd::main::BmdIdRangeList& lineIdRangeList);
    bool hasLineIdRangeList() const;

    nds::bmd::points::PointFeatureData& getPoints();
    const nds::bmd::points::PointFeatureData& getPoints() const;
    void setPoints(const nds::bmd::points::PointFeatureData& points);
    bool hasPoints() const;

    nds::bmd::main::BmdIdRangeList& getPointIdRangeList();
    const nds::bmd::main::BmdIdRangeList& getPointIdRangeList() const;
    void setPointIdRangeList(const nds::bmd::main::BmdIdRangeList& pointIdRangeList);
    bool hasPointIdRangeList() const;

    nds::common::flexattr::attrmaps::AttributeMapList& getFlexibleAttributes();
    const nds::common::flexattr::attrmaps::AttributeMapList& getFlexibleAttributes() const;
    void setFlexibleAttributes(const nds::common::flexattr::attrmaps::AttributeMapList& flexibleAttributes);
    bool hasFlexibleAttributes() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const BmdTile& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::CoordWidth m_coordWidth;
    bool m_isInitialized;
    datascript::InPlaceOptionalHolder<nds::bmd::main::BmdTileHeader> m_header;
    datascript::OptimizedOptionalHolder<nds::bmd::areas::AreaFeatureData> m_areas;
    datascript::OptimizedOptionalHolder<nds::bmd::main::BmdIdRangeList> m_areaIdRangeList;
    datascript::OptimizedOptionalHolder<nds::bmd::lines::LineFeatureData> m_lines;
    datascript::OptimizedOptionalHolder<nds::bmd::main::BmdIdRangeList> m_lineIdRangeList;
    datascript::OptimizedOptionalHolder<nds::bmd::points::PointFeatureData> m_points;
    datascript::OptimizedOptionalHolder<nds::bmd::main::BmdIdRangeList> m_pointIdRangeList;
    datascript::OptimizedOptionalHolder<nds::common::flexattr::attrmaps::AttributeMapList> m_flexibleAttributes;
};

} // namespace main
} // namespace bmd
} // namespace nds

#endif // NDS_BMD_MAIN_BMD_TILE_H
