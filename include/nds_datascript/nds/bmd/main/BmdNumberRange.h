/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_BMD_MAIN_BMD_NUMBER_RANGE_H
#define NDS_BMD_MAIN_BMD_NUMBER_RANGE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

#include "nds/common/NumBmdFeatures.h"

namespace nds
{
namespace bmd
{
namespace main
{

class BmdNumberRange
{
public:
    BmdNumberRange();
    BmdNumberRange(datascript::BitStreamReader& _in);

    nds::common::NumBmdFeatures getStartFeatureId() const;
    void setStartFeatureId(nds::common::NumBmdFeatures startFeatureId);

    nds::common::NumBmdFeatures getEndFeatureId() const;
    void setEndFeatureId(nds::common::NumBmdFeatures endFeatureId);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const BmdNumberRange& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::NumBmdFeatures m_startFeatureId;
    nds::common::NumBmdFeatures m_endFeatureId;
};

} // namespace main
} // namespace bmd
} // namespace nds

#endif // NDS_BMD_MAIN_BMD_NUMBER_RANGE_H
