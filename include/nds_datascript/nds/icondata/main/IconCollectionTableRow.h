/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ICONDATA_MAIN_ICON_COLLECTION_TABLE_ROW_H
#define NDS_ICONDATA_MAIN_ICON_COLLECTION_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/VersionId.h"
#include "nds/icondata/main/IconCollectionId.h"

namespace nds
{
namespace icondata
{
namespace main
{

class IconCollectionTableRow
{
public:
    IconCollectionTableRow();
    ~IconCollectionTableRow();

    nds::icondata::main::IconCollectionId getCollectionId() const;
    void setCollectionId(nds::icondata::main::IconCollectionId collectionId);
    bool isNullCollectionId() const;
    void setNullCollectionId();

    nds::common::VersionId getVersionId() const;
    void setVersionId(nds::common::VersionId versionId);
    bool isNullVersionId() const;
    void setNullVersionId();

private:
    datascript::InPlaceOptionalHolder<nds::icondata::main::IconCollectionId> m_collectionId;
    datascript::InPlaceOptionalHolder<nds::common::VersionId> m_versionId;
};

} // namespace main
} // namespace icondata
} // namespace nds

#endif // NDS_ICONDATA_MAIN_ICON_COLLECTION_TABLE_ROW_H
