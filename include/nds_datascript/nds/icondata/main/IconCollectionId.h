/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ICONDATA_MAIN_ICON_COLLECTION_ID_H
#define NDS_ICONDATA_MAIN_ICON_COLLECTION_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace icondata
{
namespace main
{

typedef int8_t IconCollectionId;

} // namespace main
} // namespace icondata
} // namespace nds

#endif // NDS_ICONDATA_MAIN_ICON_COLLECTION_ID_H
