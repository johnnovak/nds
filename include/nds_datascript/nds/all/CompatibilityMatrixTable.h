/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_ALL_COMPATIBILITY_MATRIX_TABLE_H
#define NDS_ALL_COMPATIBILITY_MATRIX_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/all/CompatibilityMatrixTableRow.h"
#include "nds/common/DataModelVersion.h"

namespace nds
{
namespace all
{

class CompatibilityMatrixTable
{
public:
    CompatibilityMatrixTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~CompatibilityMatrixTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<CompatibilityMatrixTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<CompatibilityMatrixTableRow>& rows) const;
    void write(const std::vector<CompatibilityMatrixTableRow>& rows);
    void update(const CompatibilityMatrixTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<CompatibilityMatrixTableRow>& rows);
    static void writeRow(const CompatibilityMatrixTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace all
} // namespace nds

#endif // NDS_ALL_COMPATIBILITY_MATRIX_TABLE_H
