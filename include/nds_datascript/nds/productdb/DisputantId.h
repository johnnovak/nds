/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_PRODUCTDB_DISPUTANT_ID_H
#define NDS_PRODUCTDB_DISPUTANT_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace productdb
{

typedef uint8_t DisputantId;

} // namespace productdb
} // namespace nds

#endif // NDS_PRODUCTDB_DISPUTANT_ID_H
