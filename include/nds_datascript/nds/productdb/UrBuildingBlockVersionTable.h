/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_PRODUCTDB_UR_BUILDING_BLOCK_VERSION_TABLE_H
#define NDS_PRODUCTDB_UR_BUILDING_BLOCK_VERSION_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/productdb/UrBuildingBlockVersionTableRow.h"
#include "nds/common/BuildingBlockId.h"
#include "nds/common/BuildingBlockType.h"
#include "nds/common/DataModelVersionId.h"
#include "nds/common/EncryptionKeyId.h"
#include "nds/common/UpdateRegionId.h"
#include "nds/common/VersionId.h"
#include "nds/shareddb/BuildingBlockMetadata.h"

namespace nds
{
namespace productdb
{

class UrBuildingBlockVersionTable
{
public:
    UrBuildingBlockVersionTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~UrBuildingBlockVersionTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<UrBuildingBlockVersionTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<UrBuildingBlockVersionTableRow>& rows) const;
    void write(const std::vector<UrBuildingBlockVersionTableRow>& rows);
    void update(const UrBuildingBlockVersionTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<UrBuildingBlockVersionTableRow>& rows);
    static void writeRow(const UrBuildingBlockVersionTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace productdb
} // namespace nds

#endif // NDS_PRODUCTDB_UR_BUILDING_BLOCK_VERSION_TABLE_H
