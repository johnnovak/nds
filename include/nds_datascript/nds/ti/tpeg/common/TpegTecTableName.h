/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TPEG_COMMON_TPEG_TEC_TABLE_NAME_H
#define NDS_TI_TPEG_COMMON_TPEG_TEC_TABLE_NAME_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace ti
{
namespace tpeg
{
namespace common
{

class TpegTecTableName
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint16_t _base_type;

    enum e_TpegTecTableName
    {
        EFFECT_CODE = UINT16_C(1),
        CAUSE_CODE = UINT16_C(2),
        LANE_RESTRICTION = UINT16_C(4),
        ADVICE_CODE = UINT16_C(5),
        TENDENCY = UINT16_C(6),
        RESTRICTION_TYPE = UINT16_C(7),
        VEHICLE_TYPE = UINT16_C(9),
        TRAFFIC_CONGESTION = UINT16_C(101),
        ACCIDENT = UINT16_C(102),
        ROADWORKS = UINT16_C(103),
        NARROW_LANES = UINT16_C(104),
        IMPASSIBILITY = UINT16_C(105),
        SLIPPERY_ROAD = UINT16_C(106),
        FIRE = UINT16_C(108),
        HAZARDOUS_DRIVING_CONDITIONS = UINT16_C(109),
        OBJECTS_ON_THE_ROAD = UINT16_C(110),
        ANIMALS_ON_ROADWAY = UINT16_C(111),
        PEOPLE_ON_ROADWAY = UINT16_C(112),
        BROKEN_DOWN_VEHICLES = UINT16_C(113),
        RESCUE_AND_RECOVERY_WORK_IN_PROGRESS = UINT16_C(115),
        REGULATORY_MEASURE = UINT16_C(116),
        EXTREME_WEATHER_CONDITIONS = UINT16_C(117),
        VISIBILITY_REDUCED = UINT16_C(118),
        PRECIPITATION = UINT16_C(119),
        RECKLESS_PERSONS = UINT16_C(120),
        MAJOR_EVENT = UINT16_C(123),
        SERVICE_NOT_OPERATING = UINT16_C(124),
        SERVICE_NOT_USEABLE = UINT16_C(125),
        SLOW_MOVING_VEHICLES = UINT16_C(126),
        DANGEROUS_END_OF_QUEUE = UINT16_C(127),
        RISK_OF_FIRE = UINT16_C(128),
        TIME_DELAY = UINT16_C(129),
        POLICE_CHECKPOINT = UINT16_C(130),
        MALFUNCTIONING_ROADSIDE_EQUIPMENT = UINT16_C(131),
        OVERTAKING_NOT_ALLOWED = UINT16_C(202),
        DRIVING_NOT_ALLOWED = UINT16_C(203),
        GIVING_PATH_VEHICLES_FROM_BACKWARD = UINT16_C(207),
        FOLLOW_DIVERSION = UINT16_C(208),
        DRIVE_CAREFULLY = UINT16_C(213),
        DO_NOT_LEAVE_YOUR_VEHICLE = UINT16_C(214),
        USE_TOLL_LANES = UINT16_C(216)
    };

    TpegTecTableName();
    TpegTecTableName(e_TpegTecTableName value);
    explicit TpegTecTableName(datascript::BitStreamReader& _in);

    operator e_TpegTecTableName() const;
    uint16_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const TpegTecTableName& other) const;
    bool operator==(e_TpegTecTableName other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static TpegTecTableName toEnum(uint16_t rawValue);

private:
    e_TpegTecTableName m_value;
};

} // namespace common
} // namespace tpeg
} // namespace ti
} // namespace nds

#endif // NDS_TI_TPEG_COMMON_TPEG_TEC_TABLE_NAME_H
