/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TPEG_EVENT_TPEG_EVENT_TABLE_H
#define NDS_TI_TPEG_EVENT_TPEG_EVENT_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ti/tpeg/event/TpegEventTableRow.h"
#include "nds/ti/common/JamCategory.h"
#include "nds/ti/common/TiDeviceOutputMask.h"
#include "nds/ti/tpeg/common/TpegApplication.h"
#include "nds/ti/tpeg/common/TpegEventCode.h"
#include "nds/ti/tpeg/common/TpegEventUrgency.h"
#include "nds/ti/tpeg/common/TpegTableNumber.h"

namespace nds
{
namespace ti
{
namespace tpeg
{
namespace event
{

class TpegEventTable
{
public:
    TpegEventTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TpegEventTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TpegEventTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TpegEventTableRow>& rows) const;
    void write(const std::vector<TpegEventTableRow>& rows);
    void update(const TpegEventTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TpegEventTableRow>& rows);
    static void writeRow(const TpegEventTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace event
} // namespace tpeg
} // namespace ti
} // namespace nds

#endif // NDS_TI_TPEG_EVENT_TPEG_EVENT_TABLE_H
