/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TMC_LOCATION_TMC_STATION_TILE_LIST_TABLE_ROW_H
#define NDS_TI_TMC_LOCATION_TMC_STATION_TILE_LIST_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/PackedTileId.h"
#include "nds/ti/tmc/common/TmcStationId.h"

namespace nds
{
namespace ti
{
namespace tmc
{
namespace location
{

class TmcStationTileListTableRow
{
public:
    TmcStationTileListTableRow();
    ~TmcStationTileListTableRow();

    nds::ti::tmc::common::TmcStationId getId() const;
    void setId(nds::ti::tmc::common::TmcStationId id);
    bool isNullId() const;
    void setNullId();

    nds::common::PackedTileId getTileId() const;
    void setTileId(nds::common::PackedTileId tileId);
    bool isNullTileId() const;
    void setNullTileId();

private:
    datascript::InPlaceOptionalHolder<nds::ti::tmc::common::TmcStationId> m_id;
    datascript::InPlaceOptionalHolder<nds::common::PackedTileId> m_tileId;
};

} // namespace location
} // namespace tmc
} // namespace ti
} // namespace nds

#endif // NDS_TI_TMC_LOCATION_TMC_STATION_TILE_LIST_TABLE_ROW_H
