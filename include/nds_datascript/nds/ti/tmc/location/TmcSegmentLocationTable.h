/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_TI_TMC_LOCATION_TMC_SEGMENT_LOCATION_TABLE_H
#define NDS_TI_TMC_LOCATION_TMC_SEGMENT_LOCATION_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/ti/tmc/location/TmcSegmentLocationTableRow.h"
#include "nds/name/namestring/RoadNumberList.h"
#include "nds/ti/common/TiNameId.h"
#include "nds/ti/tmc/common/TmcLocationId.h"

namespace nds
{
namespace ti
{
namespace tmc
{
namespace location
{

class TmcSegmentLocationTable
{
public:
    TmcSegmentLocationTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TmcSegmentLocationTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TmcSegmentLocationTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TmcSegmentLocationTableRow>& rows) const;
    void write(const std::vector<TmcSegmentLocationTableRow>& rows);
    void update(const TmcSegmentLocationTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TmcSegmentLocationTableRow>& rows);
    static void writeRow(const TmcSegmentLocationTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace location
} // namespace tmc
} // namespace ti
} // namespace nds

#endif // NDS_TI_TMC_LOCATION_TMC_SEGMENT_LOCATION_TABLE_H
