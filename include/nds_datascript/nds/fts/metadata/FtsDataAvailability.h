/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_FTS_METADATA_FTS_DATA_AVAILABILITY_H
#define NDS_FTS_METADATA_FTS_DATA_AVAILABILITY_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace fts
{
namespace metadata
{

class FtsDataAvailability
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_FtsDataAvailability
    {
        NONE = UINT8_C(0),
        INDEX_ONLY = UINT8_C(1),
        ALL = UINT8_C(2),
        INDEX_AND_REFERENCE = UINT8_C(3)
    };

    FtsDataAvailability();
    FtsDataAvailability(e_FtsDataAvailability value);
    explicit FtsDataAvailability(datascript::BitStreamReader& _in);

    operator e_FtsDataAvailability() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const FtsDataAvailability& other) const;
    bool operator==(e_FtsDataAvailability other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static FtsDataAvailability toEnum(uint8_t rawValue);

private:
    e_FtsDataAvailability m_value;
};

} // namespace metadata
} // namespace fts
} // namespace nds

#endif // NDS_FTS_METADATA_FTS_DATA_AVAILABILITY_H
