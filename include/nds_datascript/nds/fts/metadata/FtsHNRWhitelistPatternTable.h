/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_FTS_METADATA_FTS_HNRWHITELIST_PATTERN_TABLE_H
#define NDS_FTS_METADATA_FTS_HNRWHITELIST_PATTERN_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/fts/metadata/FtsHNRWhitelistPatternTableRow.h"
#include "nds/common/UpdateRegionId.h"

namespace nds
{
namespace fts
{
namespace metadata
{

class FtsHNRWhitelistPatternTable
{
public:
    FtsHNRWhitelistPatternTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~FtsHNRWhitelistPatternTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<FtsHNRWhitelistPatternTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<FtsHNRWhitelistPatternTableRow>& rows) const;
    void write(const std::vector<FtsHNRWhitelistPatternTableRow>& rows);
    void update(const FtsHNRWhitelistPatternTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<FtsHNRWhitelistPatternTableRow>& rows);
    static void writeRow(const FtsHNRWhitelistPatternTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace metadata
} // namespace fts
} // namespace nds

#endif // NDS_FTS_METADATA_FTS_HNRWHITELIST_PATTERN_TABLE_H
