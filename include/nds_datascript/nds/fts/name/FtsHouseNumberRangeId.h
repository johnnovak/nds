/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_FTS_NAME_FTS_HOUSE_NUMBER_RANGE_ID_H
#define NDS_FTS_NAME_FTS_HOUSE_NUMBER_RANGE_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace fts
{
namespace name
{

typedef uint32_t FtsHouseNumberRangeId;

} // namespace name
} // namespace fts
} // namespace nds

#endif // NDS_FTS_NAME_FTS_HOUSE_NUMBER_RANGE_ID_H
