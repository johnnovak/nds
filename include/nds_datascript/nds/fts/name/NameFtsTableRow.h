/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_FTS_NAME_NAME_FTS_TABLE_ROW_H
#define NDS_FTS_NAME_NAME_FTS_TABLE_ROW_H

#include <datascript/Types.h>
#include <string>
#include <datascript/OptionalHolder.h>

#include "nds/common/MortonCode.h"
#include "nds/common/NamedObjectClass.h"
#include "nds/common/UpdateRegionId.h"
#include "nds/name/common/NamedObjectId.h"

namespace nds
{
namespace fts
{
namespace name
{

class NameFtsTableRow
{
public:
    NameFtsTableRow();
    ~NameFtsTableRow();

    int64_t getRowid() const;
    void setRowid(int64_t rowid);
    bool isNullRowid() const;
    void setNullRowid();

    nds::name::common::NamedObjectId getNamedObjectId() const;
    void setNamedObjectId(nds::name::common::NamedObjectId namedObjectId);
    bool isNullNamedObjectId() const;
    void setNullNamedObjectId();

    nds::common::NamedObjectClass getNamedObjectClass() const;
    void setNamedObjectClass(nds::common::NamedObjectClass namedObjectClass);
    bool isNullNamedObjectClass() const;
    void setNullNamedObjectClass();

    nds::common::MortonCode getMortonCode() const;
    void setMortonCode(nds::common::MortonCode mortonCode);
    bool isNullMortonCode() const;
    void setNullMortonCode();

    nds::common::UpdateRegionId getUpdateRegionId() const;
    void setUpdateRegionId(nds::common::UpdateRegionId updateRegionId);
    bool isNullUpdateRegionId() const;
    void setNullUpdateRegionId();

    const std::string& getCriterionA() const;
    void setCriterionA(const std::string& criterionA);
    bool isNullCriterionA() const;
    void setNullCriterionA();

    const std::string& getCriterionB() const;
    void setCriterionB(const std::string& criterionB);
    bool isNullCriterionB() const;
    void setNullCriterionB();

    const std::string& getCriterionC() const;
    void setCriterionC(const std::string& criterionC);
    bool isNullCriterionC() const;
    void setNullCriterionC();

    const std::string& getCriterionD() const;
    void setCriterionD(const std::string& criterionD);
    bool isNullCriterionD() const;
    void setNullCriterionD();

    const std::string& getCriterionE() const;
    void setCriterionE(const std::string& criterionE);
    bool isNullCriterionE() const;
    void setNullCriterionE();

    const std::string& getCriterionF() const;
    void setCriterionF(const std::string& criterionF);
    bool isNullCriterionF() const;
    void setNullCriterionF();

    const std::string& getCriterionG() const;
    void setCriterionG(const std::string& criterionG);
    bool isNullCriterionG() const;
    void setNullCriterionG();

    const std::string& getCriterionH() const;
    void setCriterionH(const std::string& criterionH);
    bool isNullCriterionH() const;
    void setNullCriterionH();

    const std::string& getCriterionI() const;
    void setCriterionI(const std::string& criterionI);
    bool isNullCriterionI() const;
    void setNullCriterionI();

    const std::string& getCriterionJ() const;
    void setCriterionJ(const std::string& criterionJ);
    bool isNullCriterionJ() const;
    void setNullCriterionJ();

    const std::string& getSearchTags() const;
    void setSearchTags(const std::string& searchTags);
    bool isNullSearchTags() const;
    void setNullSearchTags();

    const std::string& getExceptionTags() const;
    void setExceptionTags(const std::string& exceptionTags);
    bool isNullExceptionTags() const;
    void setNullExceptionTags();

    int64_t getPriorityIndex() const;
    void setPriorityIndex(int64_t priorityIndex);
    bool isNullPriorityIndex() const;
    void setNullPriorityIndex();

private:
    datascript::InPlaceOptionalHolder<int64_t> m_rowid;
    datascript::InPlaceOptionalHolder<nds::name::common::NamedObjectId> m_namedObjectId;
    datascript::InPlaceOptionalHolder<nds::common::NamedObjectClass> m_namedObjectClass;
    datascript::InPlaceOptionalHolder<nds::common::MortonCode> m_mortonCode;
    datascript::InPlaceOptionalHolder<nds::common::UpdateRegionId> m_updateRegionId;
    datascript::InPlaceOptionalHolder<std::string> m_criterionA;
    datascript::InPlaceOptionalHolder<std::string> m_criterionB;
    datascript::InPlaceOptionalHolder<std::string> m_criterionC;
    datascript::InPlaceOptionalHolder<std::string> m_criterionD;
    datascript::InPlaceOptionalHolder<std::string> m_criterionE;
    datascript::InPlaceOptionalHolder<std::string> m_criterionF;
    datascript::InPlaceOptionalHolder<std::string> m_criterionG;
    datascript::InPlaceOptionalHolder<std::string> m_criterionH;
    datascript::InPlaceOptionalHolder<std::string> m_criterionI;
    datascript::InPlaceOptionalHolder<std::string> m_criterionJ;
    datascript::InPlaceOptionalHolder<std::string> m_searchTags;
    datascript::InPlaceOptionalHolder<std::string> m_exceptionTags;
    datascript::InPlaceOptionalHolder<int64_t> m_priorityIndex;
};

} // namespace name
} // namespace fts
} // namespace nds

#endif // NDS_FTS_NAME_NAME_FTS_TABLE_ROW_H
