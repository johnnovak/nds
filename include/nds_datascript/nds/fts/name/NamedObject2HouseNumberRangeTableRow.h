/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_FTS_NAME_NAMED_OBJECT2_HOUSE_NUMBER_RANGE_TABLE_ROW_H
#define NDS_FTS_NAME_NAMED_OBJECT2_HOUSE_NUMBER_RANGE_TABLE_ROW_H

#include <datascript/Types.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/UpdateRegionId.h"

namespace nds
{
namespace fts
{
namespace name
{

class NamedObject2HouseNumberRangeTableRow
{
public:
    NamedObject2HouseNumberRangeTableRow();
    ~NamedObject2HouseNumberRangeTableRow();

    int64_t getNamedObject2HouseNumberRangeId() const;
    void setNamedObject2HouseNumberRangeId(int64_t namedObject2HouseNumberRangeId);
    bool isNullNamedObject2HouseNumberRangeId() const;
    void setNullNamedObject2HouseNumberRangeId();

    nds::common::UpdateRegionId getUpdateRegionId() const;
    void setUpdateRegionId(nds::common::UpdateRegionId updateRegionId);
    bool isNullUpdateRegionId() const;
    void setNullUpdateRegionId();

private:
    datascript::InPlaceOptionalHolder<int64_t> m_namedObject2HouseNumberRangeId;
    datascript::InPlaceOptionalHolder<nds::common::UpdateRegionId> m_updateRegionId;
};

} // namespace name
} // namespace fts
} // namespace nds

#endif // NDS_FTS_NAME_NAMED_OBJECT2_HOUSE_NUMBER_RANGE_TABLE_ROW_H
