/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LANE_MAIN_LANE_TILE_CONTENT_MASK_H
#define NDS_LANE_MAIN_LANE_TILE_CONTENT_MASK_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

namespace nds
{
namespace lane
{
namespace main
{

class LaneTileContentMask
{
public:
    LaneTileContentMask();
    LaneTileContentMask(datascript::BitStreamReader& _in);

    bool getHasAttributeMaps() const;
    void setHasAttributeMaps(bool hasAttributeMaps);

    bool getHasExternalTileIdList() const;
    void setHasExternalTileIdList(bool hasExternalTileIdList);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const LaneTileContentMask& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_hasAttributeMaps;
    bool m_hasExternalTileIdList;
};

} // namespace main
} // namespace lane
} // namespace nds

#endif // NDS_LANE_MAIN_LANE_TILE_CONTENT_MASK_H
