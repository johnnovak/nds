/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LANE_MAIN_LANE_TILE_TABLE_ROW_H
#define NDS_LANE_MAIN_LANE_TILE_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/PackedTileId.h"
#include "nds/common/VersionId.h"
#include "nds/lane/main/LaneTile.h"

namespace nds
{
namespace lane
{
namespace main
{

class LaneTileTableRow
{
public:
    LaneTileTableRow();
    ~LaneTileTableRow();

    nds::common::PackedTileId getId() const;
    void setId(nds::common::PackedTileId id);
    bool isNullId() const;
    void setNullId();

    nds::common::VersionId getVersionId() const;
    void setVersionId(nds::common::VersionId versionId);
    bool isNullVersionId() const;
    void setNullVersionId();

    const nds::lane::main::LaneTile& getNdsData() const;
    void setNdsData(const nds::lane::main::LaneTile& ndsData);
    bool isNullNdsData() const;
    void setNullNdsData();

private:
    datascript::InPlaceOptionalHolder<nds::common::PackedTileId> m_id;
    datascript::InPlaceOptionalHolder<nds::common::VersionId> m_versionId;
    datascript::InPlaceOptionalHolder<nds::lane::main::LaneTile> m_ndsData;
};

} // namespace main
} // namespace lane
} // namespace nds

#endif // NDS_LANE_MAIN_LANE_TILE_TABLE_ROW_H
