/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LANE_ATTRDEFS_LANE_CONNECTIVITY_H
#define NDS_LANE_ATTRDEFS_LANE_CONNECTIVITY_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

#include "nds/lane/attrdefs/LaneConnectorId.h"

namespace nds
{
namespace lane
{
namespace attrdefs
{

class LaneConnectivity
{
public:
    LaneConnectivity();
    LaneConnectivity(datascript::BitStreamReader& _in);

    nds::lane::attrdefs::LaneConnectorId getSourceLaneConnectorId() const;
    void setSourceLaneConnectorId(nds::lane::attrdefs::LaneConnectorId sourceLaneConnectorId);

    nds::lane::attrdefs::LaneConnectorId getDestLaneConnectorId() const;
    void setDestLaneConnectorId(nds::lane::attrdefs::LaneConnectorId destLaneConnectorId);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const LaneConnectivity& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::lane::attrdefs::LaneConnectorId m_sourceLaneConnectorId;
    nds::lane::attrdefs::LaneConnectorId m_destLaneConnectorId;
};

} // namespace attrdefs
} // namespace lane
} // namespace nds

#endif // NDS_LANE_ATTRDEFS_LANE_CONNECTIVITY_H
