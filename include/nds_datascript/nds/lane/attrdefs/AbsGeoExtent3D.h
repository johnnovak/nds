/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_LANE_ATTRDEFS_ABS_GEO_EXTENT3_D_H
#define NDS_LANE_ATTRDEFS_ABS_GEO_EXTENT3_D_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/geometry3d/Vector3D.h"
#include "nds/lane/common/Circle.h"
#include "nds/lane/common/GeometryType.h"
#include "nds/lane/common/Rectangle.h"

namespace nds
{
namespace lane
{
namespace attrdefs
{

class AbsGeoExtent3D
{
public:
    AbsGeoExtent3D();
    AbsGeoExtent3D(datascript::BitStreamReader& _in);

    AbsGeoExtent3D(const AbsGeoExtent3D& _other);
    AbsGeoExtent3D& operator=(const AbsGeoExtent3D& _other);

    void initializeChildren();

    nds::lane::common::GeometryType getGeometryType() const;
    void setGeometryType(nds::lane::common::GeometryType geometryType);

    nds::common::geometry3d::Vector3D& getPoint();
    const nds::common::geometry3d::Vector3D& getPoint() const;
    void setPoint(const nds::common::geometry3d::Vector3D& point);
    bool hasPoint() const;

    nds::lane::common::Circle& getCircle();
    const nds::lane::common::Circle& getCircle() const;
    void setCircle(const nds::lane::common::Circle& circle);
    bool hasCircle() const;

    nds::lane::common::Rectangle& getRectangle();
    const nds::lane::common::Rectangle& getRectangle() const;
    void setRectangle(const nds::lane::common::Rectangle& rectangle);
    bool hasRectangle() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const AbsGeoExtent3D& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_areChildrenInitialized;
    nds::lane::common::GeometryType m_geometryType;
    datascript::OptimizedOptionalHolder<nds::common::geometry3d::Vector3D> m_point;
    datascript::OptimizedOptionalHolder<nds::lane::common::Circle> m_circle;
    datascript::OptimizedOptionalHolder<nds::lane::common::Rectangle> m_rectangle;
};

} // namespace attrdefs
} // namespace lane
} // namespace nds

#endif // NDS_LANE_ATTRDEFS_ABS_GEO_EXTENT3_D_H
