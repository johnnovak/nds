/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_ACCESSPOINTS_POI_GEO_LINE_ACCESS_TABLE_ROW_H
#define NDS_POI_ACCESSPOINTS_POI_GEO_LINE_ACCESS_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/FeatureLength.h"
#include "nds/common/PackedTileId.h"
#include "nds/common/RoadGeoLineId.h"
#include "nds/poi/main/PoiId.h"

namespace nds
{
namespace poi
{
namespace accesspoints
{

class PoiGeoLineAccessTableRow
{
public:
    PoiGeoLineAccessTableRow();
    ~PoiGeoLineAccessTableRow();

    nds::poi::main::PoiId getPoiId() const;
    void setPoiId(nds::poi::main::PoiId poiId);
    bool isNullPoiId() const;
    void setNullPoiId();

    nds::common::PackedTileId getTileId() const;
    void setTileId(nds::common::PackedTileId tileId);
    bool isNullTileId() const;
    void setNullTileId();

    nds::common::RoadGeoLineId getRoadGeoLineId() const;
    void setRoadGeoLineId(nds::common::RoadGeoLineId roadGeoLineId);
    bool isNullRoadGeoLineId() const;
    void setNullRoadGeoLineId();

    nds::common::FeatureLength getPosition() const;
    void setPosition(nds::common::FeatureLength position);
    bool isNullPosition() const;
    void setNullPosition();

    nds::common::FeatureLength getLength() const;
    void setLength(nds::common::FeatureLength length);
    bool isNullLength() const;
    void setNullLength();

    bool getPositiveDirection() const;
    void setPositiveDirection(bool positiveDirection);
    bool isNullPositiveDirection() const;
    void setNullPositiveDirection();

private:
    datascript::InPlaceOptionalHolder<nds::poi::main::PoiId> m_poiId;
    datascript::InPlaceOptionalHolder<nds::common::PackedTileId> m_tileId;
    datascript::InPlaceOptionalHolder<nds::common::RoadGeoLineId> m_roadGeoLineId;
    datascript::InPlaceOptionalHolder<nds::common::FeatureLength> m_position;
    datascript::InPlaceOptionalHolder<nds::common::FeatureLength> m_length;
    datascript::InPlaceOptionalHolder<bool> m_positiveDirection;
};

} // namespace accesspoints
} // namespace poi
} // namespace nds

#endif // NDS_POI_ACCESSPOINTS_POI_GEO_LINE_ACCESS_TABLE_ROW_H
