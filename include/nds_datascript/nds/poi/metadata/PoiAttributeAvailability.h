/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_METADATA_POI_ATTRIBUTE_AVAILABILITY_H
#define NDS_POI_METADATA_POI_ATTRIBUTE_AVAILABILITY_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/BitFieldArray.h>
#include <datascript/Types.h>

namespace nds
{
namespace poi
{
namespace metadata
{

class PoiAttributeAvailability
{
public:
    PoiAttributeAvailability();
    PoiAttributeAvailability(datascript::BitStreamReader& _in);

    uint16_t getNumAttributes() const;
    void setNumAttributes(uint16_t numAttributes);

    datascript::UInt16Array& getAttributeTypeIndexes();
    const datascript::UInt16Array& getAttributeTypeIndexes() const;
    void setAttributeTypeIndexes(const datascript::UInt16Array& attributeTypeIndexes);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const PoiAttributeAvailability& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint16_t m_numAttributes;
    datascript::UInt16Array m_attributeTypeIndexes;
};

} // namespace metadata
} // namespace poi
} // namespace nds

#endif // NDS_POI_METADATA_POI_ATTRIBUTE_AVAILABILITY_H
