/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_MAIN_POI_CATEGORY_TABLE_H
#define NDS_POI_MAIN_POI_CATEGORY_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/Types.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/poi/main/PoiCategoryTableRow.h"
#include "nds/icondata/main/IconSetId.h"
#include "nds/icondata/main/ScaleLevelId.h"
#include "nds/poi/main/CategoryId.h"
#include "nds/poi/main/PoiCatCollectionId.h"
#include "nds/poi/metadata/AttrBitMaskDefinition.h"
#include "nds/poi/predefined/StandardCat.h"

namespace nds
{
namespace poi
{
namespace main
{

class PoiCategoryTable
{
public:
    PoiCategoryTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~PoiCategoryTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<PoiCategoryTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<PoiCategoryTableRow>& rows) const;
    void write(const std::vector<PoiCategoryTableRow>& rows);
    void update(const PoiCategoryTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<PoiCategoryTableRow>& rows);
    static void writeRow(const PoiCategoryTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace main
} // namespace poi
} // namespace nds

#endif // NDS_POI_MAIN_POI_CATEGORY_TABLE_H
