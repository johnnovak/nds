/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_MAIN_CATEGORY_ID_H
#define NDS_POI_MAIN_CATEGORY_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace poi
{
namespace main
{

typedef uint16_t CategoryId;

} // namespace main
} // namespace poi
} // namespace nds

#endif // NDS_POI_MAIN_CATEGORY_ID_H
