/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_POI_REGIONS_REGION_ID_H
#define NDS_POI_REGIONS_REGION_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace poi
{
namespace regions
{

typedef int64_t RegionId;

} // namespace regions
} // namespace poi
} // namespace nds

#endif // NDS_POI_REGIONS_REGION_ID_H
