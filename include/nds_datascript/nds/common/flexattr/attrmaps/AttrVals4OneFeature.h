/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRMAPS_ATTR_VALS4_ONE_FEATURE_H
#define NDS_COMMON_FLEXATTR_ATTRMAPS_ATTR_VALS4_ONE_FEATURE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/flexattr/attrmaps/AttrValueList.h"
#include "nds/common/flexattr/attrmaps/AttributeTypeRef.h"
#include "nds/common/flexattr/attrmaps/FeatureReference.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrmaps
{

class AttrVals4OneFeature
{
public:
    AttrVals4OneFeature();
    AttrVals4OneFeature(datascript::BitStreamReader& _in,
        nds::common::flexattr::attrmaps::AttributeTypeRef& attrRefHeader);

    AttrVals4OneFeature(const AttrVals4OneFeature& _other);
    AttrVals4OneFeature& operator=(const AttrVals4OneFeature& _other);

    void initialize(
        nds::common::flexattr::attrmaps::AttributeTypeRef& attrRefHeader);
    void initializeChildren();

    nds::common::flexattr::attrmaps::AttributeTypeRef& getAttrRefHeader();
    const nds::common::flexattr::attrmaps::AttributeTypeRef& getAttrRefHeader() const;

    nds::common::flexattr::attrmaps::FeatureReference& getFeature();
    const nds::common::flexattr::attrmaps::FeatureReference& getFeature() const;
    void setFeature(const nds::common::flexattr::attrmaps::FeatureReference& feature);

    nds::common::flexattr::attrmaps::AttrValueList& getAttrValList();
    const nds::common::flexattr::attrmaps::AttrValueList& getAttrValList() const;
    void setAttrValList(const nds::common::flexattr::attrmaps::AttrValueList& attrValList);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const AttrVals4OneFeature& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::flexattr::attrmaps::AttributeTypeRef* m_attrRefHeader;
    bool m_isInitialized;
    datascript::InPlaceOptionalHolder<nds::common::flexattr::attrmaps::FeatureReference> m_feature;
    datascript::InPlaceOptionalHolder<nds::common::flexattr::attrmaps::AttrValueList> m_attrValList;
};

} // namespace attrmaps
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRMAPS_ATTR_VALS4_ONE_FEATURE_H
