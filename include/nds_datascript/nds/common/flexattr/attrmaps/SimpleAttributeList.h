/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRMAPS_SIMPLE_ATTRIBUTE_LIST_H
#define NDS_COMMON_FLEXATTR_ATTRMAPS_SIMPLE_ATTRIBUTE_LIST_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/ObjectArray.h>
#include <datascript/Types.h>

#include "nds/common/flexattr/attrmaps/Attribute.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrmaps
{

class SimpleAttributeList
{
public:
    SimpleAttributeList();
    SimpleAttributeList(datascript::BitStreamReader& _in);

    void initializeChildren();

    uint16_t getNumAttributes() const;
    void setNumAttributes(uint16_t numAttributes);

    datascript::ObjectArray<nds::common::flexattr::attrmaps::Attribute>& getAttributes();
    const datascript::ObjectArray<nds::common::flexattr::attrmaps::Attribute>& getAttributes() const;
    void setAttributes(const datascript::ObjectArray<nds::common::flexattr::attrmaps::Attribute>& attributes);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const SimpleAttributeList& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint16_t m_numAttributes;
    datascript::ObjectArray<nds::common::flexattr::attrmaps::Attribute> m_attributes;
};

} // namespace attrmaps
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRMAPS_SIMPLE_ATTRIBUTE_LIST_H
