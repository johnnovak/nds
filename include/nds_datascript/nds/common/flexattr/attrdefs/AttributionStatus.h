/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_ATTRIBUTION_STATUS_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_ATTRIBUTION_STATUS_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class AttributionStatus
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_AttributionStatus
    {
        NOT_FIELD_SURVEYED = UINT8_C(0),
        PARTLY_ATTRIBUTED = UINT8_C(1),
        FULLY_ATTRIBUTED = UINT8_C(2)
    };

    AttributionStatus();
    AttributionStatus(e_AttributionStatus value);
    explicit AttributionStatus(datascript::BitStreamReader& _in);

    operator e_AttributionStatus() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const AttributionStatus& other) const;
    bool operator==(e_AttributionStatus other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static AttributionStatus toEnum(uint8_t rawValue);

private:
    e_AttributionStatus m_value;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_ATTRIBUTION_STATUS_H
