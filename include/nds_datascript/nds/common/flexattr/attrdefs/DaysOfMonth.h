/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_DAYS_OF_MONTH_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_DAYS_OF_MONTH_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class DaysOfMonth
{
public:
    DaysOfMonth();
    DaysOfMonth(datascript::BitStreamReader& _in);

    bool getIsDay1() const;
    void setIsDay1(bool isDay1);

    bool getIsDay2() const;
    void setIsDay2(bool isDay2);

    bool getIsDay3() const;
    void setIsDay3(bool isDay3);

    bool getIsDay4() const;
    void setIsDay4(bool isDay4);

    bool getIsDay5() const;
    void setIsDay5(bool isDay5);

    bool getIsDay6() const;
    void setIsDay6(bool isDay6);

    bool getIsDay7() const;
    void setIsDay7(bool isDay7);

    bool getIsDay8() const;
    void setIsDay8(bool isDay8);

    bool getIsDay9() const;
    void setIsDay9(bool isDay9);

    bool getIsDay10() const;
    void setIsDay10(bool isDay10);

    bool getIsDay11() const;
    void setIsDay11(bool isDay11);

    bool getIsDay12() const;
    void setIsDay12(bool isDay12);

    bool getIsDay13() const;
    void setIsDay13(bool isDay13);

    bool getIsDay14() const;
    void setIsDay14(bool isDay14);

    bool getIsDay15() const;
    void setIsDay15(bool isDay15);

    bool getIsDay16() const;
    void setIsDay16(bool isDay16);

    bool getIsDay17() const;
    void setIsDay17(bool isDay17);

    bool getIsDay18() const;
    void setIsDay18(bool isDay18);

    bool getIsDay19() const;
    void setIsDay19(bool isDay19);

    bool getIsDay20() const;
    void setIsDay20(bool isDay20);

    bool getIsDay21() const;
    void setIsDay21(bool isDay21);

    bool getIsDay22() const;
    void setIsDay22(bool isDay22);

    bool getIsDay23() const;
    void setIsDay23(bool isDay23);

    bool getIsDay24() const;
    void setIsDay24(bool isDay24);

    bool getIsDay25() const;
    void setIsDay25(bool isDay25);

    bool getIsDay26() const;
    void setIsDay26(bool isDay26);

    bool getIsDay27() const;
    void setIsDay27(bool isDay27);

    bool getIsDay28() const;
    void setIsDay28(bool isDay28);

    bool getIsDay29() const;
    void setIsDay29(bool isDay29);

    bool getIsDay30() const;
    void setIsDay30(bool isDay30);

    bool getIsDay31() const;
    void setIsDay31(bool isDay31);

    bool getIsInclusive() const;
    void setIsInclusive(bool isInclusive);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const DaysOfMonth& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_isDay1;
    bool m_isDay2;
    bool m_isDay3;
    bool m_isDay4;
    bool m_isDay5;
    bool m_isDay6;
    bool m_isDay7;
    bool m_isDay8;
    bool m_isDay9;
    bool m_isDay10;
    bool m_isDay11;
    bool m_isDay12;
    bool m_isDay13;
    bool m_isDay14;
    bool m_isDay15;
    bool m_isDay16;
    bool m_isDay17;
    bool m_isDay18;
    bool m_isDay19;
    bool m_isDay20;
    bool m_isDay21;
    bool m_isDay22;
    bool m_isDay23;
    bool m_isDay24;
    bool m_isDay25;
    bool m_isDay26;
    bool m_isDay27;
    bool m_isDay28;
    bool m_isDay29;
    bool m_isDay30;
    bool m_isDay31;
    bool m_isInclusive;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_DAYS_OF_MONTH_H
