/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_DAY_OF_YEAR_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_DAY_OF_YEAR_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

#include "nds/common/MonthOfYear.h"
#include "nds/common/YearValue.h"
#include "nds/common/flexattr/attrdefs/DayOfMonth.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class DayOfYear
{
public:
    DayOfYear();
    DayOfYear(datascript::BitStreamReader& _in);

    nds::common::YearValue getYear() const;
    void setYear(nds::common::YearValue year);

    nds::common::MonthOfYear getMonth() const;
    void setMonth(nds::common::MonthOfYear month);

    nds::common::flexattr::attrdefs::DayOfMonth getDay() const;
    void setDay(nds::common::flexattr::attrdefs::DayOfMonth day);

    bool getIsInclusive() const;
    void setIsInclusive(bool isInclusive);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const DayOfYear& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    nds::common::YearValue m_year;
    nds::common::MonthOfYear m_month;
    nds::common::flexattr::attrdefs::DayOfMonth m_day;
    bool m_isInclusive;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_DAY_OF_YEAR_H
