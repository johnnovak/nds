/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_EXIT_LANE_MASK_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_EXIT_LANE_MASK_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class ExitLaneMask
{
public:
    ExitLaneMask();
    ExitLaneMask(datascript::BitStreamReader& _in);

    bool getIsRightMostExit() const;
    void setIsRightMostExit(bool isRightMostExit);

    bool getIsSecondExitRight() const;
    void setIsSecondExitRight(bool isSecondExitRight);

    bool getIsThirdExitRight() const;
    void setIsThirdExitRight(bool isThirdExitRight);

    bool getIsFourthExitRight() const;
    void setIsFourthExitRight(bool isFourthExitRight);

    bool getIsFifthExitRight() const;
    void setIsFifthExitRight(bool isFifthExitRight);

    bool getIsSixthExitRight() const;
    void setIsSixthExitRight(bool isSixthExitRight);

    bool getIsSeventhExitRight() const;
    void setIsSeventhExitRight(bool isSeventhExitRight);

    bool getIsSeventhExitLeft() const;
    void setIsSeventhExitLeft(bool isSeventhExitLeft);

    bool getIsSixthExitLeft() const;
    void setIsSixthExitLeft(bool isSixthExitLeft);

    bool getIsFifthExitLeft() const;
    void setIsFifthExitLeft(bool isFifthExitLeft);

    bool getIsFourthExitLeft() const;
    void setIsFourthExitLeft(bool isFourthExitLeft);

    bool getIsThirdExitLeft() const;
    void setIsThirdExitLeft(bool isThirdExitLeft);

    bool getIsSecondExitLeft() const;
    void setIsSecondExitLeft(bool isSecondExitLeft);

    bool getIsLeftMostExit() const;
    void setIsLeftMostExit(bool isLeftMostExit);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const ExitLaneMask& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_isRightMostExit;
    bool m_isSecondExitRight;
    bool m_isThirdExitRight;
    bool m_isFourthExitRight;
    bool m_isFifthExitRight;
    bool m_isSixthExitRight;
    bool m_isSeventhExitRight;
    bool m_isSeventhExitLeft;
    bool m_isSixthExitLeft;
    bool m_isFifthExitLeft;
    bool m_isFourthExitLeft;
    bool m_isThirdExitLeft;
    bool m_isSecondExitLeft;
    bool m_isLeftMostExit;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_EXIT_LANE_MASK_H
