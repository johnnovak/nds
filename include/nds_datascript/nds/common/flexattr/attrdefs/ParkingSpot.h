/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_PARKING_SPOT_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_PARKING_SPOT_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/flexattr/attrdefs/PrecisePolygon.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class ParkingSpot
{
public:
    ParkingSpot();
    ParkingSpot(datascript::BitStreamReader& _in);

    void initializeChildren();

    nds::common::flexattr::attrdefs::PrecisePolygon& getGeometry();
    const nds::common::flexattr::attrdefs::PrecisePolygon& getGeometry() const;
    void setGeometry(const nds::common::flexattr::attrdefs::PrecisePolygon& geometry);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const ParkingSpot& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    datascript::InPlaceOptionalHolder<nds::common::flexattr::attrdefs::PrecisePolygon> m_geometry;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_PARKING_SPOT_H
