/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_FLEXATTR_ATTRDEFS_PARKING_SPOTS_H
#define NDS_COMMON_FLEXATTR_ATTRDEFS_PARKING_SPOTS_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/ObjectArray.h>
#include <datascript/Types.h>

#include "nds/common/flexattr/attrdefs/ParkingSpot.h"

namespace nds
{
namespace common
{
namespace flexattr
{
namespace attrdefs
{

class ParkingSpots
{
public:
    ParkingSpots();
    ParkingSpots(datascript::BitStreamReader& _in);

    void initializeChildren();

    uint16_t getNumSpots() const;
    void setNumSpots(uint16_t numSpots);

    datascript::ObjectArray<nds::common::flexattr::attrdefs::ParkingSpot>& getSpots();
    const datascript::ObjectArray<nds::common::flexattr::attrdefs::ParkingSpot>& getSpots() const;
    void setSpots(const datascript::ObjectArray<nds::common::flexattr::attrdefs::ParkingSpot>& spots);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const ParkingSpots& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    uint16_t m_numSpots;
    datascript::ObjectArray<nds::common::flexattr::attrdefs::ParkingSpot> m_spots;
};

} // namespace attrdefs
} // namespace flexattr
} // namespace common
} // namespace nds

#endif // NDS_COMMON_FLEXATTR_ATTRDEFS_PARKING_SPOTS_H
