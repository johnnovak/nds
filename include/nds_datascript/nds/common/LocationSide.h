/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_LOCATION_SIDE_H
#define NDS_COMMON_LOCATION_SIDE_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace common
{

class LocationSide
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_LocationSide
    {
        SIDE_INDEPENDENT = UINT8_C(0),
        LEFT_SIDE = UINT8_C(1),
        RIGHT_SIDE = UINT8_C(2)
    };

    LocationSide();
    LocationSide(e_LocationSide value);
    explicit LocationSide(datascript::BitStreamReader& _in);

    operator e_LocationSide() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const LocationSide& other) const;
    bool operator==(e_LocationSide other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static LocationSide toEnum(uint8_t rawValue);

private:
    e_LocationSide m_value;
};

} // namespace common
} // namespace nds

#endif // NDS_COMMON_LOCATION_SIDE_H
