/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_SEASONAL_USAGE_TYPE_H
#define NDS_COMMON_SEASONAL_USAGE_TYPE_H

#include <string>
#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/HashCodeUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/Types.h>

namespace nds
{
namespace common
{

class SeasonalUsageType
{
public:
    /**
     * The type this enum is based on.
     *
     * This is the C++ mapping of the original datascript type. It can be
     * wider than the original type. E.g. "enum bit:3" would have uint8_t
     * as its base type.
     */
    typedef uint8_t _base_type;

    enum e_SeasonalUsageType
    {
        INDEPENDENT = UINT8_C(0),
        WINTER = UINT8_C(1),
        SPRING = UINT8_C(2),
        SUMMER = UINT8_C(3),
        FALL = UINT8_C(4)
    };

    SeasonalUsageType();
    SeasonalUsageType(e_SeasonalUsageType value);
    explicit SeasonalUsageType(datascript::BitStreamReader& _in);

    operator e_SeasonalUsageType() const;
    uint8_t getValue() const;

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition = 0) const;

    bool operator==(const SeasonalUsageType& other) const;
    bool operator==(e_SeasonalUsageType other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
        datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS) const;

    const char* toString() const;

    static SeasonalUsageType toEnum(uint8_t rawValue);

private:
    e_SeasonalUsageType m_value;
};

} // namespace common
} // namespace nds

#endif // NDS_COMMON_SEASONAL_USAGE_TYPE_H
