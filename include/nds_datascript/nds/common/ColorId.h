/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_COLOR_ID_H
#define NDS_COMMON_COLOR_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace common
{

typedef uint32_t ColorId;

} // namespace common
} // namespace nds

#endif // NDS_COMMON_COLOR_ID_H
