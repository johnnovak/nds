/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_LOCATION_REFERENCE_H
#define NDS_COMMON_LOCATION_REFERENCE_H

#include <datascript/BitStreamReader.h>
#include <datascript/BitStreamWriter.h>
#include <datascript/BitFieldUtil.h>
#include <datascript/CppRuntimeException.h>
#include <datascript/StringConvertUtil.h>
#include <datascript/PreWriteAction.h>
#include <datascript/OptionalHolder.h>

#include "nds/common/LocationReferenceChoice.h"
#include "nds/common/LocationReferenceType.h"
#include "nds/common/LocationSide.h"
#include "nds/common/PackedTileId.h"

namespace nds
{
namespace common
{

class LocationReference
{
public:
    LocationReference();
    LocationReference(datascript::BitStreamReader& _in);

    LocationReference(const LocationReference& _other);
    LocationReference& operator=(const LocationReference& _other);

    void initializeChildren();

    nds::common::LocationReferenceType getType() const;
    void setType(nds::common::LocationReferenceType type);

    nds::common::LocationSide getLocationSide() const;
    void setLocationSide(nds::common::LocationSide locationSide);

    bool getReferringToExternalFeature() const;
    void setReferringToExternalFeature(bool referringToExternalFeature);

    nds::common::PackedTileId getTileId() const;
    void setTileId(nds::common::PackedTileId tileId);
    bool hasTileId() const;

    nds::common::LocationReferenceChoice& getLocRef();
    const nds::common::LocationReferenceChoice& getLocRef() const;
    void setLocRef(const nds::common::LocationReferenceChoice& locRef);

    size_t bitsizeOf(size_t _bitPosition = 0) const;
    size_t setLabels(size_t _bitPosition);

    bool operator==(const LocationReference& other) const;
    int hashCode() const;

    void read(datascript::BitStreamReader& _in);
    void write(datascript::BitStreamWriter& _out,
               datascript::PreWriteAction _preWriteAction = datascript::ALL_PRE_WRITE_ACTIONS);

private:
    bool m_areChildrenInitialized;
    nds::common::LocationReferenceType m_type;
    nds::common::LocationSide m_locationSide;
    bool m_referringToExternalFeature;
    datascript::InPlaceOptionalHolder<nds::common::PackedTileId> m_tileId;
    datascript::InPlaceOptionalHolder<nds::common::LocationReferenceChoice> m_locRef;
};

} // namespace common
} // namespace nds

#endif // NDS_COMMON_LOCATION_REFERENCE_H
