/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_COMMON_DAY_OF_WEEK_H
#define NDS_COMMON_DAY_OF_WEEK_H

#include <datascript/Types.h>

namespace nds
{
namespace common
{

typedef uint8_t DayOfWeek;

} // namespace common
} // namespace nds

#endif // NDS_COMMON_DAY_OF_WEEK_H
