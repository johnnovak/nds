/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PHONETICTRANSCRIPTION_TI_PHONETIC_TRA_TABLE_H
#define NDS_SPEECH_PHONETICTRANSCRIPTION_TI_PHONETIC_TRA_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <string>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/speech/phonetictranscription/TiPhoneticTraTableRow.h"
#include "nds/speech/phonetictranscription/PhoneticTranscriptionId.h"

namespace nds
{
namespace speech
{
namespace phonetictranscription
{

class TiPhoneticTraTable
{
public:
    TiPhoneticTraTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~TiPhoneticTraTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<TiPhoneticTraTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<TiPhoneticTraTableRow>& rows) const;
    void write(const std::vector<TiPhoneticTraTableRow>& rows);
    void update(const TiPhoneticTraTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<TiPhoneticTraTableRow>& rows);
    static void writeRow(const TiPhoneticTraTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace phonetictranscription
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PHONETICTRANSCRIPTION_TI_PHONETIC_TRA_TABLE_H
