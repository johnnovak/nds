/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PHONETICTRANSCRIPTION_NG_NAME_STR_COLL_TO_PHONETIC_TABLE_ROW_H
#define NDS_SPEECH_PHONETICTRANSCRIPTION_NG_NAME_STR_COLL_TO_PHONETIC_TABLE_ROW_H

#include <datascript/OptionalHolder.h>

#include "nds/common/LanguageCode.h"
#include "nds/ng/ngname/NameStringCollectionId.h"
#include "nds/speech/phonetictranscription/PhoneticTranscriptionId.h"

namespace nds
{
namespace speech
{
namespace phonetictranscription
{

class NgNameStrCollToPhoneticTableRow
{
public:
    NgNameStrCollToPhoneticTableRow();
    ~NgNameStrCollToPhoneticTableRow();

    nds::ng::ngname::NameStringCollectionId getNameStringCollectionId() const;
    void setNameStringCollectionId(nds::ng::ngname::NameStringCollectionId nameStringCollectionId);
    bool isNullNameStringCollectionId() const;
    void setNullNameStringCollectionId();

    nds::common::LanguageCode getLanguageCode() const;
    void setLanguageCode(nds::common::LanguageCode languageCode);
    bool isNullLanguageCode() const;
    void setNullLanguageCode();

    nds::speech::phonetictranscription::PhoneticTranscriptionId getPhoneticTranscriptionId() const;
    void setPhoneticTranscriptionId(nds::speech::phonetictranscription::PhoneticTranscriptionId phoneticTranscriptionId);
    bool isNullPhoneticTranscriptionId() const;
    void setNullPhoneticTranscriptionId();

private:
    datascript::InPlaceOptionalHolder<nds::ng::ngname::NameStringCollectionId> m_nameStringCollectionId;
    datascript::InPlaceOptionalHolder<nds::common::LanguageCode> m_languageCode;
    datascript::InPlaceOptionalHolder<nds::speech::phonetictranscription::PhoneticTranscriptionId> m_phoneticTranscriptionId;
};

} // namespace phonetictranscription
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PHONETICTRANSCRIPTION_NG_NAME_STR_COLL_TO_PHONETIC_TABLE_ROW_H
