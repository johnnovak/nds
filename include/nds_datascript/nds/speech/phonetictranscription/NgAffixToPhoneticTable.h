/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PHONETICTRANSCRIPTION_NG_AFFIX_TO_PHONETIC_TABLE_H
#define NDS_SPEECH_PHONETICTRANSCRIPTION_NG_AFFIX_TO_PHONETIC_TABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/speech/phonetictranscription/NgAffixToPhoneticTableRow.h"
#include "nds/common/LanguageCode.h"
#include "nds/ng/main/NgAffixId.h"
#include "nds/speech/phonetictranscription/PhoneticTranscriptionId.h"

namespace nds
{
namespace speech
{
namespace phonetictranscription
{

class NgAffixToPhoneticTable
{
public:
    NgAffixToPhoneticTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~NgAffixToPhoneticTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<NgAffixToPhoneticTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<NgAffixToPhoneticTableRow>& rows) const;
    void write(const std::vector<NgAffixToPhoneticTableRow>& rows);
    void update(const NgAffixToPhoneticTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<NgAffixToPhoneticTableRow>& rows);
    static void writeRow(const NgAffixToPhoneticTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace phonetictranscription
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PHONETICTRANSCRIPTION_NG_AFFIX_TO_PHONETIC_TABLE_H
