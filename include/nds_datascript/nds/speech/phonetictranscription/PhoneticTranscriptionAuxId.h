/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PHONETICTRANSCRIPTION_PHONETIC_TRANSCRIPTION_AUX_ID_H
#define NDS_SPEECH_PHONETICTRANSCRIPTION_PHONETIC_TRANSCRIPTION_AUX_ID_H

#include <datascript/Types.h>

namespace nds
{
namespace speech
{
namespace phonetictranscription
{

typedef uint16_t PhoneticTranscriptionAuxId;

} // namespace phonetictranscription
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PHONETICTRANSCRIPTION_PHONETIC_TRANSCRIPTION_AUX_ID_H
