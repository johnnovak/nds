/**
 * Automatically generated by RDS Tool version unknown.
 *
 * @author Navigation Data Standard e.V.
 */

#ifndef NDS_SPEECH_PRERECORDEDVOICE_NAMED_OBJECT_PRERECORDED_VTABLE_H
#define NDS_SPEECH_PRERECORDEDVOICE_NAMED_OBJECT_PRERECORDED_VTABLE_H

#include <vector>
#include <string>
#include <nds_sqlite3.h>
#include <datascript/SqlDatabase.h>

#include "nds/all/IParameterListener.h"
#include "nds/speech/prerecordedvoice/NamedObjectPrerecordedVTableRow.h"
#include "nds/common/BlobData.h"
#include "nds/common/VersionId.h"
#include "nds/speech/common/PrerecordedVoiceId.h"

namespace nds
{
namespace speech
{
namespace prerecordedvoice
{

class NamedObjectPrerecordedVTable
{
public:
    NamedObjectPrerecordedVTable(datascript::SqlDatabase& db, const std::string& tableName);
    ~NamedObjectPrerecordedVTable();

    void createTable();
    void deleteTable();
    void createIndices();
    void deleteIndices();

    void read(nds::all::IParameterListener& parameterListener, std::vector<NamedObjectPrerecordedVTableRow>& rows) const;
    void read(nds::all::IParameterListener& parameterListener, const std::string& condition,
            std::vector<NamedObjectPrerecordedVTableRow>& rows) const;
    void write(const std::vector<NamedObjectPrerecordedVTableRow>& rows);
    void update(const NamedObjectPrerecordedVTableRow& row, const std::string& whereCondition);

private:
    static void readRow(nds::all::IParameterListener& parameterListener,
            sqlite3_stmt& statement, std::vector<NamedObjectPrerecordedVTableRow>& rows);
    static void writeRow(const NamedObjectPrerecordedVTableRow& row, sqlite3_stmt& statement);

    datascript::SqlDatabase& m_db;
    const std::string m_name;
};

} // namespace prerecordedvoice
} // namespace speech
} // namespace nds

#endif // NDS_SPEECH_PRERECORDEDVOICE_NAMED_OBJECT_PRERECORDED_VTABLE_H
