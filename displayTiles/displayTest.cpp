#include <opencv2/opencv.hpp>

#include <cstdlib>
#include <iomanip>
#include <vector>
#include <cmath>

class Tile
{
  public:
    int zoom;
    int tileX;
    int tileY;
    int size;
    int factor;
    double initialResolution;
    double originShift;

    Tile( int zoom, int x, int y )
    {
      this->zoom = zoom;
      this->tileX = x;
      this->tileY = y;
      this->size = 256;
      this->factor = 1000;
      this->initialResolution = 2.0 * M_PI * 6378137.0 / (double) this->size;
      // 156543.03392804062 for tileSize 256 pixels
      this->originShift = 2.0 * M_PI * 6378137.0 / 2.0;
      // 20037508.342789244
    }

    int getSize( void )
    {
      return this->size;
    }

    int getZoom( void )
    {
      return this->zoom;
    }

    int overrideZoom( int newZoom )
    {
      if( newZoom >= 0 )
        return newZoom;

      return this->zoom;
    }

    int getX( void )
    {
      return this->tileX;
    }

    int getY( void )
    {
      return this->tileY;
    }

    int getXHigh( void )
    {
      return getX() / this->factor;
    }

    int getXLow( void )
    {
      return getX() - getXHigh() * this->factor;
    }

    int getYHigh( void )
    {
      return getY() / this->factor;
    }

    int getYLow( void )
    {
      return getY() - getYHigh() * this->factor;
    }

    // Converts given lat/lon in WGS84 Datum to XY in Spherical Mercator EPSG:900913"
    void LatLonToMeters( double lat, double lon, double &mx, double &my )
    {
      mx = lon * this->originShift / 180.0;
      my = log( tan((90.0 + lat) * M_PI / 360.0 )) / (M_PI / 180.0);

      my = my * this->originShift / 180.0;
    }

    // Converts XY point from Spherical Mercator EPSG:900913 to lat/lon in WGS84 Datum"
    void MetersToLatLon( double mx, double my, double &lat, double &lon )
    {
      lon = (mx / this->originShift) * 180.0;
      lat = (my / this->originShift) * 180.0;

      lat = 180.0 / M_PI * (2.0 * atan( exp( lat * M_PI / 180.0)) - M_PI / 2.0);
    }
		
    double Resolution( int zoom )
    {
      return this->initialResolution / (pow(2,zoom));
    }

    void PixelsToMeters( int px, int py, double &mx, double &my, int zoom = -1 )
    {
      zoom = this->overrideZoom( zoom );

      double res = this->Resolution( zoom );
      mx = (double) px * res - this->originShift;
      my = (double) py * res - this->originShift;
    }
		
    void MetersToPixels( double mx, double my, int &px, int  &py, int zoom = -1 )
    {
      zoom = this->overrideZoom( zoom );

      double res = this->Resolution( zoom );
      px = (int) ceil( (mx + this->originShift) / res );
      py = (int) ceil( (my + this->originShift) / res );
    }

    void PixelsToTile( int px, int py, int &tx, int &ty )
    {
      tx = int( ceil( px / (double) this->size ) - 1.0 );
      ty = int( ceil( py / (double) this->size ) - 1.0 );
    }

    void PixelsToRaster( int px, int py, int &xout, int &yout, int zoom = -1 )
    {
      zoom = this->overrideZoom( zoom );

      int mapSize = this->size << zoom;
      xout = px;
      yout = mapSize - py;
    }

    //  Returns tile for given mercator coordinates
    void MetersToTile( double mx, double my, int &tx, int &ty, int zoom = -1)
    {
      zoom = this->overrideZoom( zoom );

      int px, py;

      MetersToPixels( mx, my, px, py, zoom );
      PixelsToTile( px, py, tx, ty );
    }
		
    // Returns bounds of the given tile in EPSG:900913 coordinates"
    void TileBounds( int tx, int ty, double &minx, double &miny, double &maxx, double &maxy, int zoom = -1 )
    {
      zoom = this->overrideZoom( zoom );
	
      PixelsToMeters( tx * this->size, ty * this->size, minx, miny, zoom );
      PixelsToMeters( (tx+1) * this->size, (ty+1) * this->size, maxx, maxy, zoom );
    }

    // Returns bounds of the given tile in latutude/longitude using WGS84 datum"
    void TileLatLonBounds( int tx, int ty, double &minLat, double &minLon, double &maxLat, double &maxLon, int zoom = -1 )
    {
      zoom = this->overrideZoom( zoom );

      double minx, miny, maxx, maxy;
      TileBounds( tx, ty, minx, miny, maxx, maxy, zoom );
      MetersToLatLon( minx, miny, minLat, minLon );
      MetersToLatLon( maxx, maxy, maxLat, maxLon );
    }

    // Converts TMS tile coordinates to Google Tile coordinates"
    void GoogleTile( int tx, int ty, int &gx, int &gy, int zoom = -1 )
    {
        zoom = this->overrideZoom( zoom );
        
        gx = tx;
        gy = (int) ( pow(2.0, zoom) - 1.0 - (double) ty);
    }

    // Converts TMS tile coordinates to Google Tile coordinates"
    void GoogleTileToTMS( int gx, int gy, int &tx, int &ty, int zoom = -1 )
    {
        zoom = this->overrideZoom( zoom );
        
        tx = gx;
        ty = gy + pow(2, zoom);
    }
};

std::string strCacheRoot( "/home/robond/tiletest" );
std::string strLayerName( "osm" );

void renderMap( int imgWidth, int imgHeight, cv::Mat &DispImage, int minX, int maxX, int minY, int maxY, int zoom, double centerX, double centerY )
{
  int xOffset = 0;
  int yOffset = 0;
  int xTiles = abs(minX - maxX) + 1, yTiles = abs(minY - maxY) + 1;

  cv::Mat FullImage = cv::Mat::zeros( cv::Size( xTiles * 256, yTiles * 256 ), CV_8UC3 );

  for( int iYTile = maxY; iYTile >= minY; iYTile-- )
  {
    xOffset = 0;

    for( int iXTile = minX; iXTile <= maxX; iXTile++ )
    {
      std::string fullPath;

      int gx, gy;
      Tile aTile( zoom, iXTile, iYTile );
      aTile.GoogleTile( iXTile, iYTile, gx, gy, zoom );

      Tile theTile( zoom, gx, gy );
      int tileSize = theTile.getSize();

      std::ostringstream tileNameStream;
      tileNameStream << "/" << strLayerName << "/" << theTile.getZoom() << "/" << std::setfill('0') << std::setw(3) << theTile.getXHigh();
      tileNameStream << "/" << std::setfill('0') << std::setw(3) << theTile.getXLow();
      tileNameStream << "/" << std::setfill('0') << std::setw(3) << theTile.getYHigh(); 
      tileNameStream << "/" << std::setfill('0') << std::setw(3) << theTile.getYLow() << ".png";

      fullPath = strCacheRoot + tileNameStream.str();
      cv::Mat mapTile = cv::imread( fullPath );

      if( mapTile.data == nullptr )
      { 
        std::cout << "Tile " << iXTile << " , " << iYTile << " failed to load" << std::endl;
        continue;
      }

      try
      {
        // Set the image ROI to display the current image
        // Resize the input image and copy the it to the Single Big Image
        cv::Rect ROI( xOffset, yOffset, tileSize, tileSize );
        mapTile.copyTo(FullImage(ROI));
      }

      catch( ... )
      {
      }

      xOffset += tileSize;
    }

    yOffset += 256;
  }

//
//  Compute ROI in large image to blit to display image
//
//  Get ul mx, my for large image
//  Subtract from center mx, my to get offset to center from ul
//  Get pixel coords of center point for small image relative to large image
//  Create ROi and blit
//
  Tile aTile( zoom, minX, minY );

  double metersPerPixel = aTile.Resolution( zoom );
  double minx, miny, maxx, maxy;
  
  aTile.TileBounds( minX, maxY, minx, miny, maxx, maxy, zoom );

  int centerpixelx = (centerX - minx) / metersPerPixel;
  int centerpixely = (-(centerY - maxy)) / metersPerPixel;

  cv::Rect ROISrc( centerpixelx - (imgWidth / 2), centerpixely - (imgHeight / 2), imgWidth, imgHeight );
  FullImage(ROISrc).copyTo(DispImage);
}

cv::Mat createMapLayer( double centerLat, double centerLon, int zoom, int imgHeight, int imgWidth )
{
  int centerTilex, centerTiley;
  double mx, my;
  int tileX, tileY, xout, yout;
  int centerXInPixels, centerYInPixels;
  int  ulXInPixels, ulYInPixels;
 
  Tile aTile( zoom, 1, 1 );
  aTile.LatLonToMeters( centerLat, centerLon, mx, my );
  std::cout << "initial SM coords " << std::setprecision( 15 ) << mx << ", " << my << std::endl;

  aTile.MetersToPixels( mx, my, centerXInPixels, centerYInPixels, zoom );
  aTile.PixelsToRaster( centerXInPixels, centerYInPixels, xout, yout, zoom );
  aTile.MetersToTile( mx, my, centerTilex, centerTiley, zoom );

  std::cout << "Center tile " << centerTilex << ", " << centerTiley << std::endl;
//
//  Get coordinates of window extent
//
//  window center in m +/- (window height, width / 2 )
//
  double metersPerPixel = aTile.Resolution( zoom );
  double llx, lly, urx, ury;

  llx = mx - metersPerPixel * (double) ( imgWidth / 2 );
  lly = my + metersPerPixel * (double) ( imgHeight / 2 );

  urx = mx + metersPerPixel * (double) ( imgWidth / 2 );
  ury = my - metersPerPixel * (double) ( imgHeight / 2 );
//
//  Convert window extents to tiles, subtract and add 1 tile coord and then render ...
//
  int lltx, llty, urtx, urty;

  aTile.MetersToTile( llx, lly, lltx, llty, zoom);
  aTile.MetersToTile( urx, ury, urtx, urty, zoom);

  lltx -= 1;
  llty += 1;
  urtx += 1;
  urty -= 1;

  std::cout << "ll tile " << lltx << ", " << llty << std::endl;
  std::cout << "ur tile " << urtx << ", " << urty << std::endl;

  // Create a new 3 channel image
  cv::Mat DispImage = cv::Mat::zeros( cv::Size( imgWidth, imgHeight ), CV_8UC3 );
  renderMap( imgWidth, imgHeight, DispImage, lltx, urtx, urty, llty, zoom, mx, my );

  return DispImage;
}

int main( int argc, char **argv )
{
  double centerLat, centerLon;
  int zoom, imageHeight, imageWidth;

  if( argc > 5 )
  {
    centerLat = atof( argv[ 1 ] ); 
    centerLon = atof( argv[ 2 ] ); 
    zoom = atoi( argv[ 3 ] );
    imageHeight = atoi( argv[ 4 ] );
    imageWidth = atoi( argv[ 5 ] );
  }
  else
  {
    std::cout << "maplayer centerLat centerLon zoom imageHeight imageWidth" << std::endl;
    exit( -1 );
  }

  //
  //  Rotation for yaw can be handled by caller
  //
  // rows,cols = img.shape
  // M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1)
  // dst = cv2.warpAffine(img,M,(cols,rows))
  //
  cv::Mat DispImage = createMapLayer( centerLat, centerLon, zoom, imageHeight, imageWidth );
  
  // Create a new window, and show the Single Big Image
  cv::namedWindow( "Mappiness Happiness", 1 );
  cv::imshow( "Mappiness Happiness", DispImage);
  cv::waitKey();
}

