#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "harman::hadlib" for configuration "Release"
set_property(TARGET harman::hadlib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(harman::hadlib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "Boost::filesystem;harman::parser;harman::database"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libhadlib.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS harman::hadlib )
list(APPEND _IMPORT_CHECK_FILES_FOR_harman::hadlib "${_IMPORT_PREFIX}/lib/libhadlib.a" )

# Import target "harman::sqlite" for configuration "Release"
set_property(TARGET harman::sqlite APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(harman::sqlite PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "-ldl;-lpthread;-lm"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libsqlite.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS harman::sqlite )
list(APPEND _IMPORT_CHECK_FILES_FOR_harman::sqlite "${_IMPORT_PREFIX}/lib/libsqlite.a" )

# Import target "harman::sal" for configuration "Release"
set_property(TARGET harman::sal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(harman::sal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "harman::sqlite"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libsal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS harman::sal )
list(APPEND _IMPORT_CHECK_FILES_FOR_harman::sal "${_IMPORT_PREFIX}/lib/libsal.a" )

# Import target "harman::database" for configuration "Release"
set_property(TARGET harman::database APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(harman::database PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "harman::sal;/usr/lib/x86_64-linux-gnu/libboost_filesystem.a;/usr/lib/x86_64-linux-gnu/libboost_system.a"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libdatabase.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS harman::database )
list(APPEND _IMPORT_CHECK_FILES_FOR_harman::database "${_IMPORT_PREFIX}/lib/libdatabase.a" )

# Import target "harman::parser" for configuration "Release"
set_property(TARGET harman::parser APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(harman::parser PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "harman::nds_datascript"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libparser.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS harman::parser )
list(APPEND _IMPORT_CHECK_FILES_FOR_harman::parser "${_IMPORT_PREFIX}/lib/libparser.a" )

# Import target "harman::nds_datascript" for configuration "Release"
set_property(TARGET harman::nds_datascript APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(harman::nds_datascript PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "harman::sqlite"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libnds_datascript.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS harman::nds_datascript )
list(APPEND _IMPORT_CHECK_FILES_FOR_harman::nds_datascript "${_IMPORT_PREFIX}/lib/libnds_datascript.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
