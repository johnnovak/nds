#include <iostream>
#include <cstdint>
#include <iomanip>

#include <math.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "ndsreader.h"

extern vecLineStrings vecGeometry;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  if( argc < 2 )
  {
    std::cout << "path to db required on command line" << std::endl;
    exit( 1 );
  }

  std::string DBPath( argv[ 1 ] );

  for( long tileID = 611096870; tileID < 611096880; tileID++ )
//  for( long tileID = 611096874; tileID < 611096875; tileID++ )
  {
    setupDB( DBPath, tileID );
  }

  double originX = -13577500.1926;
  double originY = 4498889.11582;
  double panStep = 25.0;
  double scale = 2.0;
  bool bDone = false;

//
//  Convert to OpenCV types and render loaded tiles
//
// Create black empty image
  while( ! bDone )
  {
    cv::Mat image = cv::Mat::zeros( 1200, 1200, CV_8UC3 );

    typedef boost::geometry::model::linestring<point_t>::iterator linestringIter;

    // Draw a line
    for( vecLineStrings::iterator iterLines = vecGeometry.begin(); iterLines != vecGeometry.end(); iterLines++ )
    {
      linestring_t *theLineString = *(iterLines);
      std::vector< cv::Point > contour;

      for( linestringIter lsIter = theLineString->begin(); lsIter != theLineString->end(); lsIter++  )
      {
        point_t thePoint = *lsIter;
        contour.push_back(cv::Point( (boost::geometry::get<0>(thePoint) - originX) / scale, (originY - boost::geometry::get<1>(thePoint)) / scale ) );
      }

      const cv::Point *pts = (const cv::Point*) cv::Mat(contour).data;
      int npts = cv::Mat(contour).rows;

      // draw the polygon
      cv::polylines(image, &pts, &npts, 1, false, cv::Scalar(0, 255, 0));

    }

    cv::imshow( "Image", image );

    int keyValue = cv::waitKey( 15 );

    switch( keyValue )
    {
      case 'x':
      case 'X':
        bDone = true;
        break;

      case 'a':
      case 'A':
        originX -= panStep * scale ;
        break;

      case 's':
      case 'S':
        originX += panStep * scale ;
        break;

      case 'w':
      case 'W':
        originY -= panStep * scale ;
        break;

      case 'z':
      case 'Z':
        originY += panStep * scale ;
        break;

      case 65579:  // plus
        scale /= 2.0;
        break;

      case 45:    // minus
        scale *= 2.0;
        break;
    }

    image.setTo(cv::Scalar(0, 0, 0));
  }

  return 0;
}
