#include <iostream>
#include <cstdint>
#include <iomanip>

#include <climits>

#include <math.h>

#include "ndsreader.h"

//Morton Code coding:

static int32_t MIN_X = INT_MIN;
static int32_t MIN_Y = MIN_X / 2;
static uint64_t magicbit2D_masks64[6] = { 0x00000000FFFFFFFF, 0x0000FFFF0000FFFF, 0x00FF00FF00FF00FF,
                                                0x0F0F0F0F0F0F0F0F, 0x3333333333333333, 0x5555555555555555 };

uint64_t morton2D_SplitBy2Bits( uint32_t a )
{
	const uint64_t* masks = magicbit2D_masks64;
	uint64_t x = (uint64_t) a;

	x = (x | x << 32) & masks[0];
	x = (x | x << 16) & masks[1];
	x = (x | x <<  8) & masks[2];
	x = (x | x <<  4) & masks[3];
	x = (x | x <<  2) & masks[4];
	x = (x | x <<  1) & masks[5];

	return x;
}

// ENCODE 2D Morton code : Magic bits
uint64_t MortonCode_shuffle_new( const uint32_t x, const uint32_t y )
{
	return morton2D_SplitBy2Bits(x) | (morton2D_SplitBy2Bits(y) << 1);
}

uint32_t morton2D_GetSecondBits( uint64_t m )
{
	const uint64_t* masks = magicbit2D_masks64;
	uint64_t x = m & masks[5];
	x = (x ^ (x >> 1)) & masks[4];
	x = (x ^ (x >> 2)) & masks[3];
	x = (x ^ (x >> 4)) & masks[2];
	x = (x ^ (x >> 8)) & masks[1];
	x = (x ^ (x >> 16)) & masks[0];
	return static_cast<uint32_t>( x );
}

void MortonCode_unshuffle( const uint64_t m, uint32_t &x, uint32_t &y )
{
	x = morton2D_GetSecondBits(m);
	y = morton2D_GetSecondBits(m >> 1);
}

int64_t MortonCode_shuffle(int32_t x, int32_t y)
{
    uint32_t mask = (1 << 31);
    int64_t result = 0;
    uint64_t outMask = (1L << 63);
    for (int32_t i = 0; i < 32; i++)
    {
        if ((y & mask) == mask)
        {
            result |= outMask;
        }
        outMask >>= 1;
        if ((x & mask) == mask)
        {
            result |= outMask;
        }
        outMask >>= 1;
        mask >>= 1;
    }

    result &= ~(1L << 63);
    return result;
}

int64_t getWidth(int32_t level)
{
    return 1L << (31 - level); // 2^(31 - level)
}

void getTileAnchor( int64_t tileNum, int32_t levelNum, int32_t &x, int32_t &y )
{
    uint64_t minZCode = ((int64_t) tileNum) << (63 - getBitCountForTileNum(levelNum));
    uint32_t coords[2];

    MortonCode_unshuffle( minZCode, coords[0], coords[1] );

    if (levelNum < 0)
    {
        x = MIN_X;
        y = MIN_Y;
    } else if (levelNum == 0)
    {
        x = coords[0];
        y = MIN_Y;
    } else
    {
        x = coords[0];
        y = coords[1];
    }
}

void getTileCenter( int64_t tileNum, int32_t levelNum, int32_t &dx, int32_t &dy )
{
  int32_t x, y;

  getTileAnchor( tileNum, levelNum, x, y );
  dx = x + (getWidth( levelNum ) / 2);
  dy = y + (getWidth( levelNum ) / 2);
}

// Tile number and level -> tileId for dedicated level (key value in all tables to get the data)

int64_t getPackedTileId( int64_t tileNum, int32_t levelNum )
{
    int64_t result = 1L << (16 + levelNum); // 2 ^ (16 + levelNum)
    result += tileNum;
    return result;
}

int32_t getBitCountForTileNum(int32_t level)
{
    return 2 * level + 1;
}

// Calculating tile ID for this position at each level(returns IDs for each level).
int64_t getTile( int32_t x, int32_t y, int32_t level)
{
    int64_t zCoord = MortonCode_shuffle(x, y);
    int32_t tileNumber = (int32_t) (zCoord >> (63 - getBitCountForTileNum(level)));
    return getPackedTileId(tileNumber, level);
}

/*
	Transform "WGS84" Coordinates into NDS Coordinates
		0. "WGS84" Coordinates
		1. Basic Input Position in decimal degree -> convert into Integer WGS84_7 (7 digit) representation
			- multiply decimal degree value 10000000 for 7 digit shift
				- 7 digit shift is maximum for integer representation based on 180° maximum of range
		2. Convert to NDS Integer value

	NOTE:	(c/c++ based code sample)
*/

//create integer representation of position coordinates
void coord_dec2int(double x_in, double y_in, int32_t &xWGS84, int32_t &yWGS84)
{
	xWGS84 = floor(x_in*10000000);
	yWGS84 = floor(y_in*10000000);
}

//create integer representation of position coordinates
void coord_int2dec(int32_t xWGS84, int32_t yWGS84, double &x, double &y)
{
	x = static_cast<double>(xWGS84)/10000000.;
	y = static_cast<double>(yWGS84)/10000000.;
}

// convert into NDS integer value representation
void wgs84_7ToNDS(int32_t xWGS84, int32_t yWGS84, int32_t &xNDS_Result, int32_t &yNDS_Result)
{
	xNDS_Result = static_cast<int32_t>((int64_t(xWGS84) << 30) / int64_t(900000000));
	yNDS_Result = static_cast<int32_t>((int64_t(yWGS84) << 30) / int64_t(900000000));
}

// convert into NDS integer value representation
void NDS_Towgs84_7(int32_t xNDS, int32_t yNDS, int32_t &xWGS84, int32_t &yWGS84 )
{
	xWGS84 = static_cast<int32_t>((int64_t(xNDS) * int64_t(900000000)) >> 30);
	yWGS84 = static_cast<int32_t>((int64_t(yNDS) * int64_t(900000000)) >> 30);
}

void wgs84To3857( double lon, double lat, double &x, double &y)
{
  x = lon * 20037508.34 / 180.0;
  y = log( tan((90.0 + lat) * M_PI / 360.0)) / (M_PI / 180.0);
  y = y * 20037508.34 / 180.0;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  Expose contained linestrings
//
vecLineStrings vecGeometry;
long currentileID;

bool tileReceiver( const NDatabase::SLaneTile &theTile )
{
//
//  No obvious way to get tileid from lane tile, which seems unfortunate
//
  int32_t dTileCenterX, dTileCenterY, dbaseTileHeight = 0.0;
  getTileCenter( currentileID, 13, dTileCenterX, dTileCenterY );

  int bufferLength = theTile.blob.size();

  nds::lane::main::LaneTile theLaneTile;

  NParser::CParser *pParser = new NParser::CParser();
  pParser->parseLaneTile( theTile.blob, theLaneTile );

  nds::lane::main::LaneTileHeader& theLaneHeader = theLaneTile.getHeader();
  nds::lane::main::LaneTileContentMask& theContentMask = theLaneHeader.getContentMask();
  nds::common::flexattr::attrmaps::AttributeMapList &theAttributeList = theLaneTile.getAttributeMaps();

  if( theAttributeList.getNumMaps() <= 0 )
    return false;

  datascript::ObjectArray<nds::common::flexattr::attrmaps::AttributeMap>& theAttributeMap = theAttributeList.getAttrMap();
  std::cout << "Attribute map Entries " << theAttributeMap.size() << std::endl;

  for( int iMapIndex = 0; iMapIndex < theAttributeMap.size(); iMapIndex++ )
  {
  nds::common::flexattr::attrmaps::AttributeMap &theRealMap = theAttributeMap.at( iMapIndex );

  nds::common::flexattr::attrmaps::AttributeTypeRef& theTypeRefs = theRealMap.getAttrRefHeader();
  std::cout << "Attribute Type Ref count " << std::dec << theTypeRefs.getNumAttrCodes() << std::endl;

  datascript::ObjectArray<nds::common::flexattr::attrmaps::AttrVals4OneFeature>& theWrapperAttrValues = theRealMap.getValues4OneFeature();
  std::cout << "Attribute Values Entries " << theWrapperAttrValues.size() << std::endl;

  for( int iAttributeIndex = 0; iAttributeIndex < theWrapperAttrValues.size(); iAttributeIndex++ )
  {
    nds::common::flexattr::attrmaps::AttrVals4OneFeature &theRealMapAttr =  theWrapperAttrValues.at( iAttributeIndex );

    nds::common::flexattr::attrmaps::AttrValueList& theValueList = theRealMapAttr.getAttrValList();
    datascript::ObjectArray<nds::common::flexattr::valuecodes::AttributeValue>& theValueValues = theValueList.getValues();

    for( int arrtLGIndex = 0; arrtLGIndex < theValueValues.size(); arrtLGIndex++ )
    {
    nds::lane::attrdefs::LaneGroup theLaneGroup = theValueValues.at( arrtLGIndex ).getLaneGroup();
//    std::cout << "Lane Group ID " << theLaneGroup.getId() << " element count " << theLaneGroup.getNumLaneConnectivityElements() << std::endl;

    for (int lce = 0; lce < theLaneGroup.getNumLaneConnectivityElements(); ++lce)
    {
//        std::cout << " Conn Element " << l << std::endl;
        auto laneConnectivityElement = theLaneGroup.getLaneConnectivityElements().at(lce);

        if (laneConnectivityElement.getHasGeometry() && laneConnectivityElement.getLaneAdvancedGeometry().hasNurbSpline())
        {
            auto nurbSpline = laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline();
//            std::cout << " Conn Element has nurb " <<  std::endl;
        }
        else if (laneConnectivityElement.getHasGeometry() )
        {
            auto pathVector = laneConnectivityElement.getLaneAdvancedGeometry().getPathVector3D();
//            std::cout << "Lane group " << theLaneGroup.getId() << " group size" << theLaneGroup.getLaneConnectivityElements().size() << " Conn Element has 3d vector " << lce << " with " << pathVector.getNumVertices() << " Points" <<  std::endl;

            linestring_t *theLineString = new( linestring_t );
//
//  Note first coord has tile anchors added
//
            nds::common::geometry3d::Vector3D startPoint = pathVector.getVertices().at( 0 );
            int32_t latStart = startPoint.getLatitude().getValue() + dTileCenterY;
            int32_t lonStart = startPoint.getLongitude().getValue() + dTileCenterX;
            int32_t heightStart = startPoint.getHeight().getValue() + dbaseTileHeight;

            int32_t xWGS84, yWGS84;
            double theLat, theLon, mercX, mercY;

            NDS_Towgs84_7( lonStart, latStart, xWGS84, yWGS84 );
            coord_int2dec( xWGS84, yWGS84, theLon, theLat );
            wgs84To3857( theLon, theLat, mercX, mercY );

            boost::geometry::append( *theLineString, point_t( mercX, mercY ) );

            for( int iVector = 1; iVector < pathVector.getNumVertices(); iVector++ )
            {
//
//  These are deltas
//
              nds::common::geometry3d::Vector3D theVector = pathVector.getVertices().at( iVector );
              int32_t lat = theVector.getLatitude().getValue() + latStart;
              int32_t lon = theVector.getLongitude().getValue() + lonStart;

              NDS_Towgs84_7( lon, lat, xWGS84, yWGS84 );
              coord_int2dec( xWGS84, yWGS84, theLon, theLat );
              wgs84To3857( theLon, theLat, mercX, mercY );
//
//  Interface to internal data structures here
//
//              std::cout << std::setprecision(12) << "Vertex " << " Lon, Lat " << theLon << ", " << theLat << " Merc " << mercX << ", " << mercY << std::endl;
              boost::geometry::append( *theLineString, point_t( mercX, mercY ) );
              latStart = lat;
              lonStart = lon;
            }

            if( boost::geometry::length( *theLineString ) > 0 )
              vecGeometry.push_back( theLineString );
        }
    }

    if( theLaneGroup.hasBoundaryElements() )
    {
      auto laneBoundaryGroups = theLaneGroup.getBoundaryElements();

      for( int boundaryGroupIndex = 0; boundaryGroupIndex < laneBoundaryGroups.size(); boundaryGroupIndex ++ )
      {
        nds::lane::attrdefs::LaneBoundaryGroup theBoundaryGroup = laneBoundaryGroups.at( boundaryGroupIndex );

        if( theBoundaryGroup.hasParallelElements() )
        {
          for( int parallelElementIndex = 0; parallelElementIndex < theBoundaryGroup.getNumParallelElements(); parallelElementIndex ++ )
          {
            nds::lane::attrdefs::LaneBoundaryParallelElement theParallelElement = theBoundaryGroup.getParallelElements().at( parallelElementIndex );

            if( theParallelElement.hasLaneBoundaryGeometry() )
            {
              auto pathVector = theParallelElement.getLaneBoundaryGeometry().getPathVector3D();

              linestring_t *theLineString = new( linestring_t );
  //
  //  Note first coord has tile anchors added
  //
              nds::common::geometry3d::Vector3D startPoint = pathVector.getVertices().at( 0 );
              int32_t latStart = startPoint.getLatitude().getValue() + dTileCenterY;
              int32_t lonStart = startPoint.getLongitude().getValue() + dTileCenterX;
              int32_t heightStart = startPoint.getHeight().getValue() + dbaseTileHeight;

              int32_t xWGS84, yWGS84;
              double theLat, theLon, mercX, mercY;

              NDS_Towgs84_7( lonStart, latStart, xWGS84, yWGS84 );
              coord_int2dec( xWGS84, yWGS84, theLon, theLat );
              wgs84To3857( theLon, theLat, mercX, mercY );

              boost::geometry::append( *theLineString, point_t( mercX, mercY ) );

              for( int iVector = 1; iVector < pathVector.getNumVertices(); iVector++ )
              {
  //
  //  These are deltas
  //
                nds::common::geometry3d::Vector3D theVector = pathVector.getVertices().at( iVector );
                int32_t lat = theVector.getLatitude().getValue() + latStart;
                int32_t lon = theVector.getLongitude().getValue() + lonStart;

                NDS_Towgs84_7( lon, lat, xWGS84, yWGS84 );
                coord_int2dec( xWGS84, yWGS84, theLon, theLat );
                wgs84To3857( theLon, theLat, mercX, mercY );
  //
  //  Interface to internal data structures here
  //
  //              std::cout << std::setprecision(12) << "Vertex " << " Lon, Lat " << theLon << ", " << theLat << " Merc " << mercX << ", " << mercY << std::endl;
                boost::geometry::append( *theLineString, point_t( mercX, mercY ) );
                latStart = lat;
                lonStart = lon;
              }

              if( boost::geometry::length( *theLineString ) > 0 )
                vecGeometry.push_back( theLineString );
            }
          }
        }
      }
    }
  }
  }
}

  return true;
}

//-----------------------------------------------------------------------------
//
//  Seems to be a bug somewhere....  So DB init and tile parsing combined for now
//
NDatabase::CLaneBuildingBlock *setupDB( std::string &DBPath, long tileID )
{
  NDatabase::CRoot theDB = NDatabase::CRoot( NSal::CSqlConnection( "./US_BayAreaFromHere_10052017/ROOT.NDS" ) );

  std::cout << "Product count " << theDB.getNumProducts() << std::endl;

  if( theDB.getNumProducts() < 1 )
  {
    std::cout << "No products in DB, exiting" << std::endl;
    exit( 0 );
  }

  NDatabase::CProduct &theProduct = (NDatabase::CProduct &) theDB.getProduct( 0 );
  const std::vector<NDatabase::SBuildingBlockInfo>& theBlocks = theProduct.getBuildingBlocksInfo();

  std::cout << "Building Blocks " << theBlocks.size() << std::endl;
  std::vector<NDatabase::SBuildingBlockInfo>::const_iterator iterBlocks;

  for( iterBlocks = theBlocks.begin(); iterBlocks != theBlocks.end(); iterBlocks++ )
  {
    std::cout <<  "Building Block Info " << (*iterBlocks).name << " type " << (int)(*iterBlocks).bbType << std::endl;

    if( (*iterBlocks).bbType == NDatabase::BuildingBlockType::LANE )
      break;
  }

  if( iterBlocks == theBlocks.end() )
  {
      std::cout << "No lanes present in DB" << std::endl;
      exit( 0 );
  }

  std::unique_ptr< NDatabase::ILaneBuildingBlock > theLanesBB = theProduct.createLaneBuildingBlock( (*iterBlocks).urId, (*iterBlocks).bbId );
  NDatabase::CLaneBuildingBlock *pLanes = (NDatabase::CLaneBuildingBlock*) theLanesBB.get();

  std::vector< NDatabase::TPackedTileId > vecTileIDs;
  vecTileIDs.push_back( tileID );
  currentileID = tileID;

  pLanes->loadTiles( vecTileIDs, tileReceiver );

  return pLanes;
}
