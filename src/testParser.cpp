#include <iostream>
#include <cstdint>
#include <iomanip>

#include <math.h>

#include "ndsreader.h"

extern vecLineStrings vecGeometry;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  if( argc < 4 )
  {
    std::cout << "path to db, latitude and longitude required on command line" << std::endl;
    exit( 1 );
  }

  double lat = atof( argv[ 2 ] );
  double lon = atof( argv[ 3 ] );
  int32_t xWGS84;
  int32_t yWGS84;
  int32_t xNDS_Result;
  int32_t yNDS_Result;
  int32_t iLevel = 13;  //  Only level for HD data

  coord_dec2int( lon, lat, xWGS84, yWGS84 );
  wgs84_7ToNDS( xWGS84, yWGS84, xNDS_Result, yNDS_Result );

  long tileID = getTile( xNDS_Result, yNDS_Result, iLevel );
  std::cout << " TileID " << tileID << " for level " << iLevel << " lat " << lat << " lon " << lon << std::endl;

  int32_t dCenterX, dCenterY;
  getTileCenter( tileID, iLevel, dCenterX, dCenterY );

  double theLat, theLon, mercX, mercY;

  NDS_Towgs84_7( dCenterX, dCenterY, xWGS84, yWGS84 );
  coord_int2dec( xWGS84, yWGS84, theLon, theLat );

  std::cout << " Tile Center for tile " << tileID << ", level " << iLevel << " lat " << theLat << " lon " << theLon << std::endl;

  std::string DBPath( argv[ 1 ] );

  for( tileID = 611096870; tileID < 611096880; tileID++ )
  {
    setupDB( DBPath, tileID );

    std::cout << "Found " <<  vecGeometry.size() << " linestrings" << std::endl;
  }

  return 0;
}
