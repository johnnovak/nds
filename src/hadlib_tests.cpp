#define BOOST_TEST_MODULE hadlib_tests
#include <boost/test/unit_test.hpp>
#include "hadlib/hadlib.h"
#include "TestConfig.hpp"

namespace tests {
    /** Note: Only HADLib public types are allowed to be used in this tests. */
    class LoadAndParseTestFixture
    {
    public:
        explicit LoadAndParseTestFixture(const std::string& rootFile)
        {
            auto root = NDatabase::open(::NTest::get_test_data_path() / rootFile);
            BOOST_REQUIRE(root);
            BOOST_REQUIRE(root->getNumProducts() > 0);

            const NDatabase::IProduct& product = root->getProduct(0);
            for (auto bbInfo : product.getBuildingBlocksInfo()) {
                if (bbInfo.bbType == NDatabase::BuildingBlockType::LANE) {
                    mLaneBb = product.createLaneBuildingBlock(bbInfo.urId, bbInfo.bbId);
                    break;
                }
            }

            mParser = NParser::create();

            BOOST_REQUIRE(mParser);
            BOOST_REQUIRE(mLaneBb);
        }

        std::unique_ptr<NDatabase::ILaneBuildingBlock> mLaneBb;
        std::unique_ptr<NParser::IParser> mParser;
    };

    class LoadAndParseTestFixture002 : public LoadAndParseTestFixture
    {
    public:
       LoadAndParseTestFixture002() : LoadAndParseTestFixture("002/ROOT.NDS")
       {
       }
    };

    BOOST_FIXTURE_TEST_SUITE(hadlib_tests002, LoadAndParseTestFixture002);

    BOOST_AUTO_TEST_CASE(load_and_parse_lane_tile)
    {
        std::vector<char> loadedData;

        mLaneBb->loadTiles({611079450}, [&loadedData](const NDatabase::SLaneTile& tile)->bool {
            loadedData = tile.blob;
            return false;
        });

        nds::lane::main::LaneTile laneTile;
        BOOST_CHECK_NO_THROW(mParser->parseLaneTile(loadedData, laneTile));

        BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);
        BOOST_CHECK_EQUAL(laneTile.hasExternalTileIdList(), false);

        BOOST_CHECK_EQUAL(laneTile.getAttributeMaps().getNumMaps(), 6);
    }

    BOOST_AUTO_TEST_CASE(load_and_parse_nurbs_from_a_specific_tile)
    {
        std::vector<char> loadedData;

        mLaneBb->loadTiles({611079450}, [&loadedData](const NDatabase::SLaneTile& tile)->bool {
            loadedData = tile.blob;
            return false;
        });

        nds::lane::main::LaneTile laneTile;
        BOOST_CHECK_NO_THROW(mParser->parseLaneTile(loadedData, laneTile));

        BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);
        BOOST_CHECK_EQUAL(laneTile.hasExternalTileIdList(), false);

        BOOST_CHECK_EQUAL(laneTile.getAttributeMaps().getNumMaps(), 6);

        for (int i = 0; i < 6; ++i) {
            auto ref = laneTile.getAttributeMaps().getAttrTypeRef().at(i);

            for (int j = 0; j < ref.getNumAttrCodes(); ++j) {
                if (ref.getAttributeTypeCodes().at(j) == nds::common::flexattr::valuecodes::AttributeTypeCode::LANE_GROUP) {
                    auto map = laneTile.getAttributeMaps().getAttrMap().at(i);
                    BOOST_CHECK_EQUAL(map.getAttrMapType(), nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE);

                    auto values = map.getValues4OneFeature();
                    for (int k = 0; k < map.getNumEntries(); ++k) {
                        auto laneGroup = values.at(k).getAttrValList().getValues().at(0).getLaneGroup();
                        for (int l = 0; l < laneGroup.getNumLaneConnectivityElements(); ++l) {
                            auto laneConnectivityElement = laneGroup.getLaneConnectivityElements().at(l);
                            if (laneConnectivityElement.hasLaneAdvancedGeometry() && laneConnectivityElement.getLaneAdvancedGeometry().hasNurbSpline()) {
                                auto nurbSpline = laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline();
                            }
                        }

                    }
                }
            }
        }
    }

    // Adding this from Vassilios branch manually. Actually this does not belong here, 
    // It should be either in devkit or in a specific exploration test suite...
    BOOST_AUTO_TEST_CASE(LnP_LaneTile)
    {
        std::vector<char> loadedData;

        mLaneBb->loadTiles({611079450}, [&loadedData](const NDatabase::SLaneTile& tile)->bool {
            loadedData = tile.blob;
            return false;
        });

        nds::lane::main::LaneTile laneTile;
        BOOST_CHECK_NO_THROW(mParser->parseLaneTile(loadedData, laneTile));

        BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);
        BOOST_CHECK_EQUAL(laneTile.hasExternalTileIdList(), false);

        BOOST_CHECK_EQUAL(laneTile.getAttributeMaps().getNumMaps(), 6);
    }

    BOOST_AUTO_TEST_CASE(Explore_LoadAndParse_Nurbs)
    {
        std::vector<char> loadedData;

        mLaneBb->loadTiles({611079450}, [&loadedData](const NDatabase::SLaneTile& tile)->bool {
            loadedData = tile.blob;
            return false;
        });

        nds::lane::main::LaneTile laneTile;
        BOOST_CHECK_NO_THROW(mParser->parseLaneTile(loadedData, laneTile));

        BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);
        BOOST_CHECK_EQUAL(laneTile.hasExternalTileIdList(), false);

        BOOST_CHECK_EQUAL(laneTile.getAttributeMaps().getNumMaps(), 6);

        for (int i = 0; i < 6; ++i) {
            auto ref = laneTile.getAttributeMaps().getAttrTypeRef().at(i);

            for (int j = 0; j < ref.getNumAttrCodes(); ++j) {
                if (ref.getAttributeTypeCodes().at(j) == nds::common::flexattr::valuecodes::AttributeTypeCode::LANE_GROUP) {
                    auto map = laneTile.getAttributeMaps().getAttrMap().at(i);
                    BOOST_CHECK_EQUAL(map.getAttrMapType(), nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE);

                    auto values = map.getValues4OneFeature();
                    for (int k = 0; k < map.getNumEntries(); ++k) {
                        auto laneGroup = values.at(k).getAttrValList().getValues().at(0).getLaneGroup();
                        for (int l = 0; l < laneGroup.getNumLaneConnectivityElements(); ++l) {
                            auto laneConnectivityElement = laneGroup.getLaneConnectivityElements().at(l);
                            if (laneConnectivityElement.hasLaneAdvancedGeometry() && laneConnectivityElement.getLaneAdvancedGeometry().hasNurbSpline()) {
                                auto nurbSpline = laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline();
                            }
                        }
                    }
                }
            }
        }
    }

    BOOST_AUTO_TEST_CASE(Explore_StartConnector_Coordinates)
    {
        std::vector<char> loadedData;

        mLaneBb->loadTiles({611079450}, [&loadedData](const NDatabase::SLaneTile& tile)->bool {
            loadedData = tile.blob;
            return false;
        });

        nds::lane::main::LaneTile laneTile;
        BOOST_CHECK_NO_THROW(mParser->parseLaneTile(loadedData, laneTile));

        BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);
        BOOST_CHECK_EQUAL(laneTile.hasExternalTileIdList(), false);

        BOOST_CHECK_EQUAL(laneTile.getAttributeMaps().getNumMaps(), 6);

        for (int i = 0; i < 6; ++i) {
            auto ref = laneTile.getAttributeMaps().getAttrTypeRef().at(i);

            for (int j = 0; j < ref.getNumAttrCodes(); ++j) {
                if (ref.getAttributeTypeCodes().at(j) == nds::common::flexattr::valuecodes::AttributeTypeCode::LANE_GROUP) {
                    auto map = laneTile.getAttributeMaps().getAttrMap().at(i);
                    BOOST_CHECK_EQUAL(map.getAttrMapType(), nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE);

                    auto values = map.getValues4OneFeature();
                    for (int k = 0; k < map.getNumEntries(); ++k) {
                        auto laneGroup = values.at(k).getAttrValList().getValues().at(0).getLaneGroup();
                        for (int l = 0; l < laneGroup.getNumLaneConnectivityElements(); ++l) {
                            auto laneConnectivityElement = laneGroup.getLaneConnectivityElements().at(l);

                            BOOST_CHECK(laneConnectivityElement.hasLaneConnectivity());

                            auto laneConnectivity = laneConnectivityElement.getLaneConnectivity();

                            BOOST_CHECK(laneConnectivityElement.hasLaneAdvancedGeometry() && laneConnectivityElement.getLaneAdvancedGeometry().hasNurbSpline());

                            auto nurbSpline = laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline();

                            auto refPointLat = nurbSpline.getRefPoint3D().getLatitude();
                            auto refPointLong = nurbSpline.getRefPoint3D().getLongitude();

                            BOOST_TEST_MESSAGE(
                                "SourceConnectorId:;" <<
                                laneConnectivity.getSourceLaneConnectorId() << ";" <<
                                refPointLat.getValue() << "," << refPointLong.getValue() << ";" <<
                                "DestConnectorId:;" << laneConnectivity.getDestLaneConnectorId()
                            );
                        }
                    }
                }
            }
        }
    }

    BOOST_AUTO_TEST_CASE(Explore_NurbCurve_KnotsCPsWeights)
    {
        std::vector<char> loadedData;

        mLaneBb->loadTiles({611079450}, [&loadedData](const NDatabase::SLaneTile& tile)->bool {
            loadedData = tile.blob;
            return false;
        });

        nds::lane::main::LaneTile laneTile;
        BOOST_CHECK_NO_THROW(mParser->parseLaneTile(loadedData, laneTile));

        BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);
        BOOST_CHECK_EQUAL(laneTile.hasExternalTileIdList(), false);

        BOOST_CHECK_EQUAL(laneTile.getAttributeMaps().getNumMaps(), 6);

        for (int i = 0; i < 6; ++i) {
            auto ref = laneTile.getAttributeMaps().getAttrTypeRef().at(i);

            for (int j = 0; j < ref.getNumAttrCodes(); ++j) {
                if (ref.getAttributeTypeCodes().at(j) == nds::common::flexattr::valuecodes::AttributeTypeCode::LANE_GROUP) {
                    auto map = laneTile.getAttributeMaps().getAttrMap().at(i);
                    BOOST_CHECK_EQUAL(map.getAttrMapType(), nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE);

                    auto values = map.getValues4OneFeature();
                    for (int k = 0; k < map.getNumEntries(); ++k) {
                        auto laneGroup = values.at(k).getAttrValList().getValues().at(0).getLaneGroup();
                        for (int l = 0; l < laneGroup.getNumLaneConnectivityElements(); ++l) {
                            auto laneConnectivityElement = laneGroup.getLaneConnectivityElements().at(l);

                            BOOST_CHECK(laneConnectivityElement.hasLaneConnectivity());

                            auto laneConnectivity = laneConnectivityElement.getLaneConnectivity();

                            BOOST_CHECK(laneConnectivityElement.hasLaneAdvancedGeometry() && laneConnectivityElement.getLaneAdvancedGeometry().hasNurbSpline());

                            auto nurbSpline = laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline();

                            BOOST_REQUIRE_EQUAL(1, nurbSpline.getNumCurves());

                            auto const& nurbCurve = laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline().getNurbCurves().at(0);

                            BOOST_REQUIRE(laneConnectivityElement.getLaneAdvancedGeometry().getNurbSpline().getNurbCurves().at(0).getControlPoints().size() > 3);

                            BOOST_TEST_MESSAGE(
                                "Knots: " << nurbCurve.getNumKnots() << "  " <<
                                "ControlPoints: " << nurbCurve.getNumControlPoints() << "  " <<
                                "Weights: " << nurbCurve.getNumWeights()
                            );
                        }
                    }
                }
            }
        }
    }

    BOOST_AUTO_TEST_SUITE_END();

    class LoadAndParseTestFixtureTomTomCut004 : public LoadAndParseTestFixture
    {
    public:
       LoadAndParseTestFixtureTomTomCut004() : LoadAndParseTestFixture("TomTomCut/004/ROOT.NDS")
       {
       }
    };

    BOOST_FIXTURE_TEST_SUITE(hadlib_testsTomTomCut004, LoadAndParseTestFixtureTomTomCut004);

    BOOST_AUTO_TEST_CASE(Explore_LocObjs)
    {
       mLaneBb->loadTiles({ 545458150, 545457712, 545554583, 545470383, 545458153, 545459562 }, [this](const NDatabase::SLaneTile& tile)->bool
       {
          nds::lane::main::LaneTile laneTile;
          BOOST_CHECK_NO_THROW(mParser->parseLaneTile(tile.blob, laneTile));

          BOOST_CHECK_EQUAL(laneTile.hasAttributeMaps(), true);

          bool hasLocObjs = false;

          for (int i = 0; i < laneTile.getAttributeMaps().getNumMaps(); ++i)
          {
             auto ref = laneTile.getAttributeMaps().getAttrTypeRef().at(i);

             for (int j = 0; j < ref.getNumAttrCodes(); ++j)
             {
                if (ref.getAttributeTypeCodes().at(j) == nds::common::flexattr::valuecodes::AttributeTypeCode::LOCALIZATION_OBJECT)
                {
                   BOOST_CHECK_EQUAL(ref.getReferenceType(), nds::common::flexattr::attrmaps::ReferenceType::ROUTING_LINK_DIRECTED);
                   hasLocObjs = true;
                   auto map = laneTile.getAttributeMaps().getAttrMap().at(i);
                   BOOST_CHECK_EQUAL(map.getAttrMapType(), nds::common::flexattr::attrmaps::AttrMapType::VALUES_TO_ONE_FEATURE);

                   auto values = map.getValues4OneFeature();
                   for (int k = 0; k < map.getNumEntries(); ++k)
                   {
                      auto locObj = values.at(k).getAttrValList().getValues().at(j).getLocalizationObject();
                      auto linkId = values.at(k).getFeature().getDirectedLinkRef();
                      for (int l = 0; l < locObj.getNumTemplates(); ++l)
                      {
                         auto template_ = locObj.getTemplates().at(l);
                         int id = template_.getId();
                         bool found = false;
                         mLaneBb->loadLocTemplates({ id }, [this, template_, tile, linkId, &found](const NDatabase::SLocTemplate& locTemplate)->bool
                         {
                            nds::locobj::main::LocTemplateDetails locTemplateDetails(static_cast<nds::locobj::main::LocTemplateType::e_LocTemplateType>(locTemplate.type));
                            BOOST_CHECK_NO_THROW(mParser->parseLocTemplateDetails(locTemplate.details, locTemplateDetails));
                            found = true;
                            printf("%d, %d, %s\n   -> pos(%d, %d, %d) dir(%d, %d, %d), scale(%d, %d, %d)\n      -> %d: %d point(s), ", tile.id, linkId.getLinkId(), linkId.getPositiveLinkDirection() ? "pos" : "neg",
                               template_.getPosition().getLongitude().getValue(), template_.getPosition().getLatitude().getValue(), template_.getPosition().getHeight().getValue(), 
                               template_.getDirection().getX().getValue(), template_.getDirection().getY().getValue(), template_.getDirection().getHeight().getValue(),
                               template_.getScaleX(), template_.getScaleY(), template_.getScaleZ(),
                               template_.getId(), locTemplateDetails.getNumLocTemplatePoints());
                            switch (locTemplate.type)
                            {
                            case NDatabase::LocTemplateType::SIGN:
                               printf("SIGN: %s, %s\n", locTemplateDetails.getTypeDetails().getSignDetails().getSignMountType().toString(), locTemplateDetails.getTypeDetails().getSignDetails().getSignType().toString());
                               break;
                            case NDatabase::LocTemplateType::POLE:
                               printf("POLE: diameter %d, height %d, %s\n", locTemplateDetails.getTypeDetails().getPoleDetails().getDiameter(), locTemplateDetails.getTypeDetails().getPoleDetails().getHeight(), locTemplateDetails.getTypeDetails().getPoleDetails().getPoleType().toString());
                               break;
                            case NDatabase::LocTemplateType::MARKING:
                               printf("MARKING: %s, %s\n", locTemplateDetails.getTypeDetails().getMarkingDetails().getColor().toString(), locTemplateDetails.getTypeDetails().getMarkingDetails().getMarkingType().toString());
                               break;
                            }
                            return true;
                         });
                         BOOST_CHECK(found);
                      }
                   }
                }
             }
          }
          BOOST_CHECK(hasLocObjs);
          return true;
       });

    }

    BOOST_AUTO_TEST_SUITE_END();
}