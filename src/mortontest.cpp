#include <iostream>
#include <cstdint>
#include <iomanip>

#include <math.h>

#include "ndsreader.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  uint64_t mortoncode;
  uint32_t initialX = 123456, initialY = 77854;

  mortoncode = MortonCode_shuffle( initialX, initialY );

  uint32_t newX, newY;
  MortonCode_unshuffle( mortoncode, newX, newY );

  if( newX != initialX || newY != initialY )
    std::cout << " Morton test failed" << std::endl;

  return 0;
}
